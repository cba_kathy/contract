<?php /* Smarty version 2.6.27, created on 2017-01-06 18:18:47
         compiled from admin/user_edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', 'admin/user_edit.html', 1, false),array('modifier', 'explode', 'admin/user_edit.html', 6, false),array('modifier', 'in_array', 'admin/user_edit.html', 82, false),array('block', 'form', 'admin/user_edit.html', 31, false),array('function', 'form_error', 'admin/user_edit.html', 33, false),array('function', 'form_label', 'admin/user_edit.html', 39, false),array('function', 'input_html', 'admin/user_edit.html', 41, false),array('function', 'form_file', 'admin/user_edit.html', 71, false),)), $this); ?>
<?php if (((is_array($_tmp=$this->_tpl_vars['defaults']['user']['user_id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
	<?php $this->assign('title', 'User Edit'); ?>
<?php else: ?>
	<?php $this->assign('title', 'User Add'); ?>
<?php endif; ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/header.html", 'smarty_include_vars' => array('title' => ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'id' => "",'css' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "fileupload/import.css") : explode($_tmp, "fileupload/import.css")),'css_link' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")),'js' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "fileupload/import.js") : explode($_tmp, "fileupload/import.js")),'js_link' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script>
	$(document).ready(function() {
		$('.frmEdit').colorErrorInputs();
	});
</script>

<div id="main">
	<ol class="breadcrumbs">
		<li><a href="/admin/">Home</a></li>
		<li><a href="/admin/user_list">user List</a></li>
		<li><em><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</em></li>
	</ol>
	<!--  .breadcrumbs -->

	<section class="section" id="top">
		<h1 class="headline"><span><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</span></h1>
		
		<?php $this->_tag_stack[] = array('form', array('class' => 'frmEdit')); $_block_repeat=true;smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
			
			<?php echo smarty_function_form_error(array(), $this);?>


			<table class="sheet mb20">
				<tbody>
					
					<tr>
						<th class="w150px"><?php echo smarty_function_form_label(array('name' => "user[username]"), $this);?>
</th>
						<td>
							<?php echo smarty_function_input_html(array('name' => "user[username]"), $this);?>

						</td>
					</tr>
					<tr>
						<th><?php echo smarty_function_form_label(array('name' => "user[fullname]"), $this);?>
</th>
						<td>
							<?php echo smarty_function_input_html(array('name' => "user[fullname]"), $this);?>

						</td>
					</tr>
					<tr>
						<th><?php echo smarty_function_form_label(array('name' => "user[nickname]"), $this);?>
</th>
						<td>
							<?php echo smarty_function_input_html(array('name' => "user[nickname]"), $this);?>

						</td>
					</tr>
					<tr>
						<th><?php echo smarty_function_form_label(array('name' => "user[password]"), $this);?>
</th>
						<td>
							<?php echo smarty_function_input_html(array('name' => "user[password]"), $this);?>

						</td>
					</tr>
					<tr>
						<th><?php echo smarty_function_form_label(array('name' => 'password_confirm'), $this);?>
</th>
						<td>
							<?php echo smarty_function_input_html(array('name' => 'password_confirm'), $this);?>

						</td>
					</tr>
					<tr>
						<th><?php echo smarty_function_form_label(array('name' => "user[image]"), $this);?>
</th>
						<td>
							<?php echo smarty_function_form_file(array('name' => "user[image]",'upload_type' => $this->_config[0]['vars']['UPLOAD_TYPE_CROP'],'text' => "Drop image here or click to crop."), $this);?>

						</td>
					</tr>

					<tr>
						<th>Role</th>
						<td>
							<?php echo smarty_function_input_html(array('name' => "user[role]"), $this);?>

							<!-- 2 -->

							<?php $this->assign('show_user', ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "2,3") : explode($_tmp, "2,3"))); ?>
							<?php if (( ((is_array($_tmp=$this->_tpl_vars['form_status'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) == 'confirm' && ((is_array($_tmp=((is_array($_tmp=$_POST['user']['role'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('in_array', true, $_tmp, ((is_array($_tmp=$this->_tpl_vars['show_user'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))) : in_array($_tmp, ((is_array($_tmp=$this->_tpl_vars['show_user'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ) || ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['user']['role'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('in_array', true, $_tmp, ((is_array($_tmp=$this->_tpl_vars['show_user'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))) : in_array($_tmp, ((is_array($_tmp=$this->_tpl_vars['show_user'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))))): ?>
                                <div class="list_user_permission list_checkbox" style="display: block!important">
									<h4>Please choose user permission:</h4>
									<?php echo smarty_function_input_html(array('name' => "user[role_desc]"), $this);?>

								</div>
                            <?php else: ?>
                            	<div class="list_user_permission list_checkbox">
									<h4>Please choose user permission:</h4>
									<?php echo smarty_function_input_html(array('name' => "user[role_desc]"), $this);?>

								</div>
                            <?php endif; ?>

						</td>
					</tr>
				</tbody>
			</table>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/form_edit_buttons.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>

	</section>
<!-- / .section -->
</div>
<!-- / #main -->
<style type="text/css">
	.list_user_permission{
		display: none;
	}
</style>
<script type="text/javascript">
	$('select[name="user[role]"]').on('change', function(){
		
		var role = $(this).val();
		if($.inArray(role, ["3", "2"]) != -1){
			$('.list_user_permission').show();
		}else{
			$('.list_user_permission').hide();
		}
	});
</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/fileupload.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>