<?php /* Smarty version 2.6.27, created on 2017-01-03 14:02:30
         compiled from index.html */ ?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>


  <input type="text" id="ajax" list="json-datalist" placeholder="e.g. datalist">
  <datalist id="json-datalist"></datalist>

<script type="text/javascript">
  // Get the <datalist> and <input> elements.
var dataList = document.getElementById('json-datalist');
var input = document.getElementById('ajax');

// Create a new XMLHttpRequest.
var request = new XMLHttpRequest();

// Handle state changes for the request.
request.onreadystatechange = function(response) {
  if (request.readyState === 4) {
    if (request.status === 200) {
      // Parse the JSON
      var jsonOptions = JSON.parse(request.responseText);
  
      // Loop over the JSON array.
      jsonOptions.forEach(function(item) {
        // Create a new <option> element.
        var option = document.createElement('option');
        // Set the value using the item in the JSON array.
        option.value = item;
        // Add the <option> element to the <datalist>.
        dataList.appendChild(option);
      });
      
      // Update the placeholder text.
      input.placeholder = "e.g. datalist";
    } else {
      // An error occured :(
      input.placeholder = "Couldn't load datalist options :(";
    }
  }
};

// Update the placeholder text.
input.placeholder = "Loading options...";

// Set up and make the request.
request.open('GET', 'http://contract.kathy.cba/html-elements.json', true);
request.send();
</script>


</body>
</html>