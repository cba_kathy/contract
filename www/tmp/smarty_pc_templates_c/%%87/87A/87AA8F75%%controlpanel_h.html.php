<?php /* Smarty version 2.6.27, created on 2016-12-28 10:39:38
         compiled from /usr/share/php/CBFramework_3.1/library/CB/data/templates/controlpanel_h.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign_debug_info', '/usr/share/php/CBFramework_3.1/library/CB/data/templates/controlpanel_h.html', 358, false),array('function', 'cycle', '/usr/share/php/CBFramework_3.1/library/CB/data/templates/controlpanel_h.html', 379, false),array('function', 'echo', '/usr/share/php/CBFramework_3.1/library/CB/data/templates/controlpanel_h.html', 383, false),array('modifier', 'cb_escape', '/usr/share/php/CBFramework_3.1/library/CB/data/templates/controlpanel_h.html', 378, false),array('modifier', 'escape', '/usr/share/php/CBFramework_3.1/library/CB/data/templates/controlpanel_h.html', 380, false),)), $this); ?>

<!--DEBUG Console-->


<style type="text/css">
<!--
#CBFWebDebug a
{
  text-decoration: none;
  font-size: 11px;
  color: #999;
  background-color: transparent;
}

#CBFWebDebug a:hover
{
  text-decoration: none;
  border: none;
  color: #333;
  background-color: transparent;
}

#CBFWebDebugBar
{
  position: absolute;
  margin: 0;
  padding: 2px 0;
  right: 0px;
  top: 0px;
  opacity: 0.90;
  filter: alpha(opacity:90);
  z-index: 10000;
  background-color: #F4F4F4;
}


#CBFWebDebugBar .menu
{
  padding: 0px;
  display: inline;
}

#CBFWebDebugBar .menu li
{
  display: inline;
  list-style: none;
  margin: 0;
  padding: 0 2px;
}

#CBFWebDebugBar .menu li.last
{
  margin: 0;
  padding: 0;
  border: 0;
}

#CBFWebDebugDatabaseDetails li
{
  margin: 0;
  margin-left: 30px;
  padding: 5px 0;
}

#CBFWebDebugShortMessages li
{
  margin-bottom: 10px;
  padding: 5px;
  background-color: #ddd;
}

#CBFWebDebugShortMessages li
{
  list-style: none;
}

#CBFWebDebugDetails
{
  margin-right: 7px;
}

#CBFWebDebug pre
{
  line-height: 1.3;
  margin-bottom: 10px;
}

#CBFWebDebug h1
{
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 20px;
  padding: 0;
  border: 0px;
  background-color: #eee;
  top: 0px;
}

#CBFWebDebug h2
{
  font-size: 14px;
  font-weight: bold;
  margin: 10px 0;
  padding: 0;
  border: 0px;
  background: none;
}

#CBFWebDebug .CBFWebDebugTop
{
  position: absolute;
  left: 0px;
  top: 0px;
  width: 99%;
  padding: 10px;
  z-index: 9999;
  background-color: #F4F4F4;
  border-bottom: 1px solid #aaa;
  opacity: 0.95;
  filter: alpha(opacity:95);

}

#CBFWebDebug .CBFWebDebugTop table th
{
	padding-left:10px;
}

#CBFDBDebug
{
  margin: 0;
  padding: 3px;
  font-size: 11px;
}

#CBFDBDebugMenu li
{
  display: inline;
  list-style: none;
  margin: 0;
  padding: 0 5px;
  border-right: 1px solid #aaa;
}

#CBFWebDebugConfigSummary
{
  display: inline;
  padding: 5px;
  background-color: #ddd;
  border: 1px solid #aaa;
  margin: 20px 0;
}

#CBFWebDebugConfigSummary li
{
  list-style: none;
  display: inline;
  margin: 0;
  padding: 0 5px;
  border-right: 1px solid #aaa;
}

#CBFWebDebugConfigSummary li.last
{
  margin: 0;
  padding: 0;
  border: 0;
}

.CBFWebDebugInfo, .CBFWebDebugInfo td
{
  background-color: #FFF;
}

.CBFWebDebugInfo2, .CBFWebDebugInfo2 td
{
  background-color: #DDD;
}

.CBFWebDebugWarning, .CBFWebDebugWarning td
{
  background-color: orange;
}

.CBFWebDebugError, .CBFWebDebugError td
{
  background-color: #f99;
}

.CBFDBDebugNumber
{
  width: 1%;
}

.CBFDBDebugType
{
  width: 1%;
  white-space: nowrap;
  color: darkgreen;
}

.CBFDBDebugInfo
{
  color: blue;
}

.ison
{
  color: #3f3;
  margin-right: 5px;
}

.isoff
{
  color: #f33;
  margin-right: 5px;
  text-decoration: line-through;
}

.CBFDBDebugs
{
  padding: 0;
  margin: 20px;
  border: 1px solid #999;
  font-size: 11px;
}

.CBFDBDebugs tr
{
  padding: 0;
  margin: 0;
  border: 0;
}

.CBFDBDebugs td
{
  margin: 0;
  border: 0;
  padding: 1px 3px;
  vertical-align: top;
}

.CBFDBDebugs th
{
  margin: 0;
  border: 0;
  padding: 3px 5px;
  vertical-align: top;
  background-color: #999;
  color: #eee;
  white-space: nowrap;
}

.CBFWebDebugDebugInfo
{
  margin-left: 10px;
  padding-left: 5px;
  border-left: 1px solid #aaa;
}

.CBFWebDebugCache
{
  padding: 0;
  margin: 0;
  font-family: Arial;
  position: absolute;
  overflow: hidden;
  z-index: 995;
  font-size: 9px;
  padding: 2px;
  filter:alpha(opacity=85);
  -moz-opacity:0.85;
  opacity: 0.85;
}
-->
</style>

<script type="text/javascript">
// <!--

function CBFDebugShow(id){
	var is_close = false;

	var element = document.getElementById(id);

	if(element.style.display == "none"){
		 is_close = true;
	}

	document.getElementById('CBFTmpDebug').style.display = 'none';
	document.getElementById('CBFDBDebug').style.display = 'none';
	document.getElementById('CBFParamDebug').style.display = 'none';
	document.getElementById('CBFStateDebug').style.display = 'none';
	//document.getElementById('CBFTraceDebug').style.display = 'none';
	document.getElementById('CBFInfoDebug').style.display = 'none';
	document.getElementById('CBFCPDebug').style.display = 'none';

	if (is_close) {
		element.style.display = '';
	}else{
		element.style.display = 'none';
	}
}

function CBFMatchDisplay(pattern)
{
	var i;

	var menuElement = document.getElementById("CBFDBDebugMenu");
	var aList = menuElement.children[0].children;
	var aListLength = aList.length;

	for (i = 0; i < aListLength; i++) {
		if (aList[i].id == pattern || (aList[i].id == "" && pattern == "ALL")) {
			aList[i].style.color = "";
		} else {
			aList[i].style.color = "#CCC";
		}
	}

	var dbDebug = document.getElementById("CBFDBDebugLines");
	var trNodes = dbDebug.children[0].children[0].children;
	var trLength = trNodes.length;
	for (i = 0; i < trLength; i++) {
		if (pattern == "ALL" || trNodes[i].id == undefined || trNodes[i].id == "") {
			trNodes[i].style.display = "";
		} else if (pattern == "OTHER") {
			trNodes[i].style.display = (trNodes[i].id.indexOf("SELECT") < 0 && trNodes[i].id.indexOf("INSERT") < 0 && trNodes[i].id.indexOf("UPDATE") < 0) ? "" : "none";
		} else {
			trNodes[i].style.display = (trNodes[i].id.indexOf(pattern) >= 0) ? "" : "none";
		}
	}
}

function CBFControlPanelShow(id){
	var is_close = false;

	var element = document.getElementById(id);

	if(element.style.display == "none"){
		 is_close = true;
	}

	document.getElementById('CBFCPEmojiList').style.display = 'none';
	document.getElementById('CBFCPCodeGen').style.display = 'none';
	document.getElementById('CBFCPDeviceList').style.display = 'none';
	document.getElementById('CBFCPHtmlList').style.display = 'none';

	if (is_close) {
		element.style.display = '';
	}else{
		element.style.display = 'none';
	}
}
// -->
</script>

<?php echo smarty_function_assign_debug_info(array(), $this);?>


<div id="CBFWebDebug">
    <div id="CBFWebDebugBar" class="CBFWebDebugInfo">
        <ul id="CBFWebDebugDetails" class="menu">
            <li><a href="javascript:void(0);" onclick="CBFDebugShow('CBFTmpDebug');">テンプレート変数</a></li>
            <li><a href="javascript:void(0);" onclick="CBFDebugShow('CBFDBDebug');">SQL</a></li>
            <li><a href="javascript:void(0);" onclick="CBFDebugShow('CBFParamDebug');">パラメータ</a></li>
            <li><a href="javascript:void(0);" onclick="CBFDebugShow('CBFStateDebug');">セッション</a></li>
						<li><a href="javascript:void(0);" onclick="CBFDebugShow('CBFInfoDebug');">情報</a></li>
			<li><a href="javascript:void(0);" onclick="CBFDebugShow('CBFCPDebug');">その他</a></li>
        </ul>
		<a href="/?_debug=off" target="_debug" onclick="document.getElementById('CBFWebDebug').style.display='none';"></a>
    </div>

    <div id="CBFTmpDebug" class="CBFWebDebugTop" style="display: none">
		<h1>テンプレート変数</h1>
        <div id="CBFTmpDebugLines">
        <table class="CBFDBDebugs">
		    <?php unset($this->_sections['vars']);
$this->_sections['vars']['name'] = 'vars';
$this->_sections['vars']['loop'] = is_array($_loop=((is_array($_tmp=$this->_tpl_vars['_debug_keys'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['vars']['show'] = true;
$this->_sections['vars']['max'] = $this->_sections['vars']['loop'];
$this->_sections['vars']['step'] = 1;
$this->_sections['vars']['start'] = $this->_sections['vars']['step'] > 0 ? 0 : $this->_sections['vars']['loop']-1;
if ($this->_sections['vars']['show']) {
    $this->_sections['vars']['total'] = $this->_sections['vars']['loop'];
    if ($this->_sections['vars']['total'] == 0)
        $this->_sections['vars']['show'] = false;
} else
    $this->_sections['vars']['total'] = 0;
if ($this->_sections['vars']['show']):

            for ($this->_sections['vars']['index'] = $this->_sections['vars']['start'], $this->_sections['vars']['iteration'] = 1;
                 $this->_sections['vars']['iteration'] <= $this->_sections['vars']['total'];
                 $this->_sections['vars']['index'] += $this->_sections['vars']['step'], $this->_sections['vars']['iteration']++):
$this->_sections['vars']['rownum'] = $this->_sections['vars']['iteration'];
$this->_sections['vars']['index_prev'] = $this->_sections['vars']['index'] - $this->_sections['vars']['step'];
$this->_sections['vars']['index_next'] = $this->_sections['vars']['index'] + $this->_sections['vars']['step'];
$this->_sections['vars']['first']      = ($this->_sections['vars']['iteration'] == 1);
$this->_sections['vars']['last']       = ($this->_sections['vars']['iteration'] == $this->_sections['vars']['total']);
?>
                <tr class='CBFDBDebugLine CBFFrontWebController <?php echo smarty_function_cycle(array('values' => "CBFWebDebugInfo,CBFWebDebugInfo2"), $this);?>
' id="<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['type'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['key'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
">
		            <th><input name="" type="text" value="{[$<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['_debug_keys'][$this->_sections['vars']['index']])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
]}" onfocus="this.select();" /></th>
		            <td id="var<?php echo ((is_array($_tmp=$this->_sections['vars']['index'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
">
		            	<?php if (((is_array($_tmp=@CB_DEBUGGING_ESCAPE_VARS)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
		            		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$this->_tpl_vars['_debug_vals'][$this->_sections['vars']['index']])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'htmlspecialchars' => true), $this);?>

		            	<?php else: ?>
		            		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$this->_tpl_vars['_debug_vals'][$this->_sections['vars']['index']])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))), $this);?>

		            	<?php endif; ?>
		            </td>
				</tr>
			<?php endfor; else: ?>
		        <tr><td><p>no template variables assigned</p></td></tr>
		    <?php endif; ?>
		</table>

		<h2>設定情報</h2>

        <table class="CBFDBDebugs">
		    <?php unset($this->_sections['config_vars']);
$this->_sections['config_vars']['name'] = 'config_vars';
$this->_sections['config_vars']['loop'] = is_array($_loop=((is_array($_tmp=$this->_tpl_vars['_debug_config_keys'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['config_vars']['show'] = true;
$this->_sections['config_vars']['max'] = $this->_sections['config_vars']['loop'];
$this->_sections['config_vars']['step'] = 1;
$this->_sections['config_vars']['start'] = $this->_sections['config_vars']['step'] > 0 ? 0 : $this->_sections['config_vars']['loop']-1;
if ($this->_sections['config_vars']['show']) {
    $this->_sections['config_vars']['total'] = $this->_sections['config_vars']['loop'];
    if ($this->_sections['config_vars']['total'] == 0)
        $this->_sections['config_vars']['show'] = false;
} else
    $this->_sections['config_vars']['total'] = 0;
if ($this->_sections['config_vars']['show']):

            for ($this->_sections['config_vars']['index'] = $this->_sections['config_vars']['start'], $this->_sections['config_vars']['iteration'] = 1;
                 $this->_sections['config_vars']['iteration'] <= $this->_sections['config_vars']['total'];
                 $this->_sections['config_vars']['index'] += $this->_sections['config_vars']['step'], $this->_sections['config_vars']['iteration']++):
$this->_sections['config_vars']['rownum'] = $this->_sections['config_vars']['iteration'];
$this->_sections['config_vars']['index_prev'] = $this->_sections['config_vars']['index'] - $this->_sections['config_vars']['step'];
$this->_sections['config_vars']['index_next'] = $this->_sections['config_vars']['index'] + $this->_sections['config_vars']['step'];
$this->_sections['config_vars']['first']      = ($this->_sections['config_vars']['iteration'] == 1);
$this->_sections['config_vars']['last']       = ($this->_sections['config_vars']['iteration'] == $this->_sections['config_vars']['total']);
?>
                <tr class='CBFDBDebugLine CBFFrontWebController <?php echo smarty_function_cycle(array('values' => "CBFWebDebugInfo,CBFWebDebugInfo2"), $this);?>
' id="<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['type'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['key'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
">
		            <th>{[#<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['_debug_config_keys'][$this->_sections['config_vars']['index']])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
#]}</th>
		            <td id="cvar<?php echo ((is_array($_tmp=$this->_sections['vars']['index'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
">
		            	<?php if (((is_array($_tmp=@CB_DEBUGGING_ESCAPE_VARS)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
		            		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$this->_tpl_vars['_debug_config_vals'][$this->_sections['config_vars']['index']])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'htmlspecialchars' => true), $this);?>

		            	<?php else: ?>
		            		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$this->_tpl_vars['_debug_config_vals'][$this->_sections['config_vars']['index']])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))), $this);?>

		            	<?php endif; ?>
		            </td>
				</tr>
		    <?php endfor; else: ?>
		        <tr><td><p>no config vars assigned</p></td></tr>
		    <?php endif; ?>
		</table>
		</div>
	</div>
