<?php /* Smarty version 2.6.27, created on 2017-03-28 16:20:16
         compiled from admin/template_edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', 'admin/template_edit.html', 1, false),array('modifier', 'explode', 'admin/template_edit.html', 6, false),array('block', 'form', 'admin/template_edit.html', 29, false),array('function', 'form_error', 'admin/template_edit.html', 31, false),array('function', 'form_label', 'admin/template_edit.html', 37, false),array('function', 'input_html', 'admin/template_edit.html', 39, false),)), $this); ?>
<?php if (((is_array($_tmp=$this->_tpl_vars['defaults']['category']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
	<?php $this->assign('title', 'Tempalte Edit'); ?>
<?php else: ?>
	<?php $this->assign('title', 'Tempalte Add'); ?>
<?php endif; ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/header.html", 'smarty_include_vars' => array('title' => ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'class' => "",'css' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")),'js' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script>
	$(document).ready(function() {
		$('.frmEdit').colorErrorInputs();
	});
</script>

<div id="main">
	<ol class="breadcrumbs">
		<li><a href="/admin/">Home</a></li>
		<li><a href="/admin/category_list">Tempalte List</a></li>
		<li><em><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</em></li>
	</ol>
	<!--  .breadcrumbs -->

	<section class="section" id="top">
		<h1 class="headline"><span><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</span></h1>
		
		<?php $this->_tag_stack[] = array('form', array('class' => 'frmEdit')); $_block_repeat=true;smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
			
			<?php echo smarty_function_form_error(array(), $this);?>


			<table class="sheet mb20">
				<tbody>
					
					<tr>
						<th class="w150px"><?php echo smarty_function_form_label(array('name' => "category[name]"), $this);?>
</th>
						<td>
							<?php echo smarty_function_input_html(array('name' => "category[name]"), $this);?>

						</td>
					</tr>
					<tr>
						<th><?php echo smarty_function_form_label(array('name' => "category[body]"), $this);?>
</th>
						<td class="td_body">
							<?php echo smarty_function_input_html(array('name' => "category[body]"), $this);?>

						</td>
					</tr>
				</tbody>
			</table>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/form_edit_buttons.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>

	</section>

<!-- / .section -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/copyright.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
<!-- / #main -->

<style type="text/css">
	textarea{
		height: 500px;
	}
</style>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>