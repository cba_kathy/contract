<?php /* Smarty version 2.6.27, created on 2017-05-04 16:17:42
         compiled from admin/contract_edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', 'admin/contract_edit.html', 1, false),array('modifier', 'explode', 'admin/contract_edit.html', 6, false),array('modifier', 'allow_html', 'admin/contract_edit.html', 63, false),array('block', 'form', 'admin/contract_edit.html', 29, false),array('function', 'form_error', 'admin/contract_edit.html', 30, false),array('function', 'input_html', 'admin/contract_edit.html', 39, false),)), $this); ?>
<?php if (((is_array($_tmp=$this->_tpl_vars['defaults']['contract']['contract_id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
	<?php $this->assign('title', 'Contract Edit'); ?>
<?php else: ?>
	<?php $this->assign('title', 'Contract Add'); ?>
<?php endif; ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/header.html", 'smarty_include_vars' => array('title' => ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'class' => "",'css' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")),'js' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script>
	$(document).ready(function() {
		$('.frmEdit').colorErrorInputs();
	});
</script>

<div id="main">
	<ol class="breadcrumbs">
		<li><a href="/admin/">Home|</a></li>
		<li><a href="/admin/contract_list">Contract List</a></li>
		<li><em><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</em></li>
	</ol>
	<!--  .breadcrumbs -->

	<section class="section" id="top">
		<h1 class="headline"><span><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</span></h1>

		<?php $this->_tag_stack[] = array('form', array('class' => 'frmEdit','id' => 'msform')); $_block_repeat=true;smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
			<?php echo smarty_function_form_error(array(), $this);?>


			<ul id="progressbar">
	            <li class="active">Choose template, staff</li>
	            <li>Fill in the form</li>
	            <li>Confirm info</li>
	        </ul>

			<fieldset id="first">
				<span class="label">Contract name: </span><?php echo smarty_function_input_html(array('name' => "contract[name]",'placeholder' => 'Contract name'), $this);?>
<br><br>
				<span class="label">Staff name: </span><?php echo smarty_function_input_html(array('name' => "contract[staff_tmp]",'id' => 'ajax','list' => "json-datalist",'placeholder' => 'Select staff name'), $this);?>
</br>
				<datalist id="json-datalist"></datalist>
				<div style="display: none;">
				  	<input type="text" name="contract[staff_id]" id="ajax-hidden">
				</div>
				<br>
				
				<span class="label">Template: </span><?php echo smarty_function_input_html(array('name' => "contract[category_id]",'class' => 'number category_id'), $this);?>
<br><br>
				<span class="label">Signed date: </span><?php echo smarty_function_input_html(array('name' => "contract[signed_date]",'placeholder' => 'Signed date','class' => 'datepicker signed_date'), $this);?>
<br><br>
				<span class="label">End date: </span><?php echo smarty_function_input_html(array('name' => "contract[end_date]",'placeholder' => 'End date','class' => 'datepicker end_date'), $this);?>
<br>
				<p class="margin20"></p>
				<p class="error errors1"></p>
				<button type="button" name="next" class="button blue2 mb0 next_btn next_btn1">Next</button>
			</fieldset>

			<?php if (((is_array($_tmp=$this->_tpl_vars['form_status'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) == 'confirm'): ?>
					<fieldset class="tr_confirm">
			<?php else: ?>
					<fieldset>
            <?php endif; ?>
				
				<?php if (! ((is_array($_tmp=$this->_tpl_vars['contract']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
                    <div class="td_body" style="padding-left: 60px;">
						<div class="category_body"><div class="contract_wrap"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['category_body'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('allow_html', true, $_tmp) : smarty_modifier_allow_html($_tmp)); ?>
</div></div>
						<div style="display: none;">
							<div class="contract_wrap"><?php echo smarty_function_input_html(array('name' => "contract[body]",'class' => 'textarea_body'), $this);?>
</div>
						</div>
					</div>
                <?php else: ?>
                	<div class="td_body category_body" style="padding-left: 60px; margin-right: 20px;">
						<div class="contract_wrap"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['category_body'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('allow_html', true, $_tmp) : smarty_modifier_allow_html($_tmp)); ?>
</div>
					</div>
					<div style="display: none; padding-left: 60px;">
						<div class="contract_wrap"><?php echo smarty_function_input_html(array('name' => "contract[body]",'class' => 'textarea_body'), $this);?>
</div>
					</div>
                <?php endif; ?>
                <p class="margin20"></p>
                <p class="error errors2"></p>
				<button type="button" name="previous" class="button blue2 mb0 pre_btn">Previous</button>
				<button type="button" name="next" class="button blue2 mb0 next_btn next_btn2">Next</button>

			
			</fieldset>

			<fieldset class="fconfirm">
				<div class="print"></div>
				<p class="margin20"></p>
				<button type="button" name="previous" class="button blue2 mb0 pre_btn">Previous</button>
				<button type="submit" class="button blue2 mb0 submit_btn">Submit</button>
			</fieldset>
			
		<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>




	</section>
<!-- / .section -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/copyright.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
<!-- / #main -->

<script type="text/javascript">
	$("button[type='submit']").click(function(){
		getIput();
		
		var inputValues = {};
	    $('.td_body :input').each(function() {
	        var type = $(this).attr("type");
	        var name = $(this).attr("name");
	        if ((type == "checkbox" || type == "radio") && this.checked) {
	            inputValues[name]= $(this).val();
	        }
	        else if (type != "button" || type != "submit") {
	        	inputValues[name]= $(this).val();
	        }
	    });
	     $('.textarea_body').val(JSON.stringify(inputValues));
	     $('.fconfirm div').html('');
	});

function callAjax(param,url,callback){
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'html',
        data: param,
        success: function(res) {
            callback(res);
        },
        fail: function(res) {
            callback(res);
        },
        error: function(status, exception){
		},
    });
}

</script>

<script type="text/javascript">
var dataList = document.getElementById('json-datalist');
var input = document.getElementById('ajax');

var request = new XMLHttpRequest();
request.onreadystatechange = function(response) {
  if (request.readyState === 4) {
    if (request.status === 200) {

      var jsonOptions = JSON.parse(request.responseText);

      $.each(jsonOptions, function(index, item) {
        var option = document.createElement('option');
        $(option).attr('data-value', index);
        option.text = item;
        dataList.appendChild(option);
      });
      
      input.placeholder = "Select staff name";
    } else {
      input.placeholder = "Couldn't load datalist options.";
    }
  }
};

input.placeholder = "Loading options...";
request.open('POST', 'http://'+document.domain+'/ajax/getUser', true);
request.send();

</script>


<script type="text/javascript">
	function getIput(){
	    var options = $('#json-datalist option'),
	        hiddenInput = $('#ajax-hidden'),
	        inputValue = $('#ajax').val();
	       
	        hiddenInput.attr('value', '');
	    for(var i = 0; i < options.length; i++) {
	        var option = options[i];
	        if(option.innerText === inputValue) {
	            hiddenInput.val(option.getAttribute('data-value'));
	            break;
	        }
	    }
	}

	function nextStep(k, errorValid){
		if(errorValid == false){
			k.parent().next().fadeIn('slow');
			k.parent().css({'display':'none'});
			$('.active').next().addClass('active');
		}
    }
    function getTemplate(){
    	var that = $('.category_id');
	    getIput();
	    var param = {
            _type : "getCategoryTemplate",
            category_id : that.val(),
            staff_id : $('#ajax-hidden').val(),
            table : "category",
        };
	    var url = '/ajax/';
	 
	    callAjax(param, url, function(res) {
	        $('.category_body').html(res);
	        $("input.datepicker").datepicker({
				dateFormat : "dd/mm/yy"
			});
	    });
    }
</script>


<script type="text/javascript">
	$(document).ready(function(){
		$("#first input").css({
		    'width': ($("#first select").width() +16 + 'px')
		 });
		$(".next_btn").click(function(){//Function runs on NEXT button click 
			var that = $(this);
			if($(this).hasClass('next_btn1')){
				getIput();
				var errorValid = false;
				var staffname = $('input[name="contract[name]"]').val();
				var staff_id = $('#ajax-hidden').val();
				var category_id = $('select[name="contract[category_id]"]').val();
				var signed_date = new Date($('input[name="contract[signed_date]"]').val());
				var end_date = new Date($('input[name="contract[end_date]"]').val());

				if(staffname=='' || staff_id=='' || category_id=='' || (end_date - signed_date < 0)){
					errorValid = true;
				}
				//check exist contract with staff name and categry
				else{
					<?php if (! empty ( ((is_array($_tmp=$this->_tpl_vars['contract'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) )): ?>
						var id = <?php echo ((is_array($_tmp=$this->_tpl_vars['contract']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
;
					<?php else: ?>
						var id = '';
					<?php endif; ?>
					var param = {
			            _type : "checkExistContract",
			            category_id : category_id,
			            staff_id : staff_id,
			            table : "contract",
			            id:id,
			        };
				    var url = '/ajax/';
				    callAjax(param, url, function(res) {
				    	if(res == 0){
				    		$('.error.errors1').text('The contract category and staff name has been existed, please choose Others.');
				    		errorValid = true;
							return false;
				    	}else{
				    		getTemplate();
				    		nextStep(that, errorValid);
				    	}
				    });
				}
				
				if(errorValid == true){
					$('.error.errors1').html('The data entered is incorrect.');
					return false;
				}
			}


			if($(this).hasClass('next_btn2')){
				var errorValid = false;
				$('.td_body :input').each(function() {
			        if (! $(this).is('[readonly]') && ($(this).attr('name') !='labor-book') && ($(this).attr('name') !='contract[body]')) {
			        	if($(this).val() ==''){
			        		$(this).css('border', "1px red solid");
			        		errorValid = true;
			        	}
			        }
			     });
				if(errorValid == true){
					$('.error.errors2').html('Please complete all information.');
					return false;
				}

				var $orginal = $('.td_body');
				var $cloned = $orginal.clone();

				var $originalSelects = $orginal.find('select');
				$cloned.find('select').each(function(index, item) {
				     $(item).val( $originalSelects.eq(index).val() );
				});
				var $originalTextareas = $orginal.find('textarea');

				$cloned.find('textarea').each(function(index, item) {
					$(item).val($originalTextareas.eq(index).val());
				}); 
				$cloned.appendTo('.fconfirm div');

				$('.fconfirm input').attr('disabled','disabled');
				$('.fconfirm select').attr('disabled','disabled');
				$('.fconfirm textarea').attr('disabled','disabled');
				nextStep($(this), errorValid);
			}
			
			
		});
		
		$(".pre_btn").click(function(){            //Function runs on PREVIOUS button click 
			$(this).parent().prev().find('.error').html('');
			$(this).parent().prev().fadeIn('slow');
			$('.fconfirm div').html('');
			$(this).parent().css({'display':'none'});
			$('.active:last').removeClass('active');
		});

		$(window).keydown(function(event){
		    if(event.keyCode == 13) {
		      event.preventDefault();
		      $('.next_btn2').click();
		    }
		});

	});

</script>
<style type="text/css">
	.fconfirm input[style], .fconfirm select[style], .fconfirm textarea[style]{
		border: 1px solid #999 !important;
	}
	#first select{
		padding: 7px;
	}
</style>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>