<?php /* Smarty version 2.6.27, created on 2017-05-04 16:16:50
         compiled from _includes/admin/header.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', '_includes/admin/header.html', 7, false),)), $this); ?>
<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=1000">
<meta name="robots" content="noindex,nofollow">
<title><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</title>
<link rel="stylesheet" href="../css/common.css" media="all">
<link rel="stylesheet" href="../css/admin.css" media="all">
<link rel="stylesheet" href="../css/font-awesome.min.css" media="all">
<link rel="stylesheet" href="../css/formValidator/validationEngine.jquery.css" media="all">
<link rel="stylesheet" href="../css/formValidator/validationTemplate.css" media="all">
<link rel="stylesheet" href="../css/colorbox.css" media="all">
<link rel="stylesheet" href="../css/jquery-ui.css">


<link rel="stylesheet" href="/css/admin_custom.css" media="all">
<link rel="stylesheet" type="text/css" href="/css/contract/common.css" media="all">
<link rel="stylesheet" type="text/css" href="/css/contract/style.css" media="all">
<link rel="stylesheet" type="text/css" href="/css/contract/print.css" media="print">


<?php $_from = ((is_array($_tmp=$this->_tpl_vars['css_link'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
<?php if (((is_array($_tmp=$this->_tpl_vars['item'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?><link rel="stylesheet" href="<?php echo ((is_array($_tmp=$this->_tpl_vars['item'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
<?php $_from = ((is_array($_tmp=$this->_tpl_vars['css'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
<?php if (((is_array($_tmp=$this->_tpl_vars['item'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?><link rel="stylesheet" href="/css/<?php echo ((is_array($_tmp=$this->_tpl_vars['item'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><?php endif; ?>
<?php endforeach; endif; unset($_from); ?>



<link rel="shortcut icon" type="image/x-icon" href="../img/favicon.ico">
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/jquery.ui.datepicker-ja.min.js"></script>

<script src="../js/formValidator/jquery.validationEngine.js"></script>
<script src="../js/formValidator/jquery.validationEngine-ja.js"></script>
<script src="../js/jquery.colorbox.js"></script>
<script src="../js/script.js"></script>
<script src="../js/admin.js"></script>

<script src="/js/contract/script1.js"></script>


<!--[if lt IE 9]>
<script src="/js/html5shiv-printshiv.js"></script>
<![endif]-->
<?php $_from = ((is_array($_tmp=$this->_tpl_vars['js'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
<?php if (((is_array($_tmp=$this->_tpl_vars['item'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?><script src="/js/<?php echo ((is_array($_tmp=$this->_tpl_vars['item'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
" ></script><?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
<?php $_from = ((is_array($_tmp=$this->_tpl_vars['js_link'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
<?php if (((is_array($_tmp=$this->_tpl_vars['item'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?><script src="<?php echo ((is_array($_tmp=$this->_tpl_vars['item'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
" ></script><?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
</head>
<body id="pageAdminHome">
<div id="headerBar">
	<div id="headerBarInner" class="clearfix">
		<p class="leftBox mr20">CYBRIDGE ASIA</p>
		<ul id="headerBarMenu" class="clearfix rightBox">
			<li class="leftBox"><a href="#"></a></li>
		</ul>
	</div>
</div>
<!-- / #headerBar -->

<noscript>
	<p id="noScript">JavaScriptが無効です。正しくサイトを表示するためには、JavaScriptを有効にする必要があります。</p>
</noscript>

<div id="wrapper">
	<div id="container">
		<header id="header">
			<a href="/admin/" id="logo"><img src="../img/cybridge/logo.gif" alt="CB-STANDARD"></a>
			<hgroup>
				<h2>ようこそ、<span><?php echo ((is_array($_tmp=$_SESSION['default']['admin']['fullname'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</span>様</h2>
			</hgroup>
			
			<ul id="headerMenu">
				<li><a href="/admin/logout" class="logout"><i class="fa fa-sign-in" aria-hidden="true"></i>Logout</a></li>
			</ul>
		</header>
		<!-- / #header -->

		<div id="contents" class="clearfix">