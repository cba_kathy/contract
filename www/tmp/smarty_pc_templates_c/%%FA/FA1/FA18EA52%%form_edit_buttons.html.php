<?php /* Smarty version 2.6.27, created on 2017-01-05 10:46:00
         compiled from _includes/admin/form_edit_buttons.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', '_includes/admin/form_edit_buttons.html', 2, false),)), $this); ?>
<div style="width:310px;" class="clearfix auto">
<?php if (((is_array($_tmp=$this->_tpl_vars['form_status'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) == 'confirm'): ?>
	<button type="submit" class="leftBox button gray2 mr10 mb0" name="submit[back]">戻る</button>
	<button type="submit" class="rigthBox button blue2 mb0" name="submit[done]">確認画面へ</button>
<?php else: ?>
	<button type="submit" class="rigthBox button blue2 mb0" name="submit[confirm]">確定</button>
<?php endif; ?>
</div>