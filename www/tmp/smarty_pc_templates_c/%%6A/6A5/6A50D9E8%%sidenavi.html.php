<?php /* Smarty version 2.6.27, created on 2017-05-04 16:16:50
         compiled from _includes/admin/sidenavi.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', '_includes/admin/sidenavi.html', 7, false),)), $this); ?>
<nav id="side">
	<ul id="navi">

		<li>
			<span class="parent">Contract</span>
			<ul class="pull">
				<li <?php if (preg_match ( "/^\/admin\/contract_list/" , ((is_array($_tmp=$_SERVER['REQUEST_URI'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) )): ?> class="active" <?php endif; ?>><a href="/admin/contract_list">Contract List</a></li>
				<?php if (((is_array($_tmp=$_SESSION['default']['check_action']['contract_edit'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) == 1): ?>
				<li <?php if (preg_match ( "/^\/admin\/contract_edit/" , ((is_array($_tmp=$_SERVER['REQUEST_URI'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) )): ?> class="active" <?php endif; ?>><a href="/admin/contract_edit">Contract Add</a></li>
				<?php endif; ?>
			</ul>
		</li>
		
		<li>
			<ul class="pull">
				<li <?php if (preg_match ( "/^\/admin\/template_/" , ((is_array($_tmp=$_SERVER['REQUEST_URI'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) )): ?> class="active" <?php endif; ?>><a href="/admin/template_list">Template List</a></li>
			</ul>
		</li>

		<li>
			<ul class="pull">
				<li <?php if (preg_match ( "/^\/admin\/staff_/" , ((is_array($_tmp=$_SERVER['REQUEST_URI'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) )): ?> class="active" <?php endif; ?>><a href="/admin/staff_list">Staff List</a></li>
			</ul>
		</li>
		<?php if (((is_array($_tmp=$_SESSION['default']['admin']['role'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) == 1): ?>
		<li>
			<ul class="pull">
				<li <?php if (preg_match ( "/^\/admin\/admin_/" , ((is_array($_tmp=$_SERVER['REQUEST_URI'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) )): ?> class="active" <?php endif; ?>><a href="/admin/admin_list">Admin List</a></li>
			</ul>
		</li>
		<?php endif; ?>
		
	</ul>
	<!-- / #navi -->
</nav>
<!-- / #side -->