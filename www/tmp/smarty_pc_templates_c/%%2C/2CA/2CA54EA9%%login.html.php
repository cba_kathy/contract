<?php /* Smarty version 2.6.27, created on 2017-01-03 13:03:50
         compiled from admin/login.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'form', 'admin/login.html', 21, false),array('function', 'form_error', 'admin/login.html', 22, false),array('function', 'input_html', 'admin/login.html', 28, false),array('modifier', 'cb_escape', 'admin/login.html', 23, false),)), $this); ?>
<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=1000">
<meta name="robots" content="noindex,nofollow">
<title>ログイン | <?php echo $this->_config[0]['vars']['SITE_TITLE']; ?>
</title>
<link rel="stylesheet" href="/css/common.css" media="all">
<link rel="stylesheet" href="/css/admin.css" media="all">
<link rel="shortcut icon" type="image/x-icon" href="../img/favicon.ico">
<script src="../js/jquery.min.js"></script>
<script src="../js/admin.js"></script>
<!--[if lt IE 9]>
<script src="../js/html5shiv-printshiv.js"></script>
<![endif]-->
</head>
<body id="pageAdminLogin">
<div id="loginWrapper" class="radius5">
	<h1 class="mb20"><a href="/admin/" id="logo"><img src="../img/cybridge/logo.gif" alt="CB-STANDARD"></a></h1>

	<?php $this->_tag_stack[] = array('form', array('id' => 'loginForm','class' => 'mb20')); $_block_repeat=true;smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
		<?php echo smarty_function_form_error(array(), $this);?>

		<?php if (((is_array($_tmp=$this->_tpl_vars['error_login'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
		<p class="alert"><?php echo ((is_array($_tmp=$this->_tpl_vars['error_login'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</p>
		<?php endif; ?>
		<p class="mb5">
			<label>
			<?php echo smarty_function_input_html(array('name' => "admin[username]",'class' => "loginInput form-control",'placeholder' => "ユーザー名",'onfocus' => "this.placeholder = ''",'onblur' => "this.placeholder = 'ユーザー名'"), $this);?>
</label>
		</p>
		<p class="mb5">
			<label>
			<?php echo smarty_function_input_html(array('name' => "admin[password]",'class' => "loginInput form-control",'placeholder' => "パスワード",'onfocus' => "this.placeholder = ''",'onblur' => "this.placeholder = 'パスワード'"), $this);?>

			</label>
		</p>
		<p class="mb10">
			<!-- <span class="checkbox">
			<input type="checkbox" id="rememberme">
			<label for="rememberme">ログイン状態を保存する</label>
			</span> -->
			<?php echo smarty_function_input_html(array('name' => "admin[remember]"), $this);?>

		</p>
		<div class="margin-top-10">
		</div>
		<p class="submit">
				<button type="submit" class="button login auto mb0" id="loginBtn">ログインする</button>
		</p>
		
	<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>

	<div class="boxText">
		<p id="nav" class="textLink"><a href="/admin/">管理画面トップはこちらから</a></p>
		<p class="text">※ログインボタンをクリックすると、エラー時のシェイクアニメーションが作動するようになっています。お好みで調整して利用してください。</p>
	</div>
</div>
</body>
</html>