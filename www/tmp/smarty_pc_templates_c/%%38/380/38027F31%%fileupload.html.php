<?php /* Smarty version 2.6.27, created on 2016-12-28 10:47:17
         compiled from _includes/fileupload.html */ ?>
<!-- Modal Image-->
<div class="modal-container">
	<div class="modal fade" role="dialog" id="modal-crop">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
			<form action="/ajax/fileupload" class="image-form" enctype="multipart/form-data" method="post">
	      
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Crop an image</h4>
			</div>

			<div class="modal-body">
				<div class="image-inputs">
					<button type="button" class="btn btn-primary" id="btnBrowser">
						<span>Browse Image...</span>
					</button>
					<input id="fileUpload" type="file" name="file" class="noDisplay">
				</div>
				<!-- Crop and preview -->
				<div class="mt20">
					<div class="image-wrapper leftBox mb20"></div>
					<div class="image-preview rightBox"></div>
				</div>
			</div>

			<div class="modal-footer clear">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button class="btn btn-primary" id="btnCrop" type="submit">Crop</button>
			</div>
			</form>
	    
	    </div>

	  </div>
	</div>
	
    </div>

<!-- Modal Upload-->
<div class="modal-container">
	<div class="modal fade" role="dialog" id="modal-upload">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Uploading...</h4>
	      </div>
	      <div class="modal-body">
            <div class="error"></div>
	        <div class="progress">
                <div class="progress-bar progress-bar-success"></div>
            </div>
			
	      </div>
	    </div>

	  </div>
	</div>
</div>