<?php /* Smarty version 2.6.27, created on 2016-12-28 10:47:20
         compiled from admin/user_contract_list.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', 'admin/user_contract_list.html', 2, false),array('modifier', 'explode', 'admin/user_contract_list.html', 2, false),array('modifier', 'allow_html', 'admin/user_contract_list.html', 46, false),array('block', 'form', 'admin/user_contract_list.html', 17, false),array('function', 'form_label', 'admin/user_contract_list.html', 22, false),array('function', 'input_html', 'admin/user_contract_list.html', 23, false),)), $this); ?>
<?php $this->assign('title', 'user_contract List'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/header.html", 'smarty_include_vars' => array('title' => ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'css' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")),'js' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="main">
	<ol class="breadcrumbs">
		<li><a href="/admin/">管理画面トップ</a></li>
		<li><em><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</em></li>
	</ol>
	<!-- / .breadcrumbs -->

	<section class="section">
		<h1 class="headline"><span><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</span></h1>
		<p class="linkList"><a href="/admin/user_contract_edit">Add New user_contract</a></p>
	<?php $this->_tag_stack[] = array('form', array('class' => 'searchBox pv10')); $_block_repeat=true;smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
		<table class="sheet">
			<tbody>
				
				<tr>
					<th><?php echo smarty_function_form_label(array('name' => 'id'), $this);?>
</th>
					<td><?php echo smarty_function_input_html(array('name' => 'id','class' => 'number'), $this);?>
</td>
				</tr>
				<tr>
					<th><?php echo smarty_function_form_label(array('name' => 'user_id'), $this);?>
</th>
					<td><?php echo smarty_function_input_html(array('name' => 'user_id','class' => 'number'), $this);?>
</td>
				</tr>
				<tr>
					<th><?php echo smarty_function_form_label(array('name' => 'contract_id'), $this);?>
</th>
					<td><?php echo smarty_function_input_html(array('name' => 'contract_id','class' => 'number'), $this);?>
</td>
				</tr>
				<tr>
					<td colspan="4" class="pa10">
						<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/form_search_buttons.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
					</td>
				</tr>
			</tbody>
		</table>
	<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
	</section>

	<section class="section">

	<?php if (((is_array($_tmp=$this->_tpl_vars['user_contract_p']['rows'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
		<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['user_contract_p']['pager']['header'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('allow_html', true, $_tmp) : smarty_modifier_allow_html($_tmp)); ?>

		<table class="sheet tblList">
			<thead>
				<tr>
					
					<th><a href="/admin/user_contract_list?sort_by=id&<?php echo ((is_array($_tmp=$this->_tpl_vars['sorts']['query'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><span>id</span></a>
					<th><a href="/admin/user_contract_list?sort_by=user_id&<?php echo ((is_array($_tmp=$this->_tpl_vars['sorts']['query'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><span>user_id</span></a>
					<th><a href="/admin/user_contract_list?sort_by=contract_id&<?php echo ((is_array($_tmp=$this->_tpl_vars['sorts']['query'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><span>contract_id</span></a>
					<th><span>Action</span></th>
				</tr>
			</thead>
			<tbody>
			<?php $_from = ((is_array($_tmp=$this->_tpl_vars['user_contract_p']['rows'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['row']):
?>
				<tr>
					
					<td><?php echo ((is_array($_tmp=$this->_tpl_vars['row']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</td>
					<td><?php echo ((is_array($_tmp=$this->_tpl_vars['row']['user_id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</td>
					<td><?php echo ((is_array($_tmp=$this->_tpl_vars['row']['contract_id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</td>
					<td class="operate">
						<a href="/admin/user_contract_edit?id=<?php echo ((is_array($_tmp=$this->_tpl_vars['row']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
" class="edit block">Edit</a>
						<a href="#" class="delete confirm block" data-alert="本当に削除してもよろしいですか？">Delete</a>
					</td>
				</tr>
			<?php endforeach; endif; unset($_from); ?>
			</tbody>
		</table> <!-- / .sheet -->
		<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['user_contract_p']['pager']['html'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('allow_html', true, $_tmp) : smarty_modifier_allow_html($_tmp)); ?>

		<!-- / .pagination -->
	<?php else: ?>
		<p class="alert"><?php echo $this->_config[0]['vars']['RESULT_NOT_FOUND']; ?>
</p>
	<?php endif; ?>

	</section>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>