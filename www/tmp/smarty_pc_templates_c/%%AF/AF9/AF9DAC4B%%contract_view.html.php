<?php /* Smarty version 2.6.27, created on 2017-05-04 18:07:36
         compiled from admin/contract_view.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', 'admin/contract_view.html', 1, false),array('modifier', 'explode', 'admin/contract_view.html', 4, false),array('modifier', 'allow_html', 'admin/contract_view.html', 43, false),array('block', 'form', 'admin/contract_view.html', 21, false),array('function', 'form_error', 'admin/contract_view.html', 23, false),)), $this); ?>
<?php if (((is_array($_tmp=$this->_tpl_vars['defaults']['contract']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
	<?php $this->assign('title', 'Contract View'); ?>
<?php endif; ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/header.html", 'smarty_include_vars' => array('title' => ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'class' => "",'css' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")),'js' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "jquery.inlineStyler.min.js") : explode($_tmp, "jquery.inlineStyler.min.js")))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="main">
	<ol class="breadcrumbs">
		<li><a href="/admin/">Home</a></li>
		<li><a href="/admin/contract_list">Contract List</a></li>
		<li><em><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</em></li>
	</ol>
	<!--  .breadcrumbs -->

	<section class="section" id="top">
		<h1 class="headline"><span><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</span></h1>
		
		<?php $this->_tag_stack[] = array('form', array('class' => 'frmEdit')); $_block_repeat=true;smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
			
			<?php echo smarty_function_form_error(array(), $this);?>


			<table class="sheet mb20">
				<tbody>
					<tr>
						<th class="th_small">Name</th>
						<td>
							<?php echo ((is_array($_tmp=$this->_tpl_vars['contract']['name'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>

						</td>
					</tr>
					<tr>
						<th>Tempalte</th>
						<td>
							<?php echo ((is_array($_tmp=$this->_tpl_vars['category'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>

						</td>
					</tr>
					<tr>
						<th>Content</th>
						<td class="td_body print">
							<div class="contract_wrap">
							<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['category_body'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('allow_html', true, $_tmp) : smarty_modifier_allow_html($_tmp)); ?>

							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div style="width:550px;" class="clearfix auto">
				<button type="button" class="rigthBox button blue2 auto mb0" name="btn_print" onClick="printQuestion('td_body')"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
				<button type="button" class="rigthBox button blue2 auto mb0"  onClick="exportDoc(event)"><i class="fa fa-file-word-o"></i> Export To Word</button>
			</div>
		<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>

	</section>
<!-- / .section -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/copyright.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</div>
<!-- / #main -->
<style type="text/css">
	textarea{
		height: 500px;
	}
	.italic span{
		font-style: italic;
		font-weight: 85%;
	}
	staff-name{
		font-weight: bold !important;
		text-transform: uppercase;
	}
</style>

<script type="text/javascript">
	$("button[type='submit']").click(function(){
		var inputValues = {};
	    $('.td_body :input').each(function() {
	        var type = $(this).attr("type");
	        var name = $(this).attr("name");
	        if ((type == "checkbox" || type == "radio") && this.checked) {
	            inputValues[name]= $(this).val();
	        }
	        else if (type != "button" || type != "submit") {
	        	inputValues[name]= $(this).val();
	        }
	    });
	    $('.textarea_body').val(JSON.stringify(inputValues));
	});

	function printQuestion(PrintClass) {
		var temp = $('body').html();
		$('body').html('<div class="td_body">'+$('.'+PrintClass).html() +'</div>');
		$('.'+PrintClass).addClass("print");
        window.print();
	    $('body').html(temp);
    }

    $(document).ready(function(){
    	var selects = $('.print select');
    	$.each( selects, function( key, value ) {
    		if( $(this).val() ==''){
    			$(this).text('');
    		}
    	});
    });

</script>
<script>
	$(document).ready(function() {
		$('input[type="text"], select').each(function(){
			if($(this).attr('name') == 'staff-name'){
				$(this).replaceWith("<span>" + this.value + " </span>");
			}else{
				$(this).replaceWith("<span style='font-weight: normal !important; margin-left: 8px;'>" + this.value + " </span>");
			}
			
		});
	});
</script>


 <form name="proposal_form" action="/ajax/exportDoc" method="post"" style="display: none;" id="proposal_form">
 	<textarea name="html" id="html"></textarea>
 	<input name="contract_name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['contract']['name'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"/>
  <input type="submit" name="submit_docs" value="Export as MS Word" class="input-button" />
</form>

<script type="text/javascript">
	function exportDoc(e){
		e.preventDefault();
		var strAlert  = 'Are you sure you want to export this contract to a doc file?';
		if(confirm(strAlert)){
			var temp = $('.td_body').html();
			$('.td_body').inlineStyler( cssRules);
			var html = $('.td_body').html();
			$('#html').val(html);
			$('#proposal_form').submit();
			$('.td_body').html(temp);
		}
	}
</script>


<script type="text/javascript">
	var cssRules = {
		'propertyGroups' : {
			"*": ["height", "margin", "padding", "width", "font-family", "font-style", "font-size", "font-weight", "letter-spacing", "line-height", "text-align", "text-decoration", "text-transform", "white-space", "word-spacing", "word-wrap", "vertical-align", "background", "background-image", "background-position", "background-repeat", "bottom", "clear", "display", "float", "list-style-type", "padding-left","padding-right","padding-top","padding-bottom", 'border'],
			'block' : ['margin', 'padding'],
			'inline' : ['color','display','padding-bottom','font-style','font-weight','text-transform'],
			'headings' : ['font-size', 'font-family','margin-bottom','text-align','font-weight']
		},
		'elementGroups' : {
			 "*": ["A", "B", "BASEFONT", "CENTER", "COL", "DD", "DIV", "DL", "DT", "EM", "I", "IMG", "LABEL", "LI", "LINK", "OL", "P","STRONG", "SUB","SUP", "TD", "TFOOT", "TH", "THEAD", "TT", "U", "UL"],
			'block' : ['DIV', 'P', "TR", "TABLE"], 
			'inline' : ['SPAN', 'STAFF-NAME'], 
			'headings' : ['H1', 'H2', 'H3', 'H4', 'H5', 'H6'],
		},
	}
</script>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>