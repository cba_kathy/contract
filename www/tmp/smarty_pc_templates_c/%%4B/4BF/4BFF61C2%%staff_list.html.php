<?php /* Smarty version 2.6.27, created on 2017-05-04 16:17:41
         compiled from admin/staff_list.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', 'admin/staff_list.html', 2, false),array('modifier', 'explode', 'admin/staff_list.html', 2, false),array('modifier', 'allow_html', 'admin/staff_list.html', 42, false),array('block', 'form', 'admin/staff_list.html', 18, false),array('function', 'form_label', 'admin/staff_list.html', 23, false),array('function', 'input_html', 'admin/staff_list.html', 24, false),)), $this); ?>
<?php $this->assign('title', 'Staff List'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/header.html", 'smarty_include_vars' => array('title' => ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'css' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")),'js' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="main">
	<ol class="breadcrumbs">
		<li><a href="/admin/">Home</a></li>
		<li><em><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</em></li>
	</ol>
	<!-- / .breadcrumbs -->

	<section class="section">
		<h1 class="headline"><span>Search Staff</span></h1>
		<div class="boxInner">
			<p class="linkList"><a href="/admin/staff_edit"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add New Staff</a></p>
			<?php $this->_tag_stack[] = array('form', array('class' => 'searchBox')); $_block_repeat=true;smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
			<table class="sheet">
				<tbody>

					<tr>
						<th><?php echo smarty_function_form_label(array('name' => 'keyword'), $this);?>
</th>
						<td><?php echo smarty_function_input_html(array('name' => 'keyword'), $this);?>
</td>
					</tr>
					<tr>
						<td colspan="4" class="pa10">
							<button type="submit" class="button blue2 auto mb0">検索する</button>
						</td>
					</tr>
				</tbody>
			</table>
		<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
		<!-- / .searchBox -->
		</div>
	</section>
	<!-- / .section -->

	<section class="section">

	<?php if (((is_array($_tmp=$this->_tpl_vars['staff_p']['rows'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
		<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['staff_p']['pager']['header'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('allow_html', true, $_tmp) : smarty_modifier_allow_html($_tmp)); ?>

		<table class="sheet tblList">
			<thead>
				<tr>
					<th class="th_small"><a href="/admin/staff_list?sort_by=id&<?php echo ((is_array($_tmp=$this->_tpl_vars['sorts']['query'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><span>ID</span></a>
					<th><a href="/admin/staff_list?sort_by=fullname&<?php echo ((is_array($_tmp=$this->_tpl_vars['sorts']['query'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><span>Full Name</span></a>
					<th><a href="/admin/staff_list?sort_by=nickname&<?php echo ((is_array($_tmp=$this->_tpl_vars['sorts']['query'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><span>Nick Name</span></a>
					<th><a href="/admin/staff_list?sort_by=position&<?php echo ((is_array($_tmp=$this->_tpl_vars['sorts']['query'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><span>Position</span></a>
					<th><a href="/admin/staff_list?sort_by=start_work_date&<?php echo ((is_array($_tmp=$this->_tpl_vars['sorts']['query'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><span>Start work date</span></a>
					<th><span>Action</span></th>
				</tr>
			</thead>
			<tbody>
			<?php $_from = ((is_array($_tmp=$this->_tpl_vars['staff_p']['rows'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['row']):
?>
				<?php if (( ((is_array($_tmp=$this->_tpl_vars['key'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) % 2 ) == 0): ?>
					<?php $this->assign('class', 'trWhite'); ?>
				<?php else: ?>
					<?php $this->assign('class', 'trGray'); ?>
				<?php endif; ?>
				<tr class="<?php echo ((is_array($_tmp=$this->_tpl_vars['class'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
">
					<td class="text-center"><?php echo ((is_array($_tmp=$this->_tpl_vars['row']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</td>
					<td><a href="/admin/contract_list?staff_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['row']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['row']['fullname'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</a></td>
					<td><?php echo ((is_array($_tmp=$this->_tpl_vars['row']['nickname'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</td>
					<td><?php echo ((is_array($_tmp=$this->_tpl_vars['positions'][$this->_tpl_vars['row']['position']])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</td>
					<td><?php echo ((is_array($_tmp=$this->_tpl_vars['row']['start_work_date'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</td>
					<td class="operate">
						<?php if (((is_array($_tmp=$_SESSION['default']['check_action']['staff_edit'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) == 1): ?>
						<a href="/admin/staff_edit?id=<?php echo ((is_array($_tmp=$this->_tpl_vars['row']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
" class="block"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</a>
						<?php endif; ?>
						<?php if (((is_array($_tmp=$_SESSION['default']['check_action']['delete'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) == 1): ?>
						<a href="#" class="ajax_delete block" data-id="<?php echo ((is_array($_tmp=$this->_tpl_vars['row']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
" data-table="staff" data-force="force"><i class="fa fa-times" aria-hidden="true"></i>Delete</a>
						<?php endif; ?>
						<a href="/admin/contract_list?staff_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['row']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
" class="block"><i class="fa fa-search" aria-hidden="true"></i>View</a>
						<a href="/admin/contract_list?staff_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['row']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
" class="block"><i class="fa fa-search" aria-hidden="true"></i>Show Contract</a>

					</td>
				</tr>
			<?php endforeach; endif; unset($_from); ?>
			</tbody>
		</table> <!-- / .sheet -->
		<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['staff_p']['pager']['html'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('allow_html', true, $_tmp) : smarty_modifier_allow_html($_tmp)); ?>

		<!-- / .pagination -->
	<?php else: ?>
		<p class="alert"><?php echo $this->_config[0]['vars']['RESULT_NOT_FOUND']; ?>
</p>
	<?php endif; ?>

	</section>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/copyright.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>