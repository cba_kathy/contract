<?php /* Smarty version 2.6.27, created on 2017-01-06 17:22:57
         compiled from admin/test.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', 'admin/test.html', 1, false),array('modifier', 'explode', 'admin/test.html', 6, false),)), $this); ?>
<?php if (((is_array($_tmp=$this->_tpl_vars['defaults']['category']['category_id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
	<?php $this->assign('title', 'Category Edit'); ?>
<?php else: ?>
	<?php $this->assign('title', 'Category Add'); ?>
<?php endif; ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/header.html", 'smarty_include_vars' => array('title' => ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'class' => "",'css' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")),'js' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script>
	$(document).ready(function() {
		$('.frmEdit').colorErrorInputs();
	});
</script>

<div id="main">

	<!--  .breadcrumbs -->

	<section class="section" id="top">
		<h1 class="headline"><span><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</span></h1>
			<table class="sheet mb20">
				<tbody>
					<tr>
						<th>Body</th>
						<td class="td_body">
							<div class="contract_wrap">
								














						<div>
							<div id="lead" class="center mb20">
								<p class="mb10"><strong>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</strong><br>
								<span class="italic f11">SOCIALIST REPUBLIC OF VIETNAM</span></p>
								<p><strong>Độc lập - Tự do - Hạnh phúc</strong><br>
								<span class="italic f11">Independence – Freedom – Happiness</span></p>
								<span>-------------------</span>
							</div>

							<p style="text-align:center"><strong>ANNEX CONTRACT: CHANGE UNIT PRICE</strong></p>

							<p style="text-align:center"><strong>No:………………</strong></p>

							<p style="text-align:center"><strong>Date:…………...</strong></p>

							<p>&nbsp;</p>

							<p><strong>Party A (Seller)</strong>:…........................................................................................................................................................................................................................................................................................................</p>

							<p>Address:..................................................................................................................................................................................................................................................................................................................................</p>

							<p>Telephone:..............................................................................................................................................................................................................................................................................................................................</p>

							<p>Fax:.........................................................................................................................................................................................................................................................................................................................................</p>

							<p>Represented by:......................................................................................................................................................................................................................................................................................................................</p>

							<p>&nbsp;</p>

							<p><strong>Party B (Buyer):</strong>...........................................................................................................................................................................................................................................................................................................</p>

							<p>Address:..................................................................................................................................................................................................................................................................................................................................</p>

							<p>Telephone:..............................................................................................................................................................................................................................................................................................................................</p>

							<p>Fax:.........................................................................................................................................................................................................................................................................................................................................</p>

							<p>Represented by:......................................................................................................................................................................................................................................................................................................................</p>

							<p>&nbsp;</p>

							<p>After discussion, the parties agreed to sign the annex of contract no:…….. with the change of unit price as follows:</p>

							<p>&nbsp;</p>

							<p><strong>Desciption of goods:</strong></p>

							<p>&nbsp;</p>

							<table border="1" cellpadding="0" cellspacing="0" style="line-height:20.7999992370605px">
								<tbody>
									<tr>
										<td style="height:27px; width:45px">
										<p>STT</p>
										</td>
										<td style="height:27px; width:153px">
										<p>Name of commodity</p>
										</td>
										<td style="height:27px; width:152px">
										<p>Quantity</p>
										</td>
										<td style="height:27px; width:146px">
										<p>Unit price</p>
										</td>
										<td style="height:27px; width:121px">
										<p>Changed price</p>
										</td>
									</tr>
									<tr>
										<td style="height:41px; width:45px">
										<p>1</p>
										</td>
										<td style="height:41px; width:153px">
										<p>&nbsp;</p>
										</td>
										<td style="height:41px; width:152px">
										<p>&nbsp;</p>
										</td>
										<td style="height:41px; width:146px">
										<p>&nbsp;</p>
										</td>
										<td style="height:41px; width:121px">
										<p>&nbsp;</p>
										</td>
									</tr>
									<tr>
										<td style="height:47px; width:45px">
										<p>2</p>
										</td>
										<td style="height:47px; width:153px">
										<p>&nbsp;</p>
										</td>
										<td style="height:47px; width:152px">
										<p>&nbsp;</p>
										</td>
										<td style="height:47px; width:146px">
										<p>&nbsp;</p>
										</td>
										<td style="height:47px; width:121px">
										<p>&nbsp;</p>
										</td>
									</tr>
									<tr>
										<td style="height:49px; width:45px">
										<p>3</p>
										</td>
										<td style="height:49px; width:153px">
										<p>&nbsp;</p>
										</td>
										<td style="height:49px; width:152px">
										<p>&nbsp;</p>
										</td>
										<td style="height:49px; width:146px">
										<p>&nbsp;</p>
										</td>
										<td style="height:49px; width:121px">
										<p>&nbsp;</p>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="height:49px; width:198px">
										<p>Total</p>
										</td>
										<td style="height:49px; width:152px">
										<p>&nbsp;</p>
										</td>
										<td style="height:49px; width:146px">
										<p>&nbsp;</p>
										</td>
										<td style="height:49px; width:121px">
										<p>&nbsp;</p>
										</td>
									</tr>
								</tbody>
							</table>

							<p>The two parties commit to implement all contents of the contract no:…….................…, which party violates the contract will be responsibe under ..................... . Within the implement time of the contract, any problem arise, the two parties discuss for the settlement. Any amendment will be implemented by the annex contract signed by the two parties.</p>

							<p>This annex is made into ...&nbsp;copies with the same value. Each party shall keep ...&nbsp;copy each.</p>

							<p><strong>Party A &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Party B</strong></p>

							<p>&nbsp;</p>

							<p>&nbsp;</p>

							<p style="text-align:justify">&nbsp;</p>
							</div>
						</div>



















							</div>
						</td>
					</tr>
				</tbody>
			</table>
		
	</section>
<!-- / .section -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/copyright.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
<!-- / #main -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>