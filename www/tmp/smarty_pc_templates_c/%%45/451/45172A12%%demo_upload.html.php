<?php /* Smarty version 2.6.27, created on 2016-12-29 10:05:09
         compiled from admin/demo_upload.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', 'admin/demo_upload.html', 2, false),array('modifier', 'explode', 'admin/demo_upload.html', 2, false),array('block', 'form', 'admin/demo_upload.html', 54, false),array('function', 'form_label', 'admin/demo_upload.html', 58, false),array('function', 'form_mce', 'admin/demo_upload.html', 60, false),array('function', 'form_file', 'admin/demo_upload.html', 66, false),)), $this); ?>
<?php $this->assign('title', 'Demo upload image'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/header.html", 'smarty_include_vars' => array('title' => ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'id' => "",'css' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "fileupload/import.css") : explode($_tmp, "fileupload/import.css")),'css_link' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")),'js' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "tinymce/tinymce.min.js,fileupload/import.js") : explode($_tmp, "tinymce/tinymce.min.js,fileupload/import.js")),'js_link' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script>
tinyMCE.init({
    mode : "none",
    language : "ja",
    height: "200px",
    plugins: [
		"advlist autolink lists link image charmap print preview hr anchor pagebreak",
		"searchreplace wordcount visualblocks visualchars fullscreen",
		"insertdatetime media nonbreaking save table contextmenu directionality",
		"emoticons template paste textcolor colorpicker textpattern code"
	],
	toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	toolbar2: "print preview media | forecolor backcolor emoticons | code",
	media_alt_source: false,
	media_poster: false,
	file_browser_callback: function(field_name, url, type, win) {
		if(type == 'image'){
			$('#'+field_name).data('title', 'mce_image').bindUploadFileMCE();
		}
		if(type == 'media'){
			$('#'+field_name).data('title', 'mce_video').bindUploadFileMCE();
		}
	},
});

function init_editor(id){
    tinyMCE.execCommand('mceAddEditor', true, id);
}

$(function() {
	$('textarea.mceEditor').each(function(index, el) {
		init_editor($(this).attr('id'));
	});
});

</script>
<div id="main">
	
	<ol class="breadcrumbs">
		<li><a href="/admin/">Management screenトップ</a></li>
		<li><em><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</em></li>
	</ol>

	<section class="section">
		<h1 class="headline"><span><?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</span></h1>
	<?php $this->_tag_stack[] = array('form', array()); $_block_repeat=true;smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>	
		<table class="sheet mb20">
			<tbody>
				<tr>
					<th><?php echo smarty_function_form_label(array('name' => "demo[mce_01]"), $this);?>
</th>
					<td>
						<?php echo smarty_function_form_mce(array('name' => "demo[mce_01]",'class' => 'mceEditor'), $this);?>

					</td>
				</tr>
				<tr>
					<th><?php echo smarty_function_form_label(array('name' => "demo[file]"), $this);?>
</th>
					<td>
						<?php echo smarty_function_form_file(array('name' => "demo[file]",'upload_type' => $this->_config[0]['vars']['UPLOAD_TYPE_FILE']), $this);?>

					</td>
				</tr>
				<tr>
					<th><?php echo smarty_function_form_label(array('name' => "demo[image]"), $this);?>
</th>
					<td>
						<?php echo smarty_function_form_file(array('name' => "demo[image]",'upload_type' => $this->_config[0]['vars']['UPLOAD_TYPE_IMAGE']), $this);?>

					</td>
				</tr>
				<tr>
					<th><?php echo smarty_function_form_label(array('name' => "demo[video]"), $this);?>
</th>
					<td>
						<?php echo smarty_function_form_file(array('name' => "demo[video]",'upload_type' => $this->_config[0]['vars']['UPLOAD_TYPE_VIDEO']), $this);?>

					</td>
				</tr>
				<tr>
					<th><?php echo smarty_function_form_label(array('name' => "demo[crop]"), $this);?>
</th>
					<td>
						<?php echo smarty_function_form_file(array('name' => "demo[crop]",'upload_type' => $this->_config[0]['vars']['UPLOAD_TYPE_CROP'],'text' => "Drop image here or click to crop."), $this);?>

					</td>
				</tr>
			</tbody>
		</table>
		
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/form_edit_buttons.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_form($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
	</section>

</div> <!-- end div#main -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/fileupload.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>