<?php /* Smarty version 2.6.27, created on 2017-05-04 16:16:50
         compiled from admin/index.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', 'admin/index.html', 2, false),array('modifier', 'explode', 'admin/index.html', 2, false),array('modifier', 'default', 'admin/index.html', 23, false),array('modifier', 'date_format', 'admin/index.html', 28, false),)), $this); ?>
<?php $this->assign('title', 'Admin'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/header.html", 'smarty_include_vars' => array('title' => ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'description' => "",'keywords' => "",'id' => "",'css' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")),'js' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")),'js_link' => ((is_array($_tmp=',')) ? $this->_run_mod_handler('explode', true, $_tmp, "") : explode($_tmp, "")))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="main">
	<ol class="breadcrumbs">
		<li><a href="/admin/">Home</a></li>
	</ol>
	<!-- / .breadcrumbs -->
	
	<section class="section">
		<h1 class="headline"><span>Recent contracts</span></h1>
		<div class="message">
			<div id="columns">
				<?php $_from = ((is_array($_tmp=$this->_tpl_vars['list_contract'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['row']):
?>
				<div class="pin" data-id=<?php echo ((is_array($_tmp=$this->_tpl_vars['row']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
>
					<a class="contract_name" href="/admin/contract_view?id=<?php echo ((is_array($_tmp=$this->_tpl_vars['row']['id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><img  class="contract_user" src="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['row']['image'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('default', true, $_tmp, '/img/no-image.jpg') : smarty_modifier_default($_tmp, '/img/no-image.jpg')); ?>
" />
					<?php echo ((is_array($_tmp=$this->_tpl_vars['row']['name'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</a>
					<p class="contract_description">
						<span class="text-green"><i class="fa fa-address-book"></i> <a href="/admin/contract_list?user_laborer=<?php echo ((is_array($_tmp=$this->_tpl_vars['row']['user_laborer'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['row']['laborer'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</a></span><br>
						<span class="text-green"><i class="fa fa-file"></i> <a href="/admin/category_list?id=<?php echo ((is_array($_tmp=$this->_tpl_vars['row']['category_id'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['row']['category_name'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</a></span><br>
						<span class="text-green"><i class="fa fa-calendar"></i> </span>Singed: <?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['row']['signed_date'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d/%m/%Y") : smarty_modifier_date_format($_tmp, "%d/%m/%Y")); ?>
<br>
						<span class="text-green"><i class="fa fa-calendar"></i> </span>End: <?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['row']['end_date'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d/%m/%Y") : smarty_modifier_date_format($_tmp, "%d/%m/%Y")); ?>
<br>
					</p>
					<p class="clear"></p>
					<div class="contract_info">
						<?php if (((is_array($_tmp=$this->_tpl_vars['row']['create_image'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)) != ''): ?>
						<span class="text-red text-img">Created by: <?php echo ((is_array($_tmp=$this->_tpl_vars['row']['create'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</span>
						<?php else: ?>
						<span class="text-red text-img">Created by: <?php echo ((is_array($_tmp=$this->_tpl_vars['row']['create'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</span>
						<?php endif; ?>
					</div>
				</div>
				<?php endforeach; endif; unset($_from); ?>

			</div>
			<p id="loading" style="display: none;">
				<img src="/img/loading.gif" alt="Loading…" />
			</p>
		</div>
	</section>
	<!-- / .section -->
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/copyright.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
<!-- / #main -->


<script type="text/javascript">
    var isPreviousEventComplete = true, isDataAvailable = true;
    $(window).scroll(function () {
     if ($(document).height() - 50 <= $(window).scrollTop() + $(window).height()) {
      if (isPreviousEventComplete && isDataAvailable) {
       
        isPreviousEventComplete = false;
        $("#loading").css("display", "block");

        $.ajax({
			url: '/admin/',
			type: 'get',
			data:{
				id: $('.pin:last-child').attr('data-id')
			},
			dataType: 'json',
          success: function (data) {
          	var str = '';
			 $.each(data, function (index, row) {
			    str+= '<div class="pin" data-id='+row.id+'>';
			    if(row.image){
			    	str += '<img  class="contract_user" src="'+row.image+'" />';
			    }else{
			    	str += '<img  class="contract_user" src="/img/no-image.jpg" />';
			    }
			   str += '<a class="contract_name" href="/admin/contract_view?id='+row.id+'">'+row.name+'</a>';
			    str += '<p class="contract_description">';
			    str += '    <span class="text-green"><i class="fa fa-address-book"></i> <a href="/admin/contract_list?staff_id='+row.staff_id+'">'+row.laborer+'</a></span><br>';
			    str += '    <span class="text-green"><i class="fa fa-file"></i> <a href="/admin/category_list?id='+row.category_id+'">'+row.category_name+'</a></span><br>';
			    str += '    <span class="text-green"><i class="fa fa-calendar"></i> </span>Singed: '+row.signed_date+'<br>';
			    str += '    <span class="text-green"><i class="fa fa-calendar"></i> </span>End: '+row.end_date+'<br>';
			    str+='</p>';
			    str += '<p class="clear"></p>';
			    str += '<div class="contract_info">';
			    str += '   <span class="text-red text-img">Created by: '+row.create+'</span>';
			    str += '</div>';
				str += '</div>';
			    });

            $("#columns").append(str);
            isPreviousEventComplete = true;

            if (data == '') //When data is not available
                isDataAvailable = false;

            $("#loading").css("display", "none");
          },
          error: function (error) {
          }
        });

      }
     }
    });
</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_includes/admin/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>