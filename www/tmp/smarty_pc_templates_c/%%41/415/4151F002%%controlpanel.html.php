<?php /* Smarty version 2.6.27, created on 2016-12-28 10:39:38
         compiled from controlpanel.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cb_escape', 'controlpanel.html', 6, false),array('modifier', 'truncate', 'controlpanel.html', 35, false),array('modifier', 'nl2br', 'controlpanel.html', 37, false),array('modifier', 'allow_html', 'controlpanel.html', 185, false),array('function', 'cycle', 'controlpanel.html', 33, false),array('function', 'echo', 'controlpanel.html', 48, false),)), $this); ?>


	<div id="CBFHTMLDebug" class="CBFWebDebugTop" style="display: none;height:100%;">
		<h1>BTS</h1>
        <div id="CBFHTMLDebugLines">
        	<?php echo ((is_array($_tmp=$this->_tpl_vars['_HTML'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>

 		</div>
	</div>

<iframe name="_debug" style="display:none"></iframe>


    <div id="CBFDBDebug" class="CBFWebDebugTop" style="display: none">
        <h1>データベース実行SQL</h1>
        <ul id="CBFDBDebugMenu">
            <li>
                <a href="javascript:void(0);" onclick="CBFMatchDisplay('ALL');">[all]</a>
                <a href="javascript:void(0);" onclick="CBFMatchDisplay('SELECT');" id="SELECT" style="color:#CCC">[select]</a>
                <a href="javascript:void(0);" onclick="CBFMatchDisplay('UPDATE');" id="UPDATE" style="color:#CCC">[update]</a>
                <a href="javascript:void(0);" onclick="CBFMatchDisplay('INSERT');" id="INSERT" style="color:#CCC">[insert]</a>
                <a href="javascript:void(0);" onclick="CBFMatchDisplay('OTHER');" id="OTHER" style="color:#CCC">[other]</a>
            </li>
        </ul>
        <div id="CBFDBDebugLines">
             <table class="CBFDBDebugs">
                <tr>
                    <th>ID</th>
                    <th>時間</th>
                    <th>種類</th>
                    <th>SQL</th>
                </tr>
				<?php $_from = ((is_array($_tmp=$this->_tpl_vars['CB_DBDebug'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['query']):
?>
	                <tr class='CBFDBDebugLine CBFFrontWebController <?php echo smarty_function_cycle(array('values' => "CBFWebDebugInfo,CBFWebDebugInfo2"), $this);?>
' id="<?php echo ((is_array($_tmp=$this->_tpl_vars['query']['queryType'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['key'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
">
	                    <td class="CBFDBDebugNumber"><?php echo ((is_array($_tmp=$this->_tpl_vars['key'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</td>
	                    <td><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['query']['time'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 8, '') : smarty_modifier_truncate($_tmp, 8, '')); ?>
</td>
	                    <td><?php echo ((is_array($_tmp=$this->_tpl_vars['query']['queryType'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</td>
	                    <td><?php echo ((is_array($_tmp=$this->_tpl_vars['query']['query'])) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</td>
	                </tr>
				<?php endforeach; endif; unset($_from); ?>
			</table>
        </div>
    </div>
    <div id="CBFParamDebug" class="CBFWebDebugTop" style="display: none">
        <h1>パラメータ一覧</h1>
        <div id="CBFParamDebugLines">
        	<h2>$_GET</h2>
        	<?php if (((is_array($_tmp=@CB_DEBUGGING_ESCAPE_VARS)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
        		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$_GET)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'htmlspecialchars' => true), $this);?>

        	<?php else: ?>
        		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$_GET)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))), $this);?>

        	<?php endif; ?>
        	<h2>$_POST</h2>
        	<?php if (((is_array($_tmp=@CB_DEBUGGING_ESCAPE_VARS)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
        		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$_POST)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'htmlspecialchars' => true), $this);?>

        	<?php else: ?>
        		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$_POST)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))), $this);?>

        	<?php endif; ?>
        	<h2>$_REQUEST</h2>
        	<?php if (((is_array($_tmp=@CB_DEBUGGING_ESCAPE_VARS)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
        		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$_REQUEST)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'htmlspecialchars' => true), $this);?>

        	<?php else: ?>
        		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$_REQUEST)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))), $this);?>

        	<?php endif; ?>
        	<h2>$_FILES</h2>
        	<?php if (((is_array($_tmp=@CB_DEBUGGING_ESCAPE_VARS)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
        		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$this->_tpl_vars['_FILES'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'htmlspecialchars' => true), $this);?>

        	<?php else: ?>
        		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$this->_tpl_vars['_FILES'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))), $this);?>

        	<?php endif; ?>
        	<h2>$_SERVER</h2>
        	<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$_SERVER)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))), $this);?>

        </div>
    </div>

    <div id="CBFStateDebug" class="CBFWebDebugTop" style="display: none">
        <h1>パラメータ一覧</h1>
        <div id="CBFStatemDebugLines">
        	<h2>$_COKKIE</h2>
        	<?php if (((is_array($_tmp=@CB_DEBUGGING_ESCAPE_VARS)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
        		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$_COOKIE)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'htmlspecialchars' => true), $this);?>

        	<?php else: ?>
        		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$_COOKIE)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))), $this);?>

        	<?php endif; ?>
        	<h2>$_SESSION</h2>
        	<?php if (((is_array($_tmp=@CB_DEBUGGING_ESCAPE_VARS)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
        		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$_SESSION)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)),'htmlspecialchars' => true), $this);?>

        	<?php else: ?>
        		<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$_SESSION)) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))), $this);?>

        	<?php endif; ?>
        </div>
    </div>

    <div id="CBFTraceDebug" class="CBFWebDebugTop" style="display: none">
        <h1>スタックトレース一覧</h1>
        <div id="CBFStatemDebugLines">
        	<h2>スタックトレース</h2>
        	<?php echo smarty_function_echo(array('output' => ((is_array($_tmp=$this->_tpl_vars['CB_backTrace'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))), $this);?>

        </div>
    </div>

    <div id="CBFInfoDebug" class="CBFWebDebugTop" style="display: none">
    	<h1>実行情報</h1>
        <div id="CBFStatemDebugLines">
			<?php if (((is_array($_tmp=$this->_tpl_vars['CB_peakMemory'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp))): ?>
			<h2>最大メモリ</h2>
			<li>　<?php echo ((is_array($_tmp=$this->_tpl_vars['CB_peakMemory'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
KB</li>
			<?php endif; ?>
			<h2>最終メモリ</h2>
		    <li>　<?php echo ((is_array($_tmp=$this->_tpl_vars['CB_currentMemory'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
KB</li>

			<h2>実行時間</h2>
		    <li class="last">
		        　<?php echo ((is_array($_tmp=$this->_tpl_vars['CB_loadTime'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
秒
		    </li>
		</div>
	</div>

	<div id="CBFCPDebug" class="CBFWebDebugTop" style="display: none">
    	<h1>CBFコントロールパネル</h1>
        <div id="CBFStatemDebugLines">
        	<table>
        		<thead>
        			<tr>
        				<th><h2>絵文字一覧</h2></th>
						<th><h2>コードヘルパー</h2></th>
						<th><h2>端末情報</h2></th>
						<th><h2>ドキュメント</h2></th>
						<th><h2>HTML一覧</h2></th>
					</tr>
				</thead>
				<tbody>
					<tr>
					    <td><a href="http://blog.cgfm.jp/garyu/appendix/lib/emj/emj_list.php" onclick="CBFControlPanelShow('CBFCPEmojiList');">絵文字一覧</a></td>
                        <td><a href="/codehelper" target="_blank">コードヘルパー</a></td>
					    <td><a href="javascript:void(0);" onclick="CBFControlPanelShow('CBFCPDeviceList');">端末情報</a></td>
					    <td>
						    <p><a href="http://group.cbri.biz/wiki/index.php?CB_Framework">フレームワークマニュアル</a></p>
						    <p><a href="http://group.cbri.biz/wiki/index.php?CB_Framework%2F%A5%C6%A5%F3%A5%D7%A5%EC%A1%BC%A5%C8%A5%D7%A5%E9%A5%B0%A5%A4%A5%F3">テンプレートプラグイン</a></p>
						</td>
					    <td><a href="javascript:void(0);" onclick="CBFControlPanelShow('CBFCPHtmlList');">HTML一覧</a></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div id="CBFCPEmojiList" style="display: none">
			EmojiList
		</div>

		<div id="CBFCPCodeGen" style="display: none">
			CodeGen

<script type="text/javascript">
// <!--
function CBFCodeGenShow(id){
	var is_close = false;

	var element = document.getElementById(id);

	if(element.style.display == "none"){
		 is_close = true;
	}

	<?php $_from = ((is_array($_tmp=$this->_tpl_vars['CB_codeGen'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['table_name'] => $this->_tpl_vars['codes']):
?>
	document.getElementById('CBFCodeGen_<?php echo ((is_array($_tmp=$this->_tpl_vars['table_name'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
').style.display = 'none';
	<?php endforeach; endif; unset($_from); ?>

	if (is_close) {
		element.style.display = '';
	}else{
		element.style.display = 'none';
	}
}
// -->
</script>

			<ul>
			<?php $_from = ((is_array($_tmp=$this->_tpl_vars['CB_codeGen'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['table_name'] => $this->_tpl_vars['codes']):
?>
			<li>
				<h3><a href="javascript:void(0);" onclick="CBFCodeGenShow('CBFCodeGen_<?php echo ((is_array($_tmp=$this->_tpl_vars['table_name'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
');"><?php echo ((is_array($_tmp=$this->_tpl_vars['table_name'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</a></h3>
				<div id="CBFCodeGen_<?php echo ((is_array($_tmp=$this->_tpl_vars['table_name'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
" style="display: none">
				<?php $_from = ((is_array($_tmp=$this->_tpl_vars['codes'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['code_type'] => $this->_tpl_vars['code']):
?>
					<h4><?php echo ((is_array($_tmp=$this->_tpl_vars['code_type'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
</h4>
					<div>
						<textarea style="<?php echo ((is_array($_tmp=$this->_tpl_vars['code']['style'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)); ?>
" onclick="this.select()"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['code']['html'])) ? $this->_run_mod_handler('cb_escape', true, $_tmp) : smarty_modifier_cb_escape($_tmp)))) ? $this->_run_mod_handler('allow_html', true, $_tmp) : smarty_modifier_allow_html($_tmp)); ?>
	</textarea>
					</div>
				<?php endforeach; endif; unset($_from); ?>
				</div>
			</li>
			<?php endforeach; endif; unset($_from); ?>
			</ul>

		</div>

		<div id="CBFCPDeviceList" style="display: none">
			DeviceList
		</div>

		<div id="CBFCPHtmlList" style="display: none">
			HtmlList
		</div>
	</div>
</div>
