(function($) {
$(function() {
/* START Script */

// ==============================================================
// Ajax change status
// ==============================================================

$('.ajax_change_status').on('change', function(){
    var elmStatus = $(this);
    var elmSelect = $(this).find('option:selected');
    var strAlert  = '本当 に' + elmSelect.text() + 'してもよろしいですか？';
    if(confirm(strAlert)){
        var param = {
                _type : "change_status",
                id    : $(this).data('id'),
                status: $(this).val(),
                table : $(this).data('table'),
            };
        var url = '/ajax/';
        if(typeof($(this).data('table')) != 'undefined') {
            callAjax(param, url, function(res) {
                if(res == "success") {
                    elmStatus.find('option').removeAttr('selected');
                    elmSelect.attr('selected', 'selected');
                }
            });
        }
    }else {
        $(this).find('option').prop('selected', function() {
            return this.defaultSelected;
        });
        return;
    }
});

// ==============================================================
// Ajax delete
// ==============================================================

$('.ajax_delete').on('click', function(e){
    e.preventDefault();
    var that = $(this);
    var strAlert  = '本当 に削除' + $(this).data('id') + 'してもよろしいですか？';
    if(confirm(strAlert)){
        var param = {
                _type : "delete",
                id    : $(this).data('id'),
                table : $(this).data('table'),
                force : $(this).data('force'),
            };
        var url = '/ajax/';
        if(typeof($(this).data('table')) != 'undefined') {
            callAjax(param, url, function(res) {
                if(res == "success") {
                    if(that.attr('data-url')){
                        window.location.href = that.attr('data-url');
                    }else{
                        that.parents('tr').remove();
                    }
                }
            });
        }
    }
});

// ==============================================================
// insert template text
// ==============================================================

$('select[name="mail[mail_template_id]"]').on('change', function(e){
    var that = $(this);
    var param = {
            _type : "getMailTemplate",
            id    : $(this).val(),
            table : "mail_template",
        };
    var url = '/ajax/';
    callAjax(param, url, function(res) {
        $('textarea[name="mail[mail_content]"]').val(res);
    });
});

// ==============================================================
// insert job_type
// ==============================================================

$('#job_type_insert').on('click', function(){
    var name     = $('#job_type_name');
    var message  = $('#job_type_message');
    var strName  = name.val().trim();

    if( strName == null || strName == "" ) {
        name.addClass("errorValidate");
        message.show().find("li").text("職種名は必須項目です。");
        return false;
    }
    if( strName.length >100) {
        name.addClass("errorValidate");
        message.show().find("li").text("職種名は100文字以内で入力してください。");
        return false;
    }
 
    strAlert = '本当 に' + strName + 'してもよろしいですか？';
    if(confirm(strAlert)){
        var table         = $(this).data('table');
        var job_type_name = name.val().trim();
        console.log(
        $.ajax({
            url      : '/ajax/update_jobtype',
            type     : 'POST',
            dataType : 'json',
            data     :
            {
                job_type_name : job_type_name,
                table         : table,
            },
            success: function(res) {
                message.hide();
                name.val("");
                if(res == "hasRow") {
                    message.show().find("li").text("この職種名は既に使用されています。");
                } else {
                    $('select[name="interview[job_type_id]"]').find('option').removeAttr('selected');
                    $('select[name="interview[job_type_id]"]').append("<option selected='selected' value=" + res.job_type_id + ">" + res.job_type_name + "</option>");
                    $("#job_type_form").hide();
                }
            },
            fail: function(res) {
            }
        })
        );
    }else {
        $(this).find('option').prop('selected', function() {
            return this.defaultSelected;
        });
        return;
    }
});
$('input#job_type_name').on("focus", function(){
    $(this).removeClass("errorValidate");
});
$('#job_type_button').on("click", function(){
    $("#job_type_form").show();
});
$('#job_type_form .cr-bg').on("click", function(){
    $("#job_type_form").hide();
});


$('#frm_sendMail').submit(function(event) {
    // event.preventDefault();
    var arr_cookie = getCookieToArr();
    $(this).find('input[name="entry_ids"]').val(arr_cookie);
    if($(this).find('input[name="entry_ids"]').val() != "") {
        return true;
    }
    return false;
});

$('#frm_exportCSV').submit(function(event) {
    var arr_cookie = getCookieToArr();
    $(this).find('input[name="entry_ids"]').val(arr_cookie);
});

$('.downloadCsv').click(function(event) {
    event.preventDefault();
    var vals = $('input[name="entry_ids[]"]:checked').map(function(){
        return $(this).val();
    }).get();
    var param = {
        _type : "exportCSV",
        entry_ids:vals,
        table : "entry",
    };
    var url = '/ajax/';
    callAjax(param, url, function(res) {
    });
});

 $('.frm_mail_update_status').click(function(event) {
    event.preventDefault();
    var vals = [];
    $(".chkOne").each(function(){
        if($(this).is(":checked")){
            vals.push($(this).attr("data-id"));
        }
    });
    $(this).find('input[name="mail_ids"]').val(vals);
        var strAlert  = '本当 に削除してもよろしいですか？';
         if($(this).find('input[name="mail_ids"]').val() == "") {
            var type = $(this).find('input[name="type_update"]').attr('value');
            if( type =='select_read' || type == 'select_delete' ){
                return false;
            }
         }
        if(confirm(strAlert)){
            var url = '/ajax/';
            
            var param = {
                _type : "company_message_update",
                type_update: $(this).find('input[name="type_update"]').attr('value'),
                mail_ids: $(this).find('input[name="mail_ids"]').attr('value'),
            };
                
            callAjax(param, url, function(res) {
                if(res.type_update == 'select_read'){
                    $.each( res.data, function( i, val ) {
                        $('tr[data-id="'+val.mail_id+'"]').find($('.notread')).addClass('readed');
                    });
                }else if(res.type_update == 'select_delete'){
                    $.each( res.data, function( i, val ) {
                        $('tr[data-id="'+val.mail_id+'"]').remove();
                    });
                }else {
                    location.reload();
                }
            });
            return true;
            
        }


            
    });


/* END Script */
});
})(jQuery);

function callAjax(param,url,callback){
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: param,
        success: function(res) {
            callback(res);
        },
        fail: function(res) {
            callback(res);
        }
    });
}