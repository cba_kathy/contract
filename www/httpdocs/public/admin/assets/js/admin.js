(function($) {
$(function() {
/* START Script */

// ==============================================================
// Insert tag a to textarea
// ==============================================================

$('#modalInsertHyberlink').on('show.bs.modal', function (event) {
var button       = $(event.relatedTarget),
    modal        = $(this),
    textarea     = $('textarea[name="notice[notice_detail]"]'),
    len          = textarea.val().length,
    start        = textarea[0].selectionStart,
    end          = textarea[0].selectionEnd,
    selectedText = textarea.val().substring(start, end),
    modalText    = modal.find('.modal-body input#textHyberlink'),
    modalLink    = modal.find('.modal-body input#linkHyberlink');

    // Set defaule value
    modalText.val(selectedText);
    modalLink.val("http://");
});
$('#btn_insertHyberlink').on('click', function(e) {
    e.preventDefault();
    var txtTitle = $('input#textHyberlink').val()
    var txtUrl   = $('input#linkHyberlink').val();
    var message  = $('#job_type_message');
    message.html('');
    if(txtTitle == ''){
        message.show().append("<li>テキストは必須項目です。</li>");
    }
    if(txtUrl == ''){
        message.show().append("<li>リンクは必須項目です。</li>");
    }
    if(txtUrl !== ''){
        var expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
        var regex = new RegExp(expression);
        if (! txtUrl.match(regex)) {
             message.show().append("<li>正しいリンクを入力してください。</li>");
             return false;
        }
    }
    
    if( txtTitle != '' && txtUrl != '') {
        var replacement = '<a title="'+ txtTitle +'" href="'+ txtUrl +'" rel="external">'+ txtTitle +'</a>';

        $('textarea[name="notice[notice_detail]"]').insertHyberlink(replacement);

        $('#modalInsertHyberlink').find('.modal-body input').val("");
    }else{
        return false;
    }
});

$('button#closeHyberlink').on('click', function() {
    $('#textHyberlink').val("");
    $('#linkHyberlink').val("http://");
});
$('.insertTempValue').on('click', function() {
    var replacement = '{[$' + $(this).data('text') + ']}';
    $('textarea[name="mail_template[mail_template_content]"]').insertHyberlink(replacement);
});


$('#btn_autoInsertName').on("click", function(){
    var vals = "";
    $('.cba_collapse_names li a').each(function(i, v){
        if(i > 0) {
            vals += ', ';
        }
        vals += $(this).text();
    });
    mail_content = $('textarea[name="mail[mail_content]"]');
    mail_content.val( mail_content.val().replace(/\{\[\$name\]\}/g, vals) );
});


$.fn.extend({
    insertHyberlink: function(replacement) {
        return this.each(function(i) {
        var len          = this.value.length,
            start        = this.selectionStart,
            end          = this.selectionEnd,
            selectedText = this.value.substring(start, end);
            this.value   = this.value.substring(0, start) + replacement + this.value.substring(end, len);
        })
    }
});

$(".cba_collapse").click(function () {
    that = $(this);
    that_next = that.next();
    that_next.slideToggle(300, function () {
        // that.text(function () {
        //     return that_next.is(":visible") ? "Collapse" : "Expand";
        // });
    });

});


// ==============================================================
// Load cookie for entry
// ==============================================================
var loading_cookie = getCookieToArr();

$('.entry_chkOne').each(function(i,e){
    if($.inArray(this.value, loading_cookie) == -1) {
        $(this).prop('checked', false);
    } else {
        $(this).prop('checked', true);
    }
});

// ==============================================================
// Check cookie for entry
// ==============================================================
$('.entry_chkAll').on('change', function(e) {
    var arr_cookie = getCookieToArr();
    if(this.checked) {
        $('.entry_chkOne').each(function(i,e){
            arr_cookie = addInArr(this.value, arr_cookie);
        });
    } else {
        $('.entry_chkOne').each(function(i,e){
            arr_cookie = removeInArr(this.value, arr_cookie);
        });
    }
    setCookie("checkids", arr_cookie.join(), 1);
});
$('.entry_chkOne').on('change', function(e) {
    var arr_cookie = getCookieToArr();
    if($.inArray(this.value, arr_cookie) == -1 ) {
        arr_cookie = addInArr(this.value, arr_cookie);
    } else {
        arr_cookie = removeInArr(this.value, arr_cookie);
    }
    setCookie("checkids", arr_cookie.join(), 1);
});



$(window).load(function () {
    setTimeout(function() {
        $("#navi .pull").slideUp(400);
    }, 5000);
});
var Timer;
$('#navi').hover(
    function(){
        clearTimeout(Timer);
        $("#navi .pull").slideDown(400);
    },
    function(){
        Timer = setTimeout(function() {
            $("#navi .pull").slideUp(400);
        }, 2000);
    }
);



/* END Script */
});
})(jQuery);



// ==============================================================
// cookie function
// ==============================================================
function setCookie(cname, cvalue, exhours) {
    var d = new Date();
    d.setTime(d.getTime() + (exhours*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function getCookieToArr() {
    var cookie = getCookie("checkids"),
        arr_cookie = cookie.split(',');
    arr_cookie = removeInArr("", arr_cookie);
    return arr_cookie;
}

function addInArr(val, arr) {
    arr.push(val);
    jQuery.unique(arr.sort()).sort();
    return arr;
}

function removeInArr(val, arr) {
    var removeItem = val;
    arr = $.grep(arr, function(value) {
      return value != removeItem;
    });
    return arr;
}
