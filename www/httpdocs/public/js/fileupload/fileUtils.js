(function($){
	var	UPLOAD_TYPE_FILE = 'upload_file',
		UPLOAD_TYPE_IMAGE = 'upload_image',
		UPLOAD_TYPE_VIDEO = 'upload_video',
		UPLOAD_TYPE_MCE_IMAGE = 'mce_image',
		UPLOAD_TYPE_MCE_VIDEO = 'mce_video',
		UPLOAD_TYPE_CROP = 'crop_image',
		UPLOAD_TYPE_DELETE = 'delete_image';
		UPLOAD_THUMBNAIL_SIZE = 100,

		UPLOAD_URL = '/ajax/fileupload',
		UPLOAD_CONTAINER = '.dropzone',
		UPLOAD_FILE = '#fileUpload',
		HTML_BTN_DELETE = '<button class="close btnDelete" type="button">×</button>',
		BTN_UPLOAD = '.btnUpload',
		BTN_DELETE = '.btnDelete',

		MODAL_UPLOAD = '#modal-upload',
		MODAL_TITLE_UPLOAD = 'Uploading file...',
		MODAL_TITLE_ERROR = 'Error!',
		MODAL_TIMEOUT = 1000,

		MODAL_CROP = '#modal-crop',
		IMAGE_FORM = '.image-form',
		IMAGE_WRAPPER = '.image-wrapper',
		IMAGE_PREVIEW = '.image-preview',

		$ctnUpload = null,
		objFormData = {},
		isMCE = false,
		intProgressTime = 0,

		strImageSrc = null,
		blnActive = false,
		blnRatio = NaN;

	function getFileOptions(upload_type) {
		var options = {};
		switch (upload_type) {
			case UPLOAD_TYPE_FILE:
				options.extension = 'txt|doc|xls';
				options.size = '2MB';
				break;
			case UPLOAD_TYPE_VIDEO:
			case UPLOAD_TYPE_MCE_VIDEO:
				options.extension = 'mov|mp4|flv|avi|3gp|3gpp|wmv|vob|mpeg|mpeg-ps|mpeg-ts|webm';
				options.size = '5GB';
				break;
			case UPLOAD_TYPE_IMAGE:
			case UPLOAD_TYPE_MCE_IMAGE:
			case UPLOAD_TYPE_CROP:
			default:
				options.extension = 'gif|jpe?g|png';
				options.size = '10MB';
				break;
		}
		return options;
	};

	function convertFileSize(size) {
		if ($.isNumeric(size)) {
			return size;
		};
		var matches = size.match(/^([0-9]+)([a-zA-Z]*)$/);
        switch (matches[2]) {
            case 'GB':
            case 'G':
                size = matches[1] * 1073741824;
                break;
            case 'MB':
            case 'M':
                size = matches[1] * 1048576;
                break;
            case 'KB':
            case 'K':
                size = matches[1] * 1024;
                break;
            default:
                size = matches[1];
        }
        return size;
	}

	$.uploadFile = function(settings) {
		var options = $.extend({}, settings);

		$(UPLOAD_FILE).fileupload({
			url: UPLOAD_URL,
			dataType: 'json',
			dropZone: $(UPLOAD_CONTAINER),
			add: function(e, data) {
				objFormData.title = $ctnUpload.data('title') ? $ctnUpload.data('title') : UPLOAD_TYPE_IMAGE;
				var fileOptions = getFileOptions(objFormData.title);
				$.extend(objFormData, fileOptions, $ctnUpload.find('input').data());
				objFormData.inputName = $ctnUpload.find('input').attr('name');
	    		intProgressTime = 0;
	    		isMCE = /^(mce)(.*)$/i.test(objFormData.title);

				var uploadErrors = [],
		        	file = data.originalFiles[0],
		        	extension = objFormData.extension.replace(/[\,|\;]/g, '|');
		        	pattExtentsion = new RegExp("(\.|\/)("+extension+")$", "i"),
		        	maxSize = convertFileSize(objFormData.size);
		       
		        if (pattExtentsion.test(file.name) && file.size <= maxSize){
		        	data.formData = objFormData;
		        	data.submit();
		        } else {
		        	uploadErrors.push('accepted file type ('+objFormData.extension+')');
		        	uploadErrors.push('max file size ('+objFormData.size+')');
					$.showModalUpload(MODAL_TITLE_ERROR, uploadErrors);
						        	
		        }
			},
			done: function (e, data) {
				var result = data.result.result;
				$.doneUploadFile(result);
			},
			progressall: function (e, data) {
				// Update the progress bar while files are being uploaded
				intProgressTime += parseInt(data.loaded / data.total * 100, 10);
				$.showModalUpload(MODAL_TITLE_UPLOAD, intProgressTime);
			},
			fail: function(e, data) {
				var responseText = /(\{.*\})/.exec(data.jqXHR.responseText);
				var info = responseText[0];
				info = $.parseJSON(info);
				$.showModalUpload(MODAL_TITLE_ERROR, info);
				strImageSrc = null;
			}
		});
	};
	
	$.doneUploadFile = function(result) {
		setTimeout(function(){
			$(MODAL_UPLOAD).modal("hide");
			if (isMCE) {
				$('.mce-container').addClass('mce-in');
				$ctnUpload.val(result);
			};
			if (objFormData.title == UPLOAD_TYPE_CROP) {
				strImageSrc = result;
				return new $.CropImage();
			};
		}, MODAL_TIMEOUT - intProgressTime);
		
		switch (objFormData.title) {
			case UPLOAD_TYPE_IMAGE:
				var html = '<img src="'+result+'" height="'+UPLOAD_THUMBNAIL_SIZE+'">';
				break;
			case UPLOAD_TYPE_VIDEO:
				var html = '<video src="'+result+'" controls="">Your browser does not support the video tag.</video>';
				break;	
			case UPLOAD_TYPE_FILE:
				var html = '<span class="boxShadow">'+result+'<span>';
				break;
		}
		if (html) {
			html += HTML_BTN_DELETE;
			$ctnUpload.find('.files').html(html)
				.next().val(result);
		};

	};

	$.showModalUpload = function(title, info) {
		if (isMCE) {
			$('.mce-container').removeClass('mce-in');
		}; 
		var $modal = $(MODAL_UPLOAD);
		$modal.find('.progress-bar').css('width', '0%');
		if (!$modal.hasClass('in')) {
			$modal.modal("show");
		}
		$.centerModal($modal);

		$modal.find('.modal-title').html('<span class="info">'+title+'</span>');
		if ($.isNumeric(info)) { // show progress bar
			$modal.find('.error').hide();
			$modal.find('.progress').show()
				.find('.progress-bar').css('width', info + '%');
		} else { // show error
			if (isMCE) {
				$('.mce-close').trigger('click');
			};
			var html = '<ul>';
			if (typeof(info) == 'object') {
				$.each(info, function(index, val) {
					html += '<li>'+val+'</li>';
				});
			} else {
				html += '<li>'+info+'</li>';
			}
			html += '</ul>';
			$modal.find('.progress').hide();
			$modal.find('.error').show().html(html);
		}
	};

	$.centerModal = function($modal) {
		var	dialog = $modal.find('.modal-dialog');
		$modal.css('display', 'block');
		dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()*2) / 2));
	};

	

	$.CropImage = function(method) {

		this.$modal = $(MODAL_CROP);
		this.$form = this.$modal.find(IMAGE_FORM);
		this.$wrapper = this.$modal.find(IMAGE_WRAPPER);
		this.$preview = this.$modal.find(IMAGE_PREVIEW);
		this.$btnBrowser = this.$modal.find('#btnBrowser');

		if (method) {
			this.trigger(method);
		} else {
			this.addListener();
			this.startCropImage();
		}
	};

	$.CropImage.prototype = {
		addListener: function() {
			this.$modal.on('show.bs.modal', $.proxy(this.showModal, this));
			this.$modal.on('hide.bs.modal', $.proxy(this.hideModal, this));
			this.$form.unbind('submit').on('submit', $.proxy(this.formSubmit, this));
			this.$btnBrowser.on('click', $.proxy(this.btnBrowserClick, this));
		},

		btnBrowserClick: function() {
			this.$modal.modal("hide");
			$(UPLOAD_FILE).trigger('click');
		},

		startCropImage: function() {
			if (strImageSrc) {
				this.$modal.modal("show");
			} else {
				this.$modal.modal("hide");
				$(UPLOAD_FILE).trigger('click');
			}
		},

		showModal: function () {

			if (strImageSrc) {
				this.initCropper();
			} else {
				this.stopCropper();
				this.alert('The image file is not valid.');
			}
		},
		hideModal: function () {
			// this.alert('');
			if (blnActive) {
				this.$image.cropper('destroy');
				blnActive = false;
				this.$wrapper.empty().removeAttr('style');
				this.$preview.empty().removeAttr('style');
			};
		},
		initCropper: function() {
			this.$image = $('<img src="' + strImageSrc + '?' + $.now() + '">');
			this.$wrapper.html(this.$image);
			var delay = 200; 
			var that = this;
			setTimeout(function(){
				that.$image.cropper({
					autoCrop: true,
					aspectRatio: blnRatio,
					preview: that.$preview.selector
				});
			}, delay); 
			blnActive = true;
		},

		formSubmit: function() {
			
			if (strImageSrc) {
				objFormData.cData = this.$image.cropper('getData');
				objFormData.src = strImageSrc;
				this.startCropper();
			}
			return false;
		},
		startCropper:function() {
			var that = this;
			$.ajax({
				url: UPLOAD_URL,
				type: 'POST',
				dataType: 'json',
				data: objFormData,
				beforeSend: function () {
				},
				success: function (data) {
					that.doneCropper(data);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					that.alert(jqXHR.responseText);
				}
			});
			
		},
		doneCropper: function(data) {
			var html = '<img src="'+data.result+'" height="'+UPLOAD_THUMBNAIL_SIZE+'">';
			html += HTML_BTN_DELETE;
			$ctnUpload.find('.files').html(html)
				.next().val(data.result);
			this.$modal.modal("hide");
			this.stopCropper();
		},
		stopCropper: function() {
			if (blnActive) {
				this.$image.cropper('destroy');
				blnActive = false;
			};
			strImageSrc = null;
			this.$wrapper.empty().removeAttr('style');
			this.$preview.empty().removeAttr('style');
		},

		alert: function (msg) {
			this.$modal.find('.alert').remove();
			if (msg) {
				var $alert = [
					'<div class="alert alert-danger avater-alert">',
					  '<button type="button" class="close" data-dismiss="alert">&times;</button>',
					  msg,
					'</div>'
				  ].join("");
				this.$modal.find('.image-inputs').after($alert);
			};
		},
	};

	$.fn.bindUploadFile = function(settings) {
		var options = $.extend({}, settings);

		this.bind('drop dragover', function (e) {
			e.preventDefault();
	    	var dropZone = $(UPLOAD_CONTAINER),
	        	foundDropzone,
	        	timeout = window.dropZoneTimeout;
	        if (!timeout) {
	            dropZone.addClass('in');
	        } else {
	            clearTimeout(timeout);
	        }

	        var found = false,
	        	node = e.target;
	        do {
	            if ($(node).hasClass('dropzone')) {
	                found = true;
	                foundDropzone = $(node);
	                break;
	            }
	 			node = node.parentNode;
	        } while (node != null);

	        dropZone.removeClass('in hover');

	        if (found) {
	            $ctnUpload = foundDropzone;
	            foundDropzone.addClass('hover');
	        }

	        window.dropZoneTimeout = setTimeout(function () {
	            window.dropZoneTimeout = null;
	            dropZone.removeClass('in hover');
	        }, 100);
	    });

	    $(BTN_UPLOAD).live('click', function() {
	    	$ctnUpload = $(this).parents(UPLOAD_CONTAINER);
	    	if ($ctnUpload.data('title') == UPLOAD_TYPE_CROP && strImageSrc) {
	    		$(MODAL_CROP).modal('show');
	    	} else {
	    		$(MODAL_CROP).modal('hide');
				$(UPLOAD_FILE).trigger('click');
	    	}
		});

		$(BTN_DELETE).live('click', function() {
	    	var $ctn = $(this).parents(UPLOAD_CONTAINER);
	    	var inputValue = $ctn.find('input').val();
			
			$.ajax({
				url: UPLOAD_URL,
				type: 'POST',
				data: {
					title: UPLOAD_TYPE_DELETE,
					inputValue: inputValue
				},
			})
			.always(function() {
				$ctn.find('.files').empty()
					.next().val('');
			});
		});
		
		$.uploadFile(options);
	};

	$.fn.bindUploadFileMCE = function(settings) {
		var options = $.extend({}, settings);
		$ctnUpload = this;
		$(UPLOAD_FILE).trigger('click');
		$.uploadFile(options);
	};

})(jQuery);

// When the server is ready...
$(function () {
	'use strict';

	$('.sheet td').bindUploadFile();

});