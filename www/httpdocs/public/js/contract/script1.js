(function($) {
	$(function() {
		function change_alias( alias ){
			var str = alias;
			str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
			str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
			str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
			str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
			str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
			str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
			str = str.replace(/đ/g,"d");
			str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g,"A"); 
			str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g,"E"); 
			str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g,"I"); 
			str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g,"O"); 
			str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g,"U"); 
			str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g,"Y"); 
			str = str.replace(/Đ/g,"D");
			return str;
		}

		var monthNames = [
			"January", "February", "March",
			"April", "May", "June", "July",
			"August", "September", "October",
			"November", "December"
		];

		function date_en_format(ele){
			var this_val   = ele.val();
			var date = new Date(this_val.split("/").reverse().join("-"));
			var day        = date.getDate();
			var monthIndex = date.getMonth();
			var year       = date.getFullYear();
			var new_date   = day + " " + monthNames[monthIndex] + " " + year;
			return new_date;
		}

		function select_gender(this_val){
			switch(this_val){
				case "Ông":
					val_en = "Mr";
					break;
				case "Bà":
					val_en = "Ms";
					break;
				default:
					val_en = "Mr";
			}
			$('.gender[readonly]').val(this_val);
			$('.gender-en[readonly]').val(val_en);
		}

		function select_job_position(this_val){
			switch(this_val){
				case "Nhân viên phát triển hệ thống":
					val_en = "System Developer";
					break;
				case "Nhân viên quản lý chất lượng":
					val_en = "Quality Controller";
					break;
				case "Nhân viên hành chính văn phòng":
					val_en = "Administration staff";
					break;
				case "Nhân viên lập trình HTML":
					val_en = "HTML Coder";
					break;
				case "Thông dịch viên và quản lý dự án":
					val_en = "Communicator and project management";
					break;
				default:
					val_en = "System Developer";
			}
			$('.job-position[readonly]').val(this_val);
			$('.job-position-en[readonly]').val(val_en);
		}

		function select_position(this_val){
				switch(this_val){
					case "Nhân viên":
						val_en = "Staff";
						break;
					case "Trưởng nhóm":
						val_en = "Leader";
						break;
					case "Quản lý":
						val_en = "Manager";
						break;
					default:
						val_en = "Staff";
				}
				$('.position[readonly]').val(this_val);
				$('.position-en[readonly]').val(val_en);
		}

		function select_scope_work(this_val){
			switch(this_val){
				case "Phát triển web":
					val_en = "Web developer";
					break;
				case "Kiểm tra hệ thống, quản lý dự án":
					val_en = "System test, project management";
					break;
				case "Hành chính văn phòng":
					val_en = "Administration";
					break;
				case "Lập trình HTML":
					val_en = "HTML coder";
					break;
				case "Thông dịch, quản lý dự án":
					val_en = "Translation, project management";
					break;
				default:
					val_en = "Web developer";
			}
			$('.scope-work[readonly]').val(this_val);
			$('.scope-work-en[readonly]').val(val_en);
		}

		function select_birth_place(this_index){
			$(".birth-place-en").find("option").eq(this_index).attr('selected','selected');
		}

		function select_area(this_index){
			$(".area-en").find("option").eq(this_index).attr('selected','selected');
		}
		
		// ====================================================
		//TH1: 2 ngôn ngữ, dữ liệu nhiều input là giống nhau
		/*
		<full-name><input type="text" value="" name="full-name" id="full-name" class="w120px"></full-name>
		<full-name-en><input type="text" value="" name="full-name-en" id="full-name-en" class="w120px" readonly="readonly"></full-name-en>
		<full-name-1><input type="text" value="" name="full-name-1" id="full-name-1" class="w120px full-name"></full-name-1>
		<full-name-en-1><input type="text" value="" name="full-name-en-1" id="full-name-en-1" class="w120px full-name-en" readonly="readonly"></full-name-en-1>
		*/
		$('body').on('keyup change', '.td_body input[type=text]', function(){
			if($(this).hasClass('datepicker')){//kiểu date
				var this_val = $(this).val();
				var input_name = $(this).attr('name');
				var new_date = date_en_format($(this));
				$('.'+input_name +'[readonly]').val(this_val);
				$('.'+input_name+'-en[readonly]').val(new_date);
			}else{//kiểu text thông thường
				var this_val = $(this).val();
				var this_val_en = change_alias(this_val);
				var input_name = $(this).attr('name');
				$('.'+input_name +'[readonly]').val(this_val);
				$('.'+input_name+'-en[readonly]').val(this_val_en);
				if($(this).attr('name').indexOf("salary") == 0 || ($(this).attr('name').match("-allowance$"))){
					this_val = this_val.replace(/[^0-9.,]+/g, "");
					this_val_en = change_alias(this_val);
					this_val_en =  this_val_en.replace(/\.+/g,",")
					$(this).val(this_val);
					$('.'+input_name +'[readonly]').val(this_val);
					$('.'+input_name+'-en[readonly]').val(this_val_en);
				}
			}
		});
	
		// ====================================================
		/* TH2: select , 2 nn */

		$('body').on('keyup change', '.td_body select', function(){
			var this_val = $(this).val();
			var name = $(this).attr('name');

			switch(name){
				case "gender":
					select_gender(this_val);
					break;
				case "job-position":
					select_job_position(this_val);
					break;

				case "position":
					select_position(this_val);
					break;
				case "scope-work":
					select_scope_work(this_val);
					break;
				case "birth-place":
					var this_index = $(this).find("option:selected").index();
					select_birth_place(this_index);
					break;
				case "area":
					var this_index = $(this).find("option:selected").index();
					select_area(this_index);
					break;
				default:
					$('.'+name).val(this_val);
			}
		});
	});
})(jQuery);