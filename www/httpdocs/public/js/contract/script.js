// ==============================================================
// 
// COPYRIGHT(C) CYBRiDGE CORPORATION.
// URL:http://www.cybridge.jp/
// 
// CB-STANDARD for HTML5
// --script.js--
// 
// デザイナー向け UI用スクリプト
// 
// デザイナーが扱うJavaScriptは、プラグイン本体を除き、原則全てここに記述し管理する
// script.jsはデフォルトで読み込み、必要な記述以外を削除・整理して使用する
// ※システム案件の場合、JSの管理はエンジニアの指示に従うこと
// 
// ==============================================================

(function($) {
	$(function() {
		
		// ==============================================================
		// Flashの読み込み swfobject.js
		// ==============================================================
		// ファイルパス、読み込み先のID、幅、高さ、FlashPlayerのバージョン、背景色の順番で記述
		if (typeof(swfobject) == "object") {
			swfobject.embedSWF("/media/cybridge_top.swf", "flashContent", "978", "300", "7", "#FFF");
		}
		
		
		// ==============================================================
		// ユーザーエージェント別の処理
		// ==============================================================
		var userAgent = window.navigator.userAgent;
		var appVersion = window.navigator.appVersion;
		
		// ユーザーエージェント判定
		if (userAgent.indexOf("MSIE") > -1) {
			// IE6～7で出現する、a要素foucs時の点線を消去
			if ((appVersion.indexOf("MSIE 7.0") > -1) || (appVersion.indexOf("MSIE 6.0") > -1)) {
				$("a").focus(function() { this.blur(); });
			}
			
		}
		
		
		// ==============================================================
		// ページの先頭へスクロール
		// ==============================================================
		// .pageTop aをクリックでページの先頭へ
		$(".pageTop a").click(function() {
			$("html, body").animate({ scrollTop : 0 });
			return false;
		});
		
		
		// ==============================================================
		// ホバー処理
		// ==============================================================
		$("a.hover, input.hover").hover(function() {
			$(this).fadeTo("fast", 0.6);
		},function(){
			$(this).fadeTo("fast", 1.0);
		});
		
		
		// ==============================================================
		// フェードイン・フェードアウト
		// ==============================================================
		$(".fade").click(function() {
			
			var elm = $(this);
			
			// 次の要素が非表示だった場合は、.fadeに.activeを付与+次の要素をフェードイン
			if (elm.next().is(":hidden")) {
				elm.addClass("active")
					.next()
					.fadeIn();
				
			// 次の要素が表示されていた場合は、.fadeと.activeを削除+次の要素をフェードアウト
			} else {
				elm.removeClass("active")
					.next()
					.fadeOut();
			}
		});
		
		
		// ==============================================================
		// スライド式トグル
		// ==============================================================
		$(".slideToggle").click(function() {
			
			var elm = $(this);
			elm.next().slideToggle(100);
			
			//.slideTogleに.activeが無ければ、.activeを付与
			if (elm.hasClass("active")) {
				elm.removeClass("active");
			} else {
				elm.addClass("active");
			}
		});
		
		
		// ==============================================================
		// アコーディオンパネル
		// ==============================================================
		$(".accordion").click(function() {
			
			var elm = $(this);
			
			// 開閉時の処理
			if (elm.next(".accordionBox").is(":visible")) {
				elm.removeClass("active")
					.next(".accordionBox")
					.slideUp(400);
					
			} else {
				
				// 兄弟要素の.activeを削除した後、クリックした要素に対して.activeを付与
				elm.next(".accordionBox")
					.slideDown(400)
					.siblings(".accordionBox")
					.slideUp(400)
					.siblings(".accordion")
					.removeClass("active");
				elm.addClass("active");
			}
		});
		
		
		// ==============================================================
		// タブメニュー
		// ==============================================================
		$(".tab a").click(function() {
			
			var elm = $(this);
			
			elm.parent("li")
				.siblings()
				.removeClass("active");
			
			elm.parent("li").addClass("active");
			
			// コンテンツ本体である.tabContentsを一旦隠す
			elm.parents(".tab")
				.next()
				.children(".tabBox")
				.hide();
			
			// htmlにはメニューのa要素href属性に
			// 表示したいボックスのIDを記述する
			// 例） <a href="#tab1"> など
			$(this.hash).fadeIn();
			return false;
		});
		
		
		// ==============================================================
		// フォントサイズ変更 jquery.cookie.js
		// ==============================================================
		if ($.cookie) {
			
			var history = $.cookie("fontSize");
			var elm = $("#wrapper");
			
			if (history) {
				elm.addClass(history);
			}
			
			// #fontChange内のli要素に書かれたidをclassに置き換えて#wrapperに付与する
			// 例） <li id="f17">をクリックすると#wrapperに.f17を付与
			// /css/common.cssの.f10～.f26を使用する
			$("#fontChange li").click(function() {
				
				var setFontSize = this.id;
				var siteDomain = location.hostname;
				
				$.cookie("fontSize", setFontSize, {
					path	: "/",
					domain	: siteDomain
				});
				
				elm.removeClass().addClass(setFontSize);
			});
		}
		
		
		// ==============================================================
		// 画像の遅延読み込み jquery.lazyload.js
		// ==============================================================
		// .lazyが付与されたimg要素をスクロールと共にフェードインさせる
		if ($().lazyload) {
			$("img.lazy").lazyload({
				effect		: "fadeIn",
				placeholder	: "/img/module/loading.gif",
				threshold	:"3",
				event		:"scroll"
			});
		}
		
		
		// ==============================================================
		// ライトボックス jquery.colorbox.js
		// ==============================================================
		if ($().colorbox) {
			// 画像をグループ化
			$("a.colorboxGroup").colorbox({
				rel			: "group",
				maxWidth	: "90%",
				maxHeight	: "90%"
			});
			
			// Ajaxで外部ファイル読み込み
			$("a.colorboxAjax").colorbox();
			
			// Youtubeの読み込み
			$("a.colorboxYoutube").colorbox({
				iframe		: true,
				innerWidth	: 425,
				innerHeight	: 344
			});
			
			// iframeで外部サイト読み込み
			$("a.colorboxIframe").colorbox({
				iframe	: true,
				width	:"80%",
				height	: "80%"
			});
			
			// インラインのHTML読み込み
			$("a.colorboxInline").colorbox({
				inline	: true,
				width	: "50%"
			});
		}
		
		
		// ==============================================================
		// ツールチップ jqury.tinyTips.js
		// ==============================================================
		// a.tTipのタイトル属性をツールチップとして表示
		if ($().tinyTips) {
			$("a.tTip").tinyTips("title");
		}
		
		
		// ==============================================================
		// コンテンツスライダー jqury.flexslider.js
		// http://flexslider.woothemes.com/
		// ==============================================================
		if ($().flexslider) {
			// スライダー
			$("div.flexSlider").flexslider({
				animation		: "slide",
				pauseOnAction	: false
			});
			
			// カルーセルパネル
			//itemWidthでボックス単体の横幅を指定
			$("div.flexCarousel").flexslider({
				animation	: "slide",
				itemWidth	: 221,
				itemMargin	: 0
			});
			// last-childが効かないIE対策用に、JSで罫線のスタイルを指定
			$(".carouselBox:last-child article").css("border-right", "none");
		}

		function change_alias( alias ){
			var str = alias;
			str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
			str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
			str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
			str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
			str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
			str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
			str = str.replace(/đ/g,"d");
			str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g,"A"); 
			str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g,"E"); 
			str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g,"I"); 
			str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g,"O"); 
			str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g,"U"); 
			str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g,"Y"); 
			str = str.replace(/Đ/g,"D");
			return str;
		}

		//input field

		$('body').on('keyup change', '#contract-no', function(){
			var this_val = $(this).val();
			$("#contract-no-en").val(this_val);
		});

		$('body').on('keyup change', '#full-name', function(){
			var this_val = $(this).val();
			var textMark = change_alias(this_val);
			$("#full-name-en").val(textMark);

			$(".full-name").val(this_val);
			$(".full-name-en").val(textMark);

			$("#staff-name").val(this_val);
		});

		$('body').on('keyup change', '#id-card', function(){
			var this_val = $(this).val();
			$("#id-card-en").val(this_val);
		});

		$('body').on('keyup change', '#labor-book', function(){
			var this_val = $(this).val();
			$("#labor-book-en").val(this_val);
		});

		$('body').on('keyup change', '#salary-basic', function(){
			var this_val = $(this).val();
			this_val = this_val.replace(/[^0-9.,]+/g, "");
			$("#salary-basic").val(this_val);
			this_val = this_val.replace(/,/g,".");
			
			$("#salary-basic-en").val(this_val);
		});

		$('body').on('keyup change', '#salary-responsibility', function(){
			var this_val = $(this).val();
			this_val = this_val.replace(/[^0-9.,]+/g, "");
			$("#salary-responsibility").val(this_val);
			this_val     = this_val.replace(/,/g,".");
			$("#salary-responsibility-en").val(this_val);
		});

		$('body').on('keyup change', '#salary-lunch', function(){
			var this_val = $(this).val();
			this_val = this_val.replace(/[^0-9.,]+/g, "");
			$("#salary-lunch").val(this_val);
			this_val     = this_val.replace(/,/g,".");
			$("#salary-lunch-en").val(this_val);
		});

		//date field
		var monthNames = [
			"January", "February", "March",
			"April", "May", "June", "July",
			"August", "September", "October",
			"November", "December"
		];

		function date_en_format(ele){
			var this_val   = ele.val();
			var date       = new Date(this_val);
			var day        = date.getDate();
			var monthIndex = date.getMonth();
			var year       = date.getFullYear();
			var new_date   = day + " " + monthNames[monthIndex] + " " + year;
			return new_date;
		}

		

		$('body').on('keyup change', '.clone_input_trans', function(){
			var ele_id = $(this).attr('id');
			var this_val = $(this).val();
			$("#"+ele_id+'-en').val(this_val);
		});


		$('body').on('keyup change', '.datepicker', function(){
			var ele_id = $(this).attr('id');
			var new_date = date_en_format($(this));
			$("#"+ele_id+'-en').val(new_date);
		});

		$('body').on('keyup change', '#issue', function(){
			var new_date = date_en_format($(this));
			$("#issue-en").val(new_date);
		});

		$('body').on('keyup change', '#date-from', function(){
			var new_date = date_en_format($(this));
			$("#date-from-en").val(new_date);
		});

		$('body').on('keyup change', '#date-to', function(){
			var new_date = date_en_format($(this));
			$("#date-to-en").val(new_date);
		});

		$('body').on('keyup change', '#date-appied', function(){
			var new_date = date_en_format($(this));
			$("#date-appied-en").val(new_date);
		});

		$('body').on('keyup change', '#date-created', function(){
			var new_date = date_en_format($(this));
			$("#date-created-en").val(new_date);
		});

		//select field

		$('body').on('keyup change', '#gender', function(){
			var this_val = $(this).val();
			switch(this_val){
				case "Ông":
					this_val = "Mr";
					break;
				case "Bà":
					this_val = "Ms";
					break;
				default:
					this_val = "Mr";
			}
			$("#gender-en").val(this_val);
		});

		$('body').on('keyup change', '#title', function(){
			var this_val = $(this).val();
			switch(this_val){
				case "Nhân viên phát triển hệ thống":
					this_val = "System Developer";
					break;
				case "Nhân viên quản lý chất lượng":
					this_val = "Quality Controller";
					break;
				case "Nhân viên hành chính văn phòng":
					this_val = "Administration staff";
					break;
				case "Nhân viên lập trình HTML":
					this_val = "HTML Coder";
					break;
				case "Thông dịch viên và quản lý dự án":
					this_val = "Communicator and project management";
					break;
				default:
					this_val = "System Developer";
			}
			$("#title-en").val(this_val);
		});

		$('body').on('keyup change', '#position', function(){
			var this_val = $(this).val();
			switch(this_val){
				case "Nhân viên":
					this_val = "Staff";
					break;
				case "Trưởng nhóm":
					this_val = "Leader";
					break;
				case "Quản lý":
					this_val = "Manager";
					break;
				default:
					this_val = "Staff";
			}
			$("#position-en").val(this_val);
		});

		$('body').on('keyup change', '#title-02', function(){
			var this_val = $(this).val();
			switch(this_val){
				case "Phát triển web":
					this_val = "Web developer";
					break;
				case "Kiểm tra hệ thống, quản lý dự án":
					this_val = "System test, project management";
					break;
				case "Hành chính văn phòng":
					this_val = "Administration";
					break;
				case "Lập trình HTML":
					this_val = "HTML coder";
					break;
				case "Thông dịch, quản lý dự án":
					this_val = "Translation, project management";
					break;
				default:
					this_val = "Web developer";
			}
			$("#title-en-02").val(this_val);
		});

		$('body').on('keyup change', '#birth-place', function(){
			var this_index = $(this).find("option:selected").index();
			$("#birth-place-en").find("option").eq(this_index).attr('selected','selected');
		});

		$('body').on('keyup change', '#area', function(){
			var this_index = $(this).find("option:selected").index();
			$("#area-en").find("option").eq(this_index).attr('selected','selected');
		});
	});
})(jQuery);