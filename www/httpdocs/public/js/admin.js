// ==============================================================
// 
// COPYRIGHT(C) CYBRiDGE CORPORATION.
// URL:http://www.cybridge.jp/
// 
// CB-STANDARD for HTML5
// --admin.js--
// 
// 管理画面用スクリプト
// 
// ==============================================================

(function($) {
	$(function() {
		
		// ==============================================================
		// 確認アラート
		// ==============================================================
		$(".confirm").click( function() {
			return confirm($(this).attr("data-alert"));
		});
		
		
		// ==============================================================
		// バリデーション FormValidator
		// ==============================================================
		if ($().validationEngine) {
			$("form").validationEngine();
		}
		
		
		// ==============================================================
		// jQuery UI カレンダー
		// ==============================================================
		

		if ($().datepicker) {
			$("input.datepicker").datepicker({
				dateFormat : "dd/mm/yy"
			});
		}
		
		
		// ==============================================================
		// Google Maps
		// ==============================================================
		
		
		
		// ==============================================================
		// ログイン画面 エラー時のシェイク
		// ==============================================================
		// $("#loginForm").submit(function() {
			// 実際に使用するにはバリデーションの処理が必要になります。
			var error = $('#loginForm .error').text();
			var perror = $('#loginForm .alert').text();
			if(error.length > 0 || perror.length >0){
				$("#loginWrapper").animate({ left: -30 }, 75)
					.animate({ left: 30 }, 75)
					.animate({ left: -15 }, 75)
					.animate({ left: 15 }, 75)
					.animate({ left: -7 }, 75)
					.animate({ left: 7 }, 75)
					.animate({ left: 0 }, 75);
			}
			// return false;
		// });

		$.fn.colorErrorInputs = function(options) {
			var $frm = this;
			var defaults = {
				elmError: 'ul.error li',
				clsError: 'errorValidate'
			};
			$.extend(defaults, options);

			$frm.find(defaults.elmError).each(function(index, el) {
				var name = $(el).data('name');
				$frm.find('input[name="'+name+'"], textarea[name="'+name+'"], select[name="'+name+'"]')
					.addClass(defaults.clsError);
			});

			$frm.find('.errorValidate').focus(function(event) {
				$(this).removeClass(defaults.clsError);
			});
		}
		
		
	});
})(jQuery);








(function($) {
	$(function() {

	// ==============================================================
	// Ajax delete
	// ==============================================================
	$('.ajax_delete').on('click', function(e){
	    e.preventDefault();
	    var that = $(this);
	    var strAlert  = '本当 に削除' + $(this).data('id') + 'してもよろしいですか？';
	    if(confirm(strAlert)){
	        var param = {
	                _type : "delete",
	                id    : $(this).data('id'),
	                table : $(this).data('table'),
	                force : $(this).data('force'),
	            };
	        var url = '/ajax/';
	        if(typeof($(this).data('table')) != 'undefined') {
	            callAjax(param, url, function(res) {
	                if(res == "success") {
	                    if(that.attr('data-url')){
	                        window.location.href = that.attr('data-url');
	                    }else{
	                        that.parents('tr').remove();
	                    }
	                }
	            });
	        }
	    }
	});
	/* END Script */
	});
	})(jQuery);

function callAjax(param,url,callback){
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: param,
        success: function(res) {
            callback(res);
        },
        fail: function(res) {
            callback(res);
        }
    });
}
