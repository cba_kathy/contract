<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class DM_Action_index extends DM_Action {

	public $_isForm = false;
	public $_freeze = false;
	public $_method = "get";

	public function init(){
		$this->httpRedirect('/admin/');
		parent::init();
	}

	public function validate(){

	}

	public function done($data){

	}

}
