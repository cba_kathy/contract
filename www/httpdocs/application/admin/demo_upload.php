<?php

/**
 * demo_upload.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class DM_Action_Admin_demo_upload extends DM_Action_admin {

	public $_isForm = true;
	public $_freeze = true;
	public $_method = "post";

	public function init(){	

	}

	public function validate(){
		$this->_elements = array(
			"demo[mce_01]" => array(
				"type" => "textarea",
				"name" => "tinyMCE",
			),
			"demo[file]" => array(
				"type" => "text",
				"name" => "Upload File",
			),
			"demo[image]" => array(
				"type" => "text",
				"name" => "Upload Image",
			),
			"demo[crop]" => array(
				"type" => "text",
				"name" => "Crop Image",
			),
			"demo[video]" => array(
				"type" => "text",
				"name" => "Upload Video",
			),
		);
	}

	public function done($data){
		d($data);
	}
}
