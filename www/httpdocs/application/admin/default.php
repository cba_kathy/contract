<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class DM_Action_Admin_default extends DM_Action_admin {

	public $_isForm = false;
	public $_freeze = false;
	public $_method = "get";

	public function init(){	
		
	}

	public function validate(){
		
	}

	public function done($data){
	
	}
