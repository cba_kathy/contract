<?php

class DM_Action_Admin_admin_list extends DM_Action_admin
{

	public $_isForm = false;
	public $_freeze = false;
	public $_method = "get";

	private $limit = 10;

	public function init(){

	}

	public function validate(){
		$this->_elements = array(
			"keyword" => array(
				"type"    => "text",
				"name"    => "Keyword",
			),
		);
	}

	public function done($data){
		$sort_col = $this->db_admin->info('priKey');

		$sorts = $this->getSortParams($data, $sort_col, array());

		if(isset($data['keyword'])){
			$conditions = array(
				"( fullname LIKE ? OR username LIKE ? OR mail LIKE ? )" => "%" . $data['keyword'] . "%",
			);
		}
		
		$opt = array(
			"fields"     => array("*"),
			"joins"      => array(),
			"conditions" => $conditions,
			"order"      => $sorts['order'],
			"limit"      => $this->limit,
		);
		$data_p = $this->db_admin->find('pager', $opt);

		$this->assign("role", $this->get_roles());
		$this->assign("admin_p", $data_p);
		$this->assign("sorts", $sorts);

	}

}
