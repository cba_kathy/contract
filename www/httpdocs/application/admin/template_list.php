<?php

class DM_Action_Admin_template_list extends DM_Action_admin
{

	public $_isForm = false;
	public $_freeze = false;
	public $_method = "get";

	private $limit = 10;

	public function init(){

	}

	public function validate(){
		
		$this->_elements = array(
			"name" => array(
				"type"    => "text",
				"name"    => "Name",
			),
		);

	}

	public function done($data){
		$sort_col = $this->db_category->info('priKey');
		$sorts = $this->getSortParams($data, $sort_col, array());

		
		$whereConditions = array(
			"name" => array("name", "LIKE ?"),
		);
		$exception = array();
		$conditions = $this->getWhereConditions($data, $whereConditions, $exception);

		$opt = array(
			"fields" => array("*"),
			"joins" => array(),
			"conditions" => $conditions,
			"order" => $sorts['order'],
			"limit" => $this->limit,
		);
		$data_p = $this->db_category->find('pager', $opt);
		$this->assign("category_p", $data_p);
		$this->assign("sorts", $sorts);

	}

}
