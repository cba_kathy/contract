<?php

class DM_Action_admin_index extends DM_Action_admin {
	
	
	public $_isForm = true;
	public $_freeze = true;
	public $_method = "post";


	public function init(){
		$this->id = $this->get('id');
		if($this->id){
			$list_contract = $this->db_contract->getList(15, $this->id);
		}else{
			$list_contract = $this->db_contract->getList(15);
		}

		foreach ($list_contract as $k => $v) {
			$list_contract[$k]['permission_edit'] = $this->getPermission('contract_edit', $v['id']);
			if($list_contract[$k]['signed_date'] == '0000-00-00'){
				$list_contract[$k]['signed_date'] = '';
			}
			if($list_contract[$k]['end_date'] == '0000-00-00'){
				$list_contract[$k]['end_date'] = '';
			}
		}

		if($this->id){
			echo json_encode($list_contract);
			exit;
		}else{
			$this->assign("list_contract", $list_contract);
		}

	}// END: init()

}// END: Class
