<?php

class DM_Action_Admin_login extends DM_Action {

	public $_isForm = true;
	public $_freeze = false;
	public $_method = "post";

	private $remember;

	public function init()
	{
		if ($this->session->admin) {
		}
		
		$cookie = $_COOKIE['admin_cookie'];

		if($cookie){
			$cookie = json_decode(base64_decode($cookie));
			
			$admin = $this->db_admin->getBy(array('id', 'fullname', 'role', 'role_desc'), array('username'=> $cookie->username));
			if($admin){
				$this->session->admin = $admin;
				$this->httpRedirect($this->getRedirectUri());
			} else {
				setcookie(ADMIN_COOKIE, "", -1, "/", $_SERVER['SERVER_NAME'], 0, true);
			}
		}
	}

	public function validate(){

		$this->_elements = array(
			"admin[username]" => array(
                "name"    => "Username",
                "type" 	  => "text",
                "require" => true,
				"rule"    => array(
				),
            ),
            
            "admin[password]" => array(
                "name"    => "パスワード",
                "type"    => "password",
                "require" => true,
				"rule"    => array(
					"login" => array(
						"elements" => array("admin[id]", "admin[password]"),
						"options" => array("page" => "admin"),
					),
				),
            ), 
            "admin[remember]" => array(
				"type" => "checkbox_one",
				"name" => "Remember",
				"value" => $_COOKIE[ADMIN_REMEMBER] ? 1 : '',
			)
		);
	}

	public function done($data){

		$admin = (object)$data['admin'];

		$login_flag = false;

		$admin_info = $this->db_admin->getBy(array(), array('admin.username'=> $admin->username));
        if ($admin_info && DM_Action::verifyPassword($admin->password, $admin_info['password'])) {
            $login_flag = true;
        }else{
        	$this->assign('error_login', 'ユーザーネームまたはパスワードが違います');
        }

		if ($login_flag) {

			if ($admin->remember) {
				$cookie_data = array(
					"username" => $admin->username,
					"token"    => $this->randomString(),
				);
				setcookie(ADMIN_COOKIE, base64_encode(json_encode($cookie_data)), time()+3600*24*90, "/", $_SERVER['SERVER_NAME'], 0, true); //Hour, Day, Month
				setcookie(ADMIN_REMEMBER, 1, time()+3600*24*90); //Hour, Day, Month
			} else {
				setcookie(ADMIN_REMEMBER, "", -1); //Hour, Day, Month
			}
			
			$this->session->admin = array(
				'id'        => $admin_info['id'],
				'fullname'  => $admin_info['fullname'],
				'role'      => $admin_info['role'],
				'role_desc' => $admin_info['role_desc'],
			);
			$this->setPermission();
			$this->httpRedirect($this->getRedirectUri());
		}

	}

}
