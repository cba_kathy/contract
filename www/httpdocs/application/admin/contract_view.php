<?php

class DM_Action_Admin_contract_view extends DM_Action_admin
{

	public $_isForm = true;
	public $_freeze = false;
	public $_method = "get";

	private $id;

	public function init()
	{
		parent::init();
	}// END: init()

	public function validate()
	{
		$this->id = $this->get('id');
		$list_category = $this->db_category->getList();
		if($this->id){
			$contract = $this->db_contract->getById($this->id);
			if (!$contract) {
				$this->httpRedirect('/contract_list');
			}
			if($this->session->admin['role'] == 2){
				$user_not_permission = (array)json_decode($this->session->admin['role_desc']);
				$user_not_permission = array_keys($user_not_permission);
				if(in_array($contract['staff_id'], $user_not_permission )){
					$flag_not_see = true;
				}
			}

			$category = $this->db_category->getById($contract['category_id']);
			$param = json_decode($contract['body']);
			foreach ($param as $k => $v) {
				preg_match("'<".$k.">(.*?)</".$k.">'si", $category["body"], $match);
				if($match) {

					 
					$input_type = '';
					if(strpos($match[0], 'type="text"')){
						$input_type = "input_text";
					}
					if(strpos($match[0], 'type="radio"')){
						$input_type = "input_radio";
					}
					if(strpos($match[0], '<textarea')){
						$input_type = "textarea";
					}
					if(strpos($match[0], '<select')){
						$input_type = "select";
					}
					if( isset($flag_not_see) && in_array($k, array('salary-basic', 'salary-basic-en', 'salary-responsibility', 'salary-responsibility-en', 'basic-salary-before-1', 'basic-salary-after-1', 'basic-salary-before-2', 'basic-salary-after-2'))){
						continue;
					}
					if(in_array($this->session->admin['role'], array(3,4)) && in_array($k, array('salary-basic', 'salary-basic-en', 'salary-responsibility', 'salary-responsibility-en', 'basic-salary-before-1', 'basic-salary-after-1', 'basic-salary-before-2', 'basic-salary-after-2'))){
						continue;
					}
					switch ($input_type) {
						case 'input_text':
								$value_old = 'value=""';
								if(preg_match("/salary/", $k)){
										$value_new = 'value="'.$this->mc_decrypt($v, ENCRYPTION_KEY).'"';
								}else{
									$value_new = 'value="'.$v.'"';
								}
								$dom_old = $match[0];

								// loại bỏ các giá trị trong value nếu mà bị set sẵn
								preg_match('/value="(.*?)"/mis', $dom_old, $exits_value);
								if(isset($exits_value[1])){
									$value_old = 'value="'.$exits_value[1].'"';
								}
								//

								$dom_new = str_replace($value_old, $value_new, $dom_old);
								$category["body"] = preg_replace('#<'.$k.'>.*</'.$k.'>#m', $dom_new, $category["body"]);
							break;
						case 'select':
							$match[0] = str_replace("selected", '', $match[0]);
							
							$value_old = 'value="'.$v.'"';
							$value_new = 'value="'.$v.'" selected';
							$dom_old = $match[0];
							$dom_new = str_replace($value_old, $value_new, $dom_old);
							$category["body"] = preg_replace('#<'.$k.'>.*</'.$k.'>#s', $dom_new, $category["body"]);//utf8
							break;
						case 'textarea':
							$dom_new = $v;
							$category["body"] = preg_replace('#<'.$k.'>.*</'.$k.'>#m', $dom_new, $category["body"]);
							break;
						default:
							break;
					}
				
				}

			}
			$this->assign("category", $category["name"]);
			$this->assign("category_body", $category["body"]);
			$this->assign("contract", $contract);
			$this->_defaults['contract'] = $contract;
		}

		
		$this->_elements = array(
			"contract[id]" => array(
				"type"    => "hidden",
				"name"    => "Id",
				"require" => false,
			),
			"contract[name]" => array(
				"type"    => "text",
				"name"    => "Name",
				"require" => true,
				"max"		=> 255,
			),
			"contract[category_id]" => array(
				"type"    => "select",
				"name"    => "Category",
				"options" => $list_category,
				"require" => true,
				"value"   => $contract['category_id'],

			),
			"contract[body]" => array(
				"type"    => "textarea",
				"name"    => "Body",
				"require" => false,
			),
		);


	}// END: validate()

	public function done($data)
	{
		$contract = $data['contract'];
		if ($this->db_contract->autoIu($contract)) {
			$this->httpRedirect("/admin/contract_list");
		}
		
	}

}
