<?php

class DM_Action_Admin_contract_edit extends DM_Action_admin
{

	public $_isForm = true;
	public $_freeze = false;
	public $_method = "post";

	private $id;

	public function init()
	{
		parent::init();
	}// END: init()

	public function validate()
	{
		$this->id = $this->get('id');
		$list_category = $this->db_category->getList();
		$list_category_body = $this->db_category->getListBody();
		$this->assign("list_category_body", $list_category_body);

		if($this->id){//edit
			if(isset($_POST['contract'])){//post
				$contract = $_POST['contract'];
			}else{//load first
				$contract = $this->db_contract->getById($this->id);
				if(empty($contract)){
					$this->httpRedirect("/admin/contract_list");
				}
			}
			$this->convertTemplate($contract);

		}else{//create
			if(isset($_POST['contract'])){//post
				$contract = $_POST['contract'];
				$this->convertTemplate($contract);
			}else{//load first
				//khi đã add 1 labor contract thì cho phép tự động điền thông tin cho những lần add sau này
				$this->assign("category_body", $list_category_body[1]);
			}
		}

		$this->_elements = array(
			"contract[id]" => array(
				"type"    => "hidden",
				"name"    => "Id",
				"require" => false,
			),
			"contract[name]" => array(
				"type"    => "text",
				"name"    => "Contract Name",
				"require" => true,
				"max"     => 255,
			),
			"contract[category_id]" => array(
				"type"    => "select",
				"name"    => "Category",
				"options" => $list_category,
				"require" => true,
				"value"   => $contract['category_id'],
			),
			"contract[body]" => array(
				"type"    => "textarea",
				"name"    => "Body",
				"require" => false,
			),
			"contract[staff_id]" => array(
				"type"    => "text",
				"name"    => "Staff",
				"require" => true,
			),
			"contract[staff_tmp]" => array(
				"type"    => "text",
				"name"    => "staff_tmp",
				"require" => false,
			),
			"contract[admin_id]" => array(
				"type"    => "hidden",
				"name"    => "Admin Create",
				"require" => false,
			),
			"contract[signed_date]" => array(
				"type"    => "text",
				"name"    => "Signed date",
				"require" => false,
			),
			"contract[end_date]" => array(
				"type"    => "text",
				"name"    => "End date",
				"require" => false,
			),
		);


	}// END: validate()

	public function done($data)
	{
		$data['contract']['admin_id'] = $this->session->admin['id'];
		if(!empty($data['contract']['signed_date'])){
			$data['contract']['signed_date'] = str_replace('/', '-', $data['contract']['signed_date']);
			$data['contract']['signed_date'] = date('Y-m-d', strtotime($data['contract']['signed_date']));
		}
		
		if(!empty($data['contract']['end_date'])){
			$data['contract']['end_date'] = str_replace('/', '-', $data['contract']['end_date']);
			$data['contract']['end_date'] = date('Y-m-d', strtotime($data['contract']['end_date']));
		}
		

		$contract = $data['contract'];

		if($this->session->admin['role'] == 2 ){//ko đc xem mức lương  của leader/manager 
			$user_not_permission = (array)json_decode($this->session->admin['role_desc']);
			$user_not_permission = array_keys($user_not_permission);

			if(in_array($data['contract']['staff_id'], $user_not_permission)){//ko đc phép xem lương của các user này
				$contract['body'] = $this->get_body_new($contract);
			}
		}

		if($this->session->admin['role'] == 3 ){//ko đc xem mức lương tất cả nhân viên
			$contract['body'] = $this->get_body_new($contract);
		}
		$body = (array)json_decode($contract['body']);
		foreach ($body as $k => $v) {
			if(preg_match("/salary/", $k)){
				$body[$k]= $this->mc_encrypt($v, ENCRYPTION_KEY);
			}
		}
		$contract['body'] = json_encode($body);
		if(isset($body['contract-no'])){
			$contract['contract_number']= $body['contract-no'];
		}
		if(isset($body['job-position-en'])){
			$contract['title']= $body['job-position-en'];
		}
		if ($this->db_contract->autoIu($contract)) {
			$this->httpRedirect("/admin/contract_list");
		}
		
	}


	public function get_body_new($contract = null){
		$body_new = (array)json_decode($contract['body']);

			if(isset($contract['id'])){//edit
				$contract_old = $this->db_contract->getById($contract['id']);
				$body_old = (array)json_decode($contract_old['body']);
				foreach ($body_new as $k => $v) {
					if(preg_match("/salary/", $k)){
						if(isset($body_old[$k])){
							$body_new[$k] = $body_old[$k];
						}else{
							$body_new[$k] = '';
						}
					}
				}
			}
			return json_encode($body_new);
	}


	public function convertTemplate($contract = null){
		$category = $this->db_category->getById($contract['category_id']);
			$param = json_decode($contract['body']);
			if(!$param){
				$param = array();
			}

			if($this->session->admin['role'] == 2){
				$user_not_permission = (array)json_decode($this->session->admin['role_desc']);
				$user_not_permission = array_keys($user_not_permission);
				if(in_array($contract['staff_id'], $user_not_permission )){
					$flag_not_see = true;
				}
			}

			foreach ($param as $k => $v) {
				preg_match("'<".$k.">(.*?)</".$k.">'si", $category["body"], $match);
				if($match) {

					$input_type = '';
					if(strpos($match[0], 'type="text"')){
						$input_type = "input_text";
					}
					if(strpos($match[0], 'type="radio"')){
						$input_type = "input_radio";
					}
					if(strpos($match[0], '<textarea')){
						$input_type = "textarea";
					}
					if(strpos($match[0], '<select')){
						$input_type = "select";
					}

					switch ($input_type) {
						case 'input_text':
								$value_old = 'value=""';

								if(preg_match("/salary/", $k)){
									if(isset($flag_not_see) || in_array($this->session->admin['role'], array(3,4)) ){
										$value_new = 'value="" readonly="readonly"';
									}else{
										$value_new = 'value="'.$this->mc_decrypt($v, ENCRYPTION_KEY).'"';
									}
								}else{
									$value_new = 'value="'.$v.'"';
								}
								$dom_old = $match[0];
								
								// loại bỏ các giá trị trong value nếu mà bị set sẵn
								preg_match('/value="(.*?)"/mis', $dom_old, $exits_value);
								if(isset($exits_value[1])){
									$value_old = 'value="'.$exits_value[1].'"';
								}
								//
								$dom_new = str_replace($value_old, $value_new, $dom_old);

								$category["body"] = preg_replace('#<'.$k.'>.*</'.$k.'>#m', $dom_new, $category["body"]);
							break;
						case 'select':
							$match[0] = str_replace("selected", '', $match[0]);

							$value_old = 'value="'.$v.'"';
							$value_new = 'value="'.$v.'" selected';
							$dom_old = $match[0];
							$dom_new = str_replace($value_old, $value_new, $dom_old);
							$category["body"] = preg_replace('#<'.$k.'>.*</'.$k.'>#s', $dom_new, $category["body"]);//utf8
							break;
						case 'textarea':
							$dom_old = $match[0];
							$value_new = $v.'</textarea>';
							$value_old = strip_tags($dom_old).'</textarea>';
							$dom_new = str_replace($value_old, $value_new, $dom_old);
							$category["body"] = preg_replace('#<'.$k.'>.*</'.$k.'>#m', $dom_new, $category["body"]);
							break;
						default:
							break;
					}
				
				}
			}

			$user_laborer = $this->db_staff->getBy(array('id'=>$contract['staff_id']));
			$contract['staff_tmp']= $user_laborer['fullname'] .' ('.$user_laborer['nickname'].')';
			$this->assign("category_body", $category["body"]);
			$this->assign("contract", $contract);
			$this->_defaults['contract'] = $contract;
	}

}
