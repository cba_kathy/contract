<?php

class DM_Action_Admin_staff_list extends DM_Action_admin
{

	public $_isForm = false;
	public $_freeze = false;
	public $_method = "get";

	private $limit = 10;

	public function init(){

	}

	public function validate(){
		$this->_elements = array(
			"keyword" => array(
				"type"    => "text",
				"name"    => "Keyword",
			),
		);
	}

	public function done($data){
		$sort_col = $this->db_staff->info('priKey');

		$sorts = $this->getSortParams($data, $sort_col, array());

		$positions  = $this->get_positions();

		if(isset($data['keyword'])){
			$conditions = array(
				"( fullname LIKE ? OR nickname LIKE ? )" => "%" . $data['keyword'] . "%",
			);
		}

		$opt = array(
			"fields"     => array("*"),
			"joins"      => array(),
			"conditions" => $conditions,
			"order"      => $sorts['order'],
			"limit"      => $this->limit,
		);
		$data_p = $this->db_staff->find('pager', $opt);

		$this->assign("positions", $positions);
		$this->assign("staff_p", $data_p);
		$this->assign("sorts", $sorts);
	}

}
