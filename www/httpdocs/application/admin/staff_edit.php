<?php

class DM_Action_Admin_staff_edit extends DM_Action_admin
{

	public $_isForm = true;
	public $_freeze = true;
	public $_method = "post";

	private $id;

	public function init()
	{
		parent::init();
	}// END: init()

	public function validate()
	{
		$this->id = $this->get('id');
		if($this->id){
			$staff = $this->db_staff->getById($this->id);
			unset($staff['password']);
			if (!$staff) {
				$this->httpRedirect('/staff_list');
			}
			$this->_defaults['staff'] = $staff;
			$this->assign('staff', $staff);
		}


		$this->_elements = array(
			"staff[id]" => array(
				"type"    => "hidden",
				"name"    => "Id",
				"require" => false,
			),
			"staff[fullname]" => array(
				"type"    => "text",
				"name"    => "Fullname",
				"require" => true,
				"max"     => 255,
			),
			"staff[nickname]" => array(
				"type"    => "text",
				"name"    => "Nickname",
				"require" => false,
				"max"     => 255,
				"rule"		=> array(
					"exist_value" => array(
						"options" => array("table" => "staff", "field"=>"nickname", "id" => $this->id),
						"message" => "Nickname is exist",
					),
				)
			),
			"staff[position]" => array(
				"type"    => "select",
				"name"    => "Position",
				"require" => false,
				"options" => $this->get_positions()
			),
			"staff[start_work_date]" => array(
				"type"    => "text",
				"name"    => "Start work date",
				"require" => false,
			),
			"staff[image]" => array(
				"type" => "text",
				"name" => "Avatar",
			),
		);
	}// END: validate()

	public function done($data)
	{
		if(!empty($data['staff']['start_work_date'])){
			$data['staff']['start_work_date'] = str_replace('/', '-', $data['staff']['start_work_date']);
			$data['staff']['start_work_date'] = date('Y-m-d', strtotime($data['staff']['start_work_date']));
		}
		if(!empty($data['staff']['password'])){
			$data['staff']['password'] = $this->hashPassword($data['staff']['password']);
		}else{
			unset($data['staff']['password']);
		}
		$staff = $data['staff'];
		if ($this->db_staff->autoIu($staff)) {
			$this->httpRedirect("/admin/staff_list");
		}
		
	}

}
