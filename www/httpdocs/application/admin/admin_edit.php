<?php

class DM_Action_Admin_admin_edit extends DM_Action_admin
{

	public $_isForm = true;
	public $_freeze = true;
	public $_method = "post";

	private $id;

	public function init()
	{
		parent::init();
	}// END: init()

	public function validate()
	{
		$require_pass = true;
		$require_role = true;
		$this->id = $this->get('id');
		if($this->id){
			$admin = $this->db_admin->getById($this->id);
			unset($admin['password']);
			if (!$admin) {
				$this->httpRedirect('/admin_list');
			}
			$admin['role_desc'] = (array)json_decode($admin['role_desc']);
			$this->_defaults['admin'] = $admin;
			$this->assign('admin', $admin);
			$require_pass = false;

			if($admin['role'] == 1){
				$require_role = false;
			}
		}else{
			$this->_defaults['admin']['role'] = 4;
		}


		$this->_elements = array(
			"admin[id]" => array(
				"type"    => "hidden",
				"name"    => "Id",
				"require" => false,
			),
			"admin[fullname]" => array(
				"type"    => "text",
				"name"    => "Full Name",
				"require" => true,
				"max"     => 255,
			),
			"admin[mail]" => array(
				"type"    => "text",
				"name"    => "Email",
				"require" => false,
				"max"     => 255,
				"email"=> true
			),
			"admin[username]" => array(
				"type"    => "text",
				"name"    => "User Name",
				"require" => true,
				"max"     => 255,
				"rule"		=> array(
					"exist_value" => array(
						"options" => array("table" => "admin", "field"=>"username", "id" => $this->id),
						"message" => "Username is exist",
					),
				)
			),
			"admin[password]" => array(
				"type"    => "password",
				"name"    => "Password",
				"require" => $require_pass,
				"max"     => 255,
				"min"     => 6
			),
			"password_confirm" => array(
				"type"    => "password",
				"name"    => "Password Confirm",
				"require" => $require_pass,
				"rule"    => array(
					"compare_2_values" => array(
						"elements" => array(
							"admin[password]",
							"password_confirm"
						),
					),
				),
			),
			"admin[role]" => array(
				"type"    => "select",
				"name"    => "Role",
				"require" => $require_role,
				"options" => $this->get_roles(),
			),
			"admin[role_desc]" => array(
				"type"    => "checkbox",
				"name"    => "role_desc",
				"require" => false,
				"options" => $this->db_staff->getOptions('id', 'fullname'),
			),
			"admin[image]" => array(
				"type" => "text",
				"name" => "Avatar",
			),
		);


	}// END: validate()

	public function done($data)
	{
		if($this->_defaults['admin']['role'] == 1){
			unset($data['admin']['role']);
		}
		$data['admin']['role_desc'] = json_encode($data['admin']['role_desc']);
		if(!empty($data['admin']['password'])){
			$data['admin']['password'] = $this->hashPassword($data['admin']['password']);
		}else{
			unset($data['admin']['password']);
		}
		$admin = $data['admin'];
		if ($this->db_admin->autoIu($admin)) {
			$this->httpRedirect("/admin/admin_list");
		}
		
	}

}
