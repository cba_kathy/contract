<?php

class DM_Action_Admin_contract_list extends DM_Action_admin
{

	public $_isForm = false;
	public $_freeze = false;
	public $_method = "get";

	private $limit = 10;

	public function init(){

	}

	public function validate(){
		$this->_elements = array(
			"keyword" => array(
				"type"    => "text",
				"name"    => "Keyword",
			),
			"staff_id" => array(
				"type"    => "text",
				"name"    => "staff_id",
			),
			"category_id" => array(
				"type"    => "text",
				"name"    => "category_id",
			),
		);
	}

	public function done($data){
		$sort_col = $this->db_contract->info('priKey');
		$sorts = $this->getSortParams($data, $sort_col, array());

		if(isset($data['keyword'])){
			$conditions = array(
				"( contract.name LIKE ? OR contract.contract_number LIKE ? OR contract.title LIKE ? OR c.name LIKE ? OR u2.fullname LIKE ? OR u2.nickname LIKE ?)" => "%" . $data['keyword'] . "%",
			);
		}
		if(isset($data['staff_id'])){
			$conditions = array(
				"staff_id = ?" => $data['staff_id'],
			);
		}
		if(isset($data['category_id'])){
			$conditions = array(
				"category_id = ?" => $data['category_id'],
			);
		}
		$express		= "(SELECT * FROM category)";
		$arr_joins["c"] = array(new Zend_Db_Expr($express), "c.id = contract.category_id", array('c.name as category_name'), "left");
		$arr_joins["u"] = array(new Zend_Db_Expr("(SELECT * FROM admin)"), "u.id = contract.admin_id", array('u.fullname as user_create'), "left");
		$arr_joins["u2"] = array(new Zend_Db_Expr("(SELECT * FROM staff)"), "u2.id = contract.staff_id", array('u2.fullname as user_name', 'u2.nickname'), "left");

		$opt = array(
			"fields"     => array('*'),
			"joins"      => $arr_joins,
			"conditions" => $conditions,
			"order"      => $sorts['order'],
			"limit"      => $this->limit,
		);
		$data_p = $this->db_contract->find('pager', $opt);
		foreach ($data_p['rows'] as $k => $v) {
			$body = json_decode($v['body']);
			$body = (array) $body;
			$data_p['rows'][$k]['contract_no'] = $body['contract-no'];
			if($v['signed_date'] == '0000-00-00'){
				$data_p['rows'][$k]['status'] = 'Not sign yet'; 
			}else{
				$data_p['rows'][$k]['status'] = 'Signed'; 
			}
		}
		$this->assign("contract_p", $data_p);
		$this->assign("sorts", $sorts);
	}

}
