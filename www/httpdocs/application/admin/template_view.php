<?php

class DM_Action_Admin_template_view extends DM_Action_admin
{

	public $_isForm = false;
	public $_freeze = false;
	public $_method = "get";

	private $id;

	public function init()
	{
		parent::init();
	}// END: init()

	public function validate()
	{
	}// END: validate()

	public function done($data)
	{
		$this->id = $this->get('id');
		if($this->id){
			$category = $this->db_category->getById($this->id);
			if (!$category) {
				$this->httpRedirect('/category_list');
			}
		}
		$this->assign("category", $category);
		
	}

}
