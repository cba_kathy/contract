<?php

class DM_Action_Admin_logout extends DM_Action_admin {

	public $_isForm = false;
	public $_freeze = false;
	public $_method = "get";
	
	public function init(){
	}

	public function validate(){
	}

	public function done($data){
		unset($this->session->choices);
		unset($this->session->admin);
		unset($this->session->request_uri);
		unset($_SESSION["CSRF_TOKEN"]);
		
		setcookie(ADMIN_COOKIE, "", -1, "/", $_SERVER['SERVER_NAME'], 0, true);
		
		$this->httpRedirect("/admin/login");
	}

}
