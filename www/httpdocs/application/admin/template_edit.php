<?php

class DM_Action_Admin_template_edit extends DM_Action_admin
{

	public $_isForm = true;
	public $_freeze = true;
	public $_method = "post";

	private $id;

	public function init()
	{
		parent::init();
	}// END: init()

	public function validate()
	{
		$this->id = $this->get('id');
		if($this->id){
			$category = $this->db_category->getById($this->id);
			if (!$category) {
				$this->httpRedirect('/category_list');
			}
			$this->_defaults['category'] = $category;
			$field_exist = "field_exist_update";
		}else{
			$field_exist = "field_exist";
		}

		
		$this->_elements = array(
			"category[id]" => array(
				"type"    => "hidden",
				"name"    => "Id",
				"require" => false,
			),
			"category[name]" => array(
				"type"    => "text",
				"name"    => "Name",
				"require" => true,
				"max"     => 255,
				"trim"    =>true,
				 "rule"    => array(
                    $field_exist   => array(
                        "elements" => array("category[name]"),
                        "options"  => array("category", "name", $this->id),
                        "message"  => "この職種名は既に使用されています。",
                    ),
                ),
			),
			"category[body]" => array(
				"type"    => "textarea",
				"name"    => "Content",
				"require" => true,
				"trim"    =>true
			),
		);

	}// END: validate()

	public function done($data)
	{
		
		$category = $data['category'];
		if ($this->db_category->autoIu($category)) {
			$this->httpRedirect("/admin/template_list");
		}
		
	}

}
