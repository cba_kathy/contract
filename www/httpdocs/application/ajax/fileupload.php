<?php

/**
 * fileupload.php
 *
 * @category    CB_Framework
 * @package
 * @subpackage  admin
 * @copyright   CYBRiDGE
 * @license     CYBRiDGE License 3.1
 *
 */

class DM_Action_ajax_fileupload extends DM_Action_ajax {
    
    public $_isForm = false;
    public $_csrf = false;
    public $_method = "post";

    private $result;
    private $msg;

    public function done($data){

        $title = $this->post('title');
        $inputValue = $this->post('inputValue');
        $inputName = $this->post('inputName');
        $file = $_FILES['file'];

        $options = array(
            UPLOAD_TYPE_FILE => array(
                "dir" => UPLOAD_DIR."/file",
                "name" => $inputName."_".date('YmdHis'),
            ),
            UPLOAD_TYPE_IMAGE => array(
                "dir" => UPLOAD_DIR."/image",
                "name" => $inputName."_".date('YmdHis'),
            ),
            UPLOAD_TYPE_VIDEO => array(
                "dir" => UPLOAD_DIR."/video",
                "name" => $inputName."_".date('YmdHis'),
            ),
            UPLOAD_TYPE_MCE_IMAGE => array(
                "dir" => UPLOAD_DIR."/mce",
                "name" => date('YmdHis')."_img",
            ),
            UPLOAD_TYPE_MCE_VIDEO => array(
                "dir" => UPLOAD_DIR."/mce",
                "name" => date('YmdHis')."_video",
            ),
            UPLOAD_TYPE_CROP => array(
                "dir" => UPLOAD_DIR."/crop",
                "name" => $inputName."_".date('YmdHis'),
            ),
        );

        switch ($title) {
            case UPLOAD_TYPE_FILE:
            case UPLOAD_TYPE_IMAGE:
            case UPLOAD_TYPE_VIDEO:
            case UPLOAD_TYPE_MCE_IMAGE:
            case UPLOAD_TYPE_MCE_VIDEO:
                $dir = $options[$title]['dir'];
                $name = $options[$title]['name'];
                $this->uploadFile($file, $dir, $name);
                break; 
            case UPLOAD_TYPE_CROP:
                $src = $this->post('src');
                if ($src && file_exists(CB_PUBLIC.$src)) {
                    $cData = (object)$this->post('cData');

                    $tmp_file = CB_PUBLIC.$src;
                    $dir = $options[$title]['dir'];
                    $name = $options[$title]['name'];
                    $this->cropImage($tmp_file, $cData, $dir, $name);

                } else {
                    $dir = UPLOAD_DIR."/tmp";
                    $name = "tmp_image";
                    $this->uploadFile($file, $dir, $name);
                }

                break;
            case UPLOAD_TYPE_DELETE:
                if ($inputValue && is_array($this->session->files) && in_array($inputValue, $this->session->files)) {
                    $key = array_search($inputValue, $this->session->files);
                    $this->deleteFile($inputValue);
                    unset($this->session->files[$key]);
                }
                break;
        }
        if ($title == UPLOAD_TYPE_DELETE) {
            echo "deleted file"; exit;
        } else {
            if ($this->result) {
                $this->session->files[] = $this->result;
            }
            $response = array(
                'message' => $this->msg,
                'result' => $this->result,
            );
            $response = array_map("htmlspecialchars", $response);
            echo json_encode($response); exit;
        }

    }

    private function uploadFile($file, $dir, $name) {
        $errorCode = $file['error'];

        if ($errorCode === UPLOAD_ERR_OK) {
            if ($this->setDir($dir)) {
                $extension = ".".end(explode('.', $file['name']));
                $dst = $dir."/".$name.$extension;
                $result = move_uploaded_file($file['tmp_name'], CB_PUBLIC.$dst);
                if ($result) {
                    $this->result = $dst;
                } else {
                    $this->msg = "Failed to save file. Please check permission of folder \"{$dir}\"";
                }
            } else {
                $this->msg = 'Failed to create folder "'.$dir.'"';
            }
        } else {
            $this->msg = $this -> codeToMessage($errorCode);
        }
    }

    private function cropImage($tmp_file, $cData, $dir, $name) {
        if ($this->setDir($dir)) {
            $Image = new CB_ImageEditor();
            $Image->load($tmp_file);
            unlink($tmp_file);
            
            $info = $Image->getImageInfo();
            $Image->crop($cData->width, $cData->height, $cData->x, $cData->y);
            $dst = CB_PUBLIC . $dir . "/" . $name . "." . $info['type'];
            
            $Image->save($dst, $info['type']);
            $Image->free();
            $this->result = str_replace(CB_PUBLIC, '', $dst);
            
        } else {
            $this->msg = 'Failed to create folder "'.$dir.'"';
        }
    }

    private function setDir($dir) {
        if (strpos($dir, CB_PUBLIC) === false) {
            $dir = CB_PUBLIC . "/" . ltrim($dir, "\/");
        }
        if (!file_exists($dir)) {
            return mkdir($dir, 0777, true);
        }
        return true;
    }

    private function codeToMessage($code) {
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE:
                $message = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $message = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                break;
            case UPLOAD_ERR_PARTIAL:
                $message = 'The uploaded file was only partially uploaded';
                break;
            case UPLOAD_ERR_NO_FILE:
                $message = 'No file was uploaded';
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $message = 'Missing a temporary folder';
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $message = 'Failed to write file to disk';
                break;
            case UPLOAD_ERR_EXTENSION:
                $message = 'File upload stopped by extension';
                break;

            default:
                $message = 'Unknown upload error';
        }
        return $message;
    }

    private function base64ToImage($src) {
        $base64 = base64_decode(end(explode(',', $src)));
        $tmp_file = CB_TMP . "/" . md5(time().microtime());
        file_put_contents($tmp_file, $base64);
        return $tmp_file;
    }

    private function imageToBase64($file) {
        $tmp_file = CB_TMP . "/" . md5(time().microtime());
        move_uploaded_file($file['tmp_name'], $tmp_file);
        $type = pathinfo($tmp_file, PATHINFO_EXTENSION);
        $data = file_get_contents($tmp_file);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        unlink($tmp_file);
        return $base64;
    }

}
