<?php

class DM_Action_Ajax_index extends DM_Action_admin
{
    var $_isForm        = false;
    var $_freeze        = false;
    var $_method        = "post";
    var $table          = false;
    var $id             = false;
    var $status         = false;
    var $_type          = false;
    var $entry_ids      = false;
    var $force          = false;
    var $store_category = false;

    public function init()
    {
        parent::init();
        if(! $_POST) {
            echo json_encode("no result"); exit;
        }
    }// END: init()

    public function validate()
    {
        $this->table          = $this->post("table");
        $this->id             = $this->post("id");
        $this->status         = $this->post("status");
        $this->_type          = $this->post("_type");
        $this->entry_ids      = $this->post("entry_ids");
        $this->force          = $this->post("force");
        $this->store_category = $this->post("store_category");
    }// END: validate()

    public function done($data)
    {

        switch ($this->_type) {
            case 'getCategoryTemplate':
                $this->getCategoryTemplate();
                 break;
            case 'change_status':
                $result = $this->changeStatus();
                echo json_encode($result);
                break;
            case 'delete':
                $result = $this->delete();
                echo json_encode($result);
                break;
            case 'checkExistContract':
                 $result = $this->checkExistContract();
                echo json_encode($result);
                break;
            default:
                $result = "no result";
                echo json_encode($result);
                break;
        }

        exit();
    }// END: done()


    public function getCategoryTemplate(){
        $category_id = $this->post("category_id");
        $staff_id = $this->post("staff_id");


        if(!empty($staff_id)){//tự động điền dữ liệu

            $contract_exist = $this->db_contract->getByLaborer($staff_id, 1);
            if(!empty($contract_exist['body'])){
                $contract_exist['category_id'] = $category_id;
                $body['body'] =  $this->convertTemplate($contract_exist);
            }else{
                //tìm kiếm dữ liệu các mẫu hợp đồng khác
                $contract_exist = $this->db_contract->getExistContract($staff_id);
                if(!empty($contract_exist['body'])){
                    $contract_exist['category_id'] = $category_id;
                    $body['body'] =  $this->convertTemplate($contract_exist);
                }else{
                    $body = $this->db_category->getById($category_id);
                }
            }
        }else{
            $body= $this->db_category->getById($category_id);
        }
        echo $body['body'];
        return;
    }


    public function changeStatus()
    {
        $options = array(
            "{$this->table}_id"     => $this->id,
            "{$this->table}_status" => $this->status,
        );
        if($this->table == "store" && $this->status == "非公開") {
            $options["{$this->table}_unpublic_on"] = date('Y-m-d H:i:s');
        }
        $result  = $this->{"db_{$this->table}"}->autoUpdate($options);
        return ($result == "update") ? "success" : "none";
    } // END: changeStatus()


    public function delete()
    {
        $options = array(
            "{$this->table}.id"        => $this->id,
            "{$this->table}_is_delete" => "1",
        );
        if($this->force == 'force') {
            $result = $this->{"db_{$this->table}"}->autoDelete($this->id);
        } else {
            $result = $this->{"db_{$this->table}"}->autoUpdate($options);
        }
        return ( $result || $result == "update") ? "success" : "none";
    } // END: delete()



    //lấy các giá trị có sẵn điền vô
    public function convertTemplate($contract = null){
        $category = $this->db_category->getById($contract['category_id']);
            $param = json_decode($contract['body']);
            if(!$param){
                $param = array();
            }

            if($this->session->admin['role'] == 2){
                $user_not_permission = (array)json_decode($this->session->admin['role_desc']);
                $user_not_permission = array_keys($user_not_permission);
                if(in_array($contract['staff_id'], $user_not_permission )){
                    $flag_not_see = true;
                }
            }

            foreach ($param as $k => $v) {
                preg_match("'<".$k.">(.*?)</".$k.">'si", $category["body"], $match);
                if($match) {

                    $input_type = '';
                    if(strpos($match[0], 'type="text"')){
                        $input_type = "input_text";
                    }
                    if(strpos($match[0], 'type="radio"')){
                        $input_type = "input_radio";
                    }
                    if(strpos($match[0], '<textarea')){
                        $input_type = "textarea";
                    }
                    if(strpos($match[0], '<select')){
                        $input_type = "select";
                    }
                    
                    switch ($input_type) {
                        case 'input_text':
                                $value_old = 'value=""';

                                if(preg_match("/salary/", $k)){
                                    if(isset($flag_not_see) || in_array($this->session->admin['role'], array(3,4)) ){
                                        $value_new = 'value="" readonly="readonly"';
                                    }else{
                                        $value_new = 'value="'.$this->mc_decrypt($v, ENCRYPTION_KEY).'"';
                                    }
                                }else{
                                    $value_new = 'value="'.$v.'"';
                                }
                                $dom_old = $match[0];
                                
                                // loại bỏ các giá trị trong value nếu mà bị set sẵn
                                preg_match('/value="(.*?)"/mis', $dom_old, $exits_value);
                                if(isset($exits_value[1])){
                                    $value_old = 'value="'.$exits_value[1].'"';
                                }
                                //
                                $dom_new = str_replace($value_old, $value_new, $dom_old);

                                $category["body"] = preg_replace('#<'.$k.'>.*</'.$k.'>#m', $dom_new, $category["body"]);
                            break;
                        case 'select':
                            $match[0] = str_replace("selected", '', $match[0]);

                            $value_old = 'value="'.$v.'"';
                            $value_new = 'value="'.$v.'" selected';
                            $dom_old = $match[0];
                            $dom_new = str_replace($value_old, $value_new, $dom_old);
                            $category["body"] = preg_replace('#<'.$k.'>.*</'.$k.'>#s', $dom_new, $category["body"]);//utf8
                            break;
                        case 'textarea':
                            $dom_old = $match[0];
                            $value_new = $v.'</textarea>';
                            $value_old = strip_tags($dom_old).'</textarea>';
                            $dom_new = str_replace($value_old, $value_new, $dom_old);
                            $category["body"] = preg_replace('#<'.$k.'>.*</'.$k.'>#m', $dom_new, $category["body"]);
                            break;
                        default:
                            # code...
                            break;
                    }
                
                }
            }

        return $category["body"];
    }

    

    public function checkExistContract(){
        $category_id  = $this->post("category_id");
        $staff_id = $this->post("staff_id");
        $contract_id  = $this->post('id');

        $contract_exist = $this->db_contract->getByLaborer($staff_id, $category_id);

        if(!empty($contract_exist)){
            if(!empty($contract_id))
            {//edit
                if($contract_id == $contract_exist['id']){//self
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {//create new
                return 0;
            }
        }
        else
        {//not exist
            return 1;
        }

    }


}// END: Class