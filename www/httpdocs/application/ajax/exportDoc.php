<?php

class DM_Action_Ajax_exportDoc extends DM_Action_ajax 
{
    var $_isForm        = false;
    var $_freeze        = false;
    var $_method        = "post";

    public function init()
    {
        parent::init();
    }// END: init()

    public function validate()
    {
    }// END: validate()

    public function done($data)
    {
        $contract_name = $_POST['contract_name'];
        header("Content-Type: application/vnd.msword");
        header("Expires: 0");//no-cache
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");//no-cache
        header("content-disposition: attachment;filename=".$contract_name.".doc");
        echo "<html>".$_POST['html']."</body></html>";

    exit();
    }// END: done()


}// END: Class