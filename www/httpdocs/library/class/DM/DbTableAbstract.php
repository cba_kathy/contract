<?php

/**
 * default.php
 *
 * @category    CB_Framework
 * @package
 * @subpackage  admin
 * @copyright   CYBRiDGE
 * @license     CYBRiDGE License 3.1
 *
 */

class DM_DbTableAbstract extends CB_DbTableAbstract
{

	const FETCH_OBJ = 5;
	protected $limit = 10;
	/**
	 * check a record exists in db
	 * @param  [string] $key
	 * @return [array] $data of database info
	 */

	public function info($key = null)
	{
		$info = $this->zendTable->info();

		switch ($key) {
			case 'priKey':
				return $info['primary'][1];
				break;
			case 'cols':
				return $info['cols'];
				break;
			case 'name':
				return $info['name'];
				break;
			case 'alias':
				return $this->_alias ? $this->_alias : $info['name'];
				break;
			default:
				break;
		}
		return "";
	}

	/**
	* [find description]
	* @param  string  $type          [type of query. All for get all data, pager for fetchPager, row for select 1 item, list for select on dropdown box or radio, checkbox]
	* @param  array   $opt           [array of additional options for where, join, have, group, order by]
	* @param  boolean $display_query [if TRUE this will print current query for debug]
	* @return [array|bool]                 [return array of data from DB or false if no data found]
	*/
	public function find($type = 'all', $opt = array(), $display_query = false)
	{
		$opt = array_merge(
			array(
				'alias' => $this->info('alias'),
				'fields' => '*', 'joins' => array(), 'conditions' => array(),
				'group' => null, 'have' => null, 'order' => null, 'limit' => null,
				'toObject' => false,
			),
			(array)$opt
		);

		$select = $this->select($opt['fields'], $opt['alias']);
		// joins: str_alias => array(str_name, str_cond, arr_cols, str_type)
		foreach ($opt['joins'] as $alias => $join) {
			$join_cols = is_array($join[2]) ? $join[2] : array();
			switch ($join[3]) {
				case 'left':
					$select->joinLeft(array($alias => $join[0]), $join[1], $join_cols);
					break;
				case 'right':
					$select->joinRight(array($alias => $join[0]), $join[1], $join_cols);
					break;
				default:
					$select->joinInner(array($alias => $join[0]), $join[1], $join_cols);
					break;
			}
		}
		// conditions
		if (is_array($opt['conditions'])) {
			foreach ($opt['conditions'] as $cond => $val) {
				$select->where($cond, $val);
			}
		}
		if ($opt['group']) {
			$select->group($opt['group']);
		}
		if ($opt['having']) {
			$select->having($opt['having']);
		}
		if ($opt['order']) {
			$select->order($opt['order']);
		}
		if ($opt['limit']) {
			$select->limit($opt['limit']);
		} else {
			if ($type == 'pager') {
				$select->limit($this->limit);
			}
		}
		if ($opt['toObject']) {
			$this -> zendDb -> setFetchMode(self::FETCH_OBJ);
		}

		if ($display_query) {
			echo $select;
		}

		switch ($type) {
			case 'row':
			case 'first':
				return $this -> zendDb -> fetchRow($select);
				break;

			case 'list':
				$list = $this -> zendDb -> fetchPairs($select);
				return $opt['toObject'] ? (object)$list : $list;
				break;

			case 'all':
				return $this-> zendDb -> fetchAll($select);
				break;

			case 'count':
				$select = preg_replace('/ORDER BY(.*)$/s', '', $select);
				if (preg_match('/(HAVING|GROUP BY)/', $select)) {
					$select = "SELECT COUNT(*) FROM (" . $select . ") as __framework_tmp;";
				} else {
					$select = preg_replace('/SELECT(.*)FROM/s', 'SELECT COUNT(*) FROM', $select);
				}
				return $this -> zendDb -> fetchOne($select);
				break;

			case 'pager':
				return $this->fetchPager($select);
				break;
		}
		return false;
	}
	/**
	 * check a record exists in db
	 * @param  [object] $select, [array] $options
	 * @return [array] $data
	 */
	public function fetchPager($select, $options = null)
	{
		$page = 1;
		if (is_numeric($_GET['p'])) {
			$page = $_GET['p'];
		}

		// count total result
		$select_count = clone $select;
		$select_count->reset(Zend_Db_Select::LIMIT_COUNT)
			->reset(Zend_Db_Select::LIMIT_OFFSET)
			->reset(Zend_Db_Select::ORDER)
		;
		if (preg_match('/(HAVING|GROUP BY)/', $select_count)) {
			$select_count = "SELECT COUNT(*) FROM (" . $select_count . ") as __framework_tmp;";
		} else {
			$select_count->reset(Zend_Db_Select::COLUMNS)->columns("COUNT(*)");
		}
		$total = $this-> zendDb -> fetchOne($select_count);

		$limit = $select->getPart(Zend_Db_Select::LIMIT_COUNT);
		$offset = $limit * ($page - 1);
		$uri = $_SERVER["REQUEST_URI"];
		if (preg_match('/p=[\d]+/', $uri)) {
			$uri  = preg_replace('/p=[\d]+/', 'p=[param]', $uri);
		} else {
			$uri .= preg_match('/\?/', $uri) ? "&p=[param]" : "?p=[param]";
		}
		$pager = new DM_Pager($page, $limit, $total, $uri);

		$select->limit($limit, $offset);
		$data = array(
			'rows' => $this -> zendDb -> fetchAll($select),
			'pager' => $pager->createLinks(),
		);

		return $data;
	}

	public function getByField($field_name, $id)
	{
		if (!in_array($field_name, $this->info('cols')) || empty($id)) {
			return false;
		}
		$select = $this->select($field_name);
		if (is_array($id)) {
			foreach ($id as $cond => $value) {
				$select->where($cond, $value);
			}
		} else {
			$pk = $this->info('priKey');
			$select->where("{$pk} = ?", $id);
		}
		return $this->fetchOne($select);
	}

	/**
	 * check a record exists in db
	 * @param  [int|array] $id         [if int->check primary_key, array->check condition]
	 * @return [bool]             [true if exists]
	 */
	public function exists($id)
	{

		$select = $this->select("COUNT(*)");

		if (is_numeric($id)) {
			$pk = $this->info('priKey');
			$select->where("{$pk} = ?", $id);
		} elseif (is_array($id)) {
			foreach ($id as $cond => $value) {
				$select->where($cond, $value);
			}
		} else {
			return false;
		}

		if ($this->fetchOne($select) > 0) {
			return true;
		}
		return false;
	}
	/**
	 * check a record exists in db
	 * @param  void
	 * @return [int]             [next ID from table]
	 */
	public function getNewId()
	{
		$select = $this->select("MAX(".$this->info('priKey').")");
		return $this->fetchOne($select) + 1;
	}


	  public function autoDelete($where) {
        $status = false;
        try {
            $this->beginTransaction();
            if (is_numeric($where)) {
                $status = $this->deleteById($where);
            } else {
                $status = $this->delete($where);
            }
            $this->commit();
        } catch(Exception $e) {
            $this->rollBack();
        }
        return $status;
    }

}
