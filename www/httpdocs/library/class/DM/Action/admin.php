<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class DM_Action_admin extends DM_Action 
{
	public function commonInit() {
		if($this->getPermission() == false){
			$this->session->error403 = 'Sorry Acess Dinied!';
			$this->httpRedirect("/admin/403");
			exit();
		}
		if (!$this->post('submit')) {
			if (is_array($this->session->files)) {
				unset($this->session->files);
			}
			unset($this->session->image_datetime);
		}
	}

	public function auth() 
	{
		if (empty($this->session->admin)) {
			$this->session->redirect_uri = $_SERVER['REQUEST_URI'];
			$this->httpRedirect("/admin/login");
			exit;
		}
	}

	public function getSortParams($data = array(), $sort_key, $sort_conditions = array(), $sort_type = "DESC") 
	{
		$str_query = "";
		if ($this->get('sort_type')) {
			$sort_type = $this->get('sort_type');
		}
		$data['sort_type'] = ($sort_type == "DESC") ? "ASC" : "DESC";
		if ($this->get('p')) {
			$data['p'] = $this->get('p');
		}
		if (!empty($data)) {
			$str_query = http_build_query($data);
		}
		
		$sort_by = $this->get("sort_by") ? $this->get("sort_by") : $sort_key;
		if (array_key_exists($sort_key, $sort_conditions)) {
			$sort_by = $sort_conditions[$sort_key];
		}

		$sorts = array(
			"order" => $sort_by." ".$sort_type,
			"query" => $str_query,
		);
		return $sorts;

	}

	/*(mức lương: basic+trách nhiệm)
	A: full quyền
	B: full quyền nhưng ko đc xem, chỉnh sửa lương của Leader/manager
	C: full quyền nhưng ko đc xem, chỉnh sửa lương của tất cả nhân viên
	D: chỉ xem (xem các thông tin cơ bản trong hợp đồng trừ tiền lương)
	*/
	public function getPermission($action = null, $id = null){
		if(!$action){
			$action = str_replace('/admin/', '', $_SERVER['REDIRECT_URL']);
		}
		if($this->post("_type")){
			$action =  $this->post("_type") == 'delete' ? 'delete': '';
		}
		if(!$id){
			$id = $this->get('id');
		}
		if(!in_array($action, array('login', 'logout'))){
			$permission =  $this->session->check_action;
			$role = $this->session->admin['role'];
			if( isset($permission[$action]) && $role ){
				switch ($role) {
					case 1:
						return true;
						break;
					default:
						return $permission[$action] == 1 ? true : false;
					break;
				}
			}else{
				return true;
			}
			
		}
		return 1;
	}


	// Code mã hóa
	function mc_encrypt($encrypt, $key) {
	    $encrypt = serialize($encrypt);
	    $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
	    $key = pack('H*', $key);
	    $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
	    $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt . $mac, MCRYPT_MODE_CBC, $iv);
	    $encoded = base64_encode($passcrypt) . '|' . base64_encode($iv);
	    return $encoded;
	}

	// Code giải mã
	function mc_decrypt($decrypt, $key) {
	    $decrypt = explode('|', $decrypt);
	    $decoded = base64_decode($decrypt[0]);
	    $iv = base64_decode($decrypt[1]);
	    if (strlen($iv) !== mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)) {
	        return false;
	    }
	    $key = pack('H*', $key);
	    $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
	    $mac = substr($decrypted, -64);
	    $decrypted = substr($decrypted, 0, -64);
	    $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
	    if ($calcmac !== $mac) {
	        return false;
	    }
	    $decrypted = unserialize($decrypted);
	    return $decrypted;
	}
}
