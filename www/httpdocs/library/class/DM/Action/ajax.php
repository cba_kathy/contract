<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class DM_Action_ajax extends DM_Action
{

	public function view() {
		exit;
	}

	public function checkOne($id, $sess_name = "choice_list", $remove_old_key = true)
	{
		$choice_s = array();
		if (!empty($this->session->choices[$sess_name])) {
			$choice_s = explode(',', $this->session->choices[$sess_name]);
		}
		$flag = true;

		if (!$choice_s) {
			$choice_s[] = $id;
		} else {
			if (in_array($id, $choice_s)) {
				if ($remove_old_key) {
					$old_key = array_search($id, $choice_s);
					unset($choice_s[$old_key]);
					$flag = false;
				}
			} else {
				$choice_s[] = $id;
			}
		}

		$this->session->choices[$sess_name] = empty($choice_s) ? '' : implode(',', $choice_s);
		if (!$flag) {
			return false;
		}
		return true;

	}

	public function checkAll($data, $sess_name = "choice_list", $key_field)
	{
		if (!$data) {
			return false;
		}
		$tmp_s = array();
		foreach ($data as $value) {
			$tmp_s[] = $value[$key_field];
		}
		$tmp_s = implode(',', $tmp_s);
		$this->session->choices[$sess_name] = $tmp_s;
		return $tmp_s;
	}

	public function checkSessionData($sess_name) {
		if (empty($this->session->choices[$sess_name])) {
			return false;
		}
		return true;
	}

}
