<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class DM_Pager
{

	public $_page;
	public $_size;
	public $_totalRecords;
	public $_link;
	public $_pageDisplay = 3;


	public function __construct($page = null, $size = null, $totalRecords = null, $link = null)
	{
		$this->_page = $page;
		$this->_size = $size;
		$this->_link = $link;
		$this->_totalRecords = $totalRecords;
	}

	public function createLinks()
	{

		if (preg_match('/^\/admin/', $_SERVER["REQUEST_URI"])) {
			return $this->pagerAdmin();
		} else {
			return $this->pagerUser();
		}

	}

	private function pagerUser()
	{

		$totalPages = ($this->_totalRecords == 0) ? 0 : ceil($this->_totalRecords / $this->_size);

		$html = '';
		$dot = '<li>...</li>';
		$first = $prev = $next = $last = $link = $prev_dot = $next_dot = '';

		if ($totalPages > 1) {
			if ($totalPages <= ($this->_pageDisplay + 1)) { //show all pages
				for ($i = 1; $i <= $totalPages; $i++) {
					$cls = ($i == $this->_page) ? 'active' : '';
					$href = ($i == $this->_page) ? '#' : str_replace('[param]', $i, $this->_link);
					$link .= '<li><a class="piecss3 '.$cls.'" href="'.$href.'">'.$i.'</a></li>';
				}
			} else { //show limit pages
				if ($this->_page > $this->_pageDisplay) {
					$first = '<li><a class="piecss3" href="'.str_replace('[param]', 1, $this->_link).'">1</a></li>';
				}
				if (($this->_page + ceil($this->_pageDisplay/2)) <= $totalPages) {
					$last = '<li><a class="piecss3" href="'.str_replace('[param]', $totalPages, $this->_link).'">'.$totalPages.'</a></li>';
				}
				$prev_dot = ($first != '') ? $dot : '';
				$next_dot = ($last != '') ? $dot : '';
				if ($first == '') {
					$from = 1;
				} elseif ($last == '') {
					$from = $totalPages - $this->_pageDisplay;
				} else {
					$from = $this->_page - floor($this->_pageDisplay/2);
				}
				$to = ($first == '' || $last == '') ? $from + $this->_pageDisplay + 1 : $from + $this->_pageDisplay;
				for ($from; $from < $to; $from++) {
					$cls = ($from == $this->_page) ? 'active' : '';
					$href = ($from == $this->_page) ? '#' : str_replace('[param]', $from, $this->_link);
					$link .= '<li><a class="piecss3 '.$cls.'" href="'.$href.'">'.$from.'</a></li>';
				}
			}
			$prev = ($this->_page > 1) ? '<li class="prev"><a class="piecss3" href="'.str_replace('[param]', $this->_page - 1, $this->_link).'">前の'.$this->_size.'件</a></li>' : '';
			$next = ($this->_page < $totalPages) ? '<li class="next"><a class="piecss3" href="'.str_replace('[param]', $this->_page + 1, $this->_link).'">次の'.$this->_size.'件</a></li>' : '';
		}

		$html .= $prev.$first.$prev_dot.$link.$next_dot.$last.$next;

		$start = $this->_size * $this->_page + 1 - $this->_size;
		$end = $this->_size * $this->_page;
		if($end > $this->_totalRecords){
			$end = $this->_totalRecords;
		}
		$header = '<p class="pagerText"><span>'.$this->_totalRecords.'</span>件中 <em>'.$start.'～'.$end.'</em>件目の講師を表示中</p>';
		$data = array(
			"header" => $header,
			"html"  => '<ul class="pagerList">' . $html . '</ul>',
			"current"      => $this->_page,
			"total"        => $totalPages,
			"start"        => $start,
			"end"          => $end,
			"count" 	   => $this->_totalRecords,
		);
		return $data;
	}

	private function pagerAdmin()
	{
		$totalPages = ($this->_totalRecords == 0) ? 0 : ceil($this->_totalRecords / $this->_size);
		$this->_pageDisplay = 7;

		$html = '';
		$dot = '<li>...</li>';
		$first = $prev = $next = $last = $link = $prev_dot = $next_dot = '';

		if ($totalPages > 1) {
			if ($totalPages <= ($this->_pageDisplay + 1)) { //show all pages
				for ($i = 1; $i <= $totalPages; $i++) {
					if ($i == $this->_page) {
						$link .= '<li class="cur"><strong>'.$i.'</strong></li>';
					} else {
						$link .= '<li><a href="'.str_replace('[param]', $i, $this->_link).'">'.$i.'</a></li>';
					}
				}
			} else { //show limit pages
				if ($this->_page > $this->_pageDisplay) {
					$first = '<li><a href="'.str_replace('[param]', 1, $this->_link).'">1</a></li>';
				}
				if (($this->_page + ceil($this->_pageDisplay/2)) <= $totalPages) {
					$last = '<li><a href="'.str_replace('[param]', $totalPages, $this->_link).'">'.$totalPages.'</a></li>';
				}
				$prev_dot = ($first != '') ? $dot : '';
				$next_dot = ($last != '') ? $dot : '';
				if ($first == '') {
					$from = 1;
				} elseif ($last == '') {
					$from = $totalPages - $this->_pageDisplay;
				} else {
					$from = $this->_page - floor($this->_pageDisplay/2);
				}
				$to = ($first == '' || $last == '') ? $from + $this->_pageDisplay + 1 : $from + $this->_pageDisplay;
				for ($from; $from < $to; $from++) {
					if ($from == $this->_page) {
						$link .= '<li class="cur"><strong>'.$from.'</strong></li>';
					} else {
						$link .= '<li><a href="'.str_replace('[param]', $from, $this->_link).'">'.$from.'</a></li>';
					}
				}
			}
			$prev = ($this->_page > 1) ? '<li class="prev"><a href="'.str_replace('[param]', $this->_page - 1, $this->_link).'">戻る</a></li>' : '';
			$next = ($this->_page < $totalPages) ? '<li class="next"><a href="'.str_replace('[param]', $this->_page + 1, $this->_link).'">次へ</a></li>' : '';
		}

		$html .= $prev.$first.$prev_dot.$link.$next_dot.$last.$next;

		$start = $this->_size * $this->_page + 1 - $this->_size;
		$end = $this->_size * $this->_page;
		if($end > $this->_totalRecords){
			$end = $this->_totalRecords;
		}
		$header =
			'<dl class="searchUtil">'
				.'<dt><strong>'.$this->_totalRecords.'</strong>Show</dt>'
				.'<dd> '.$start.' in - '.$end.' items</dd>'
			.'</dl>';
		$data = array(
			"header" => $header,
			"html"  => '<ol class="pager">' . $html . '</ol>',
			"current"      => $this->_page,
			"total"        => $totalPages,
			"start"        => $start,
			"end"          => $end,
			"count" 	   => $this->_totalRecords,
		);
		return $data;
	}

}
