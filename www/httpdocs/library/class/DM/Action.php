<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class DM_Action extends CB_Action
{

	public function hashString($value)
	{
		return hash("sha512", $value.HASH_STRING);
	}
	public function hashPassword($string)
	{
		$options = [
			'cost' => 13,
			'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
		];
		return password_hash($string, PASSWORD_BCRYPT, $options);
	}

	public function verifyPassword($password, $hash)
	{
		return password_verify($password, $hash);
	}
	public function getRedirectUri($uri = '/admin')
	{
		if ($this->session->redirect_uri) {
			$uri = $this->session->redirect_uri;
			unset($this->session->redirect_uri);
		}
		return $uri;
	}

	public function getFlag($key = null, $value = null)
	{
		
		$option_r = array(
		);
		if (!$key || !isset($option_r[$key])) {
			return array();
		}
		$option = $option_r[$key];
		if (array_key_exists($value, $option)) {
			return $option[$value];
		}

		return $option;
	}


	public function getHiraganaAlphabet()
	{
		return array(
			"あ" => "あ", "い" => "い", "う" => "う", "え" => "え", "お" => "お",
			"か" => "か", "き" => "き", "く" => "く", "け" => "け", "こ" => "こ",
			"さ" => "さ", "し" => "し", "す" => "す", "せ" => "せ", "そ" => "そ",
			"た" => "た", "ち" => "ち", "つ" => "つ", "て" => "て", "と" => "と",
			"な" => "な", "に" => "に", "ぬ" => "ぬ", "ね" => "ね", "の" => "の",
			"は" => "は", "ひ" => "ひ", "ふ" => "ふ", "へ" => "へ", "ほ" => "ほ",
			"ま" => "ま", "み" => "み", "む" => "む", "め" => "め", "も" => "も",
			"や" => "や", "ゆ" => "ゆ", "よ" => "よ",
			"ら" => "ら", "り" => "り", "る" => "る", "れ" => "れ", "ろ" => "ろ",
			"わ" => "わ", "を" => "を", "ん" => "ん",
		);
	}


	public function randomString($length = 32, $characters = "0123456789abcdefghijklmnopqrstuv0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	{
		if (!is_int($length) || $length < 0) {
			return false;
		}
		$characters_length = strlen($characters) - 1;
		$string = '';
		$string = substr( str_shuffle( $characters ), 0, $length);
		return $string;
	}

	public function getWhereConditions($col_s = array(), $cond_s = array(), $exception = array())
	{
		$exception = array_merge(array('submit'), $exception);
		$conditions = array();
		foreach ($col_s as $col => $value) {
			if ($value === null || $value === "" || in_array($col, $exception)) {
				continue;
			}
			if (preg_match("/(datetime_max|date_max)/", $col)) {
				$value = date('Y-m-d 23:59:59', strtotime($value));
			}
			if (array_key_exists($col, $cond_s)) {
				$cond_key = $cond_s[$col][0];
				$cond_operation = $cond_s[$col][1];
				if (strpos($cond_operation, 'LIKE ?') !== false) {
					$value = "%".$value."%";
				}

				if (is_array($cond_key)) {
					$cond_key = implode(" ".$cond_operation." OR ", $cond_key);
				}

				$conditions += array($cond_key . " " . $cond_operation => $value);
			} else {
				if (is_numeric($value)) {
					$conditions += array($col . " = ?" => $value);
				} else {
					$conditions += array($col . " LIKE ?" => "%".$value."%");
				}
			}
		}

		return $conditions;
	}

	public function deleteFile($path)
	{
		if (strpos($path, CB_PUBLIC) === false) {
			$path = CB_PUBLIC.$path;
		}
		if (file_exists($path)) {
			unlink($path);
		}
	}

	public function addRuleCheckWhiteSpaces($elements)
	{
		foreach ($elements as $key => $value) {
			if (in_array($value['type'], array('text', 'textarea')) ) {
				if (!isset($value['rule']['check_white_spaces'])) {
					$elements[$key]['rule']['check_white_spaces'] = array();
				}
			}
		}
		return $elements;
	}

	public function getListDatesFrom($str_time = 'now', $total = 6)
	{
		$list = array();
		$key = 1;
		for ($i=$total; $i >= 0; $i--) {
			$list[$key] = date('Y-m-d', strtotime($str_time.' -'.$i.' day'));
			$key++;
		}
		return $list;
	}

	public function downloadCSV($data, $header, $filename = null)
	{
		if (!$filename) {
			$filename = 'csv_'.date('YmdHi').'.csv';
		}
		foreach ($data as $item) {
			$tmp_s = array();
			foreach ($header as $key => $value) {
				$tmp_s[] = $item[$key];
			}
			$csv_s[] = $tmp_s;
		}

		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=".$filename);
		header("Pragma: no-cache");
		header("Expires: 0");

		$output = fopen("php://output","w");
		fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));
		fputcsv($output, array_values($header));
		foreach ($csv_s as $row) {
			fputcsv($output, $row);
		}
		fclose($output);
		exit;
	}

	public function display()
	{
		if (!$this -> _tpl -> template_exists($this -> _template)){
			header("HTTP/1.0 404 Not Found");
			$this -> setTemplate("/_common/404.html");
			$buf = $this -> _tpl -> fetch($this -> _template);
		}

		parent::display();
	}

	public function sendMail($from, $to, $subject, $body = array(), $options = array() )
	{
		$tpl_file = CB_PUBLIC.'/mail_templates/'.$body['tpl'].'.tpl';
		if (!file_exists($tpl_file)) {
			return false;
		}
		$tpl = file_get_contents($tpl_file);
		foreach ($body['data'] as $key => $value) {
			if (strpos($tpl, '{[$'.$key.']}') !== false) {
				$tpl = str_replace('{[$'.$key.']}', $value, $tpl);
			}
		}

		$mail = new CB_Zend_Mail("UTF-8");
		$mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
		switch ($options["type"]) {
			case 'html':
				$mail->setBodyHtml($tpl);
				break;
			case 'attach':
				$mail->setBodyText($tpl);
				if($body["file"]["file_path"]){
					$mail->setType(CB_Zend_Mime::MULTIPART_RELATED);
					$file = $mail->createAttachment(file_get_contents($body["file"]["file_path"]) );
					$file ->type        = $body["file"]["file_type"];
					$file ->disposition = CB_Zend_Mime::DISPOSITION_INLINE;
					$file ->encoding    = CB_Zend_Mime::ENCODING_BASE64;
					$file ->filename    = $body["file"]["file_name"];
				}
				break;
			default:
				$mail->setBodyText($tpl);
				break;
		}

		$mail->setFrom($from)
			 ->addTo($to)
			 ->setSubject($subject);

		if ($mail->send()) {
			return true;
		}
		return false;
	}

	public function convertData(&$data, $cols = array(), $type, $separator = null, $by_keys = true)
	{
		foreach ($cols as $col) {
			$value = $data[$col];
			if ($value == null || $value == '') {
				$data[$col] = '';
				continue;
			}
			switch ($type) {
				case 'string_to_array':
					if (!$separator) {
						$separator = ",";
					}
					$str_s = explode($separator, $value);
					if ($by_keys) {
						if (!empty($str_s)) {
							$str_s = array_fill_keys($str_s, 1);
						}
					}
					$data[$col] = $str_s;
					break;
				case 'array_to_string':
					if (!$separator) {
						$separator = ",";
					}
					if (is_array($value) && !$this->isEmptyArray($value)) {
						if ($by_keys) {
							$value = array_keys($value);
						}
						$data[$col] = implode($separator, $value);
					} else {
						$data[$col] = "";
					}
					break;
				case 'json_to_array':
					$data[$col] = json_decode($value, true);
					break;
				case 'array_to_json':
					$json_tmp = "";
					if (is_array($value)) {
						if (!$this->isEmptyArray($value)) {
							$json_tmp = json_encode($value);
						}
					}
					$data[$col] = $json_tmp;
					break;
				default:
					$data[$col] = null;
					break;
			}

		}

	}

	public function isEmptyArray($arr)
	{
		$result = true;
		if (is_array($arr) && count($arr) > 0) {
			foreach ($arr as $value) {
				$result = $result && $this->isEmptyArray($value);
			}
		} else {
			$result = ($arr == null && $arr == '') ? true : false;
		}
		return $result;
	}

	public function getExtension($name)
	{
		$ext = strtolower(end(explode(".", $name)));
		return $ext;
	}


	public function get_roles( $id = false ){
		$roles = [
			1 => "(A) Full control",
			2 => "(B) Not see salary of manager/leader",
			3 => "(C) Not see salary of everybody",
			4 => "(D) Only view",
		];
		if($id){
			return $roles[$id];
		}
		return $roles;
	}


	public function get_positions( $id = false ){
		$positons = [
			1 => "Staff",
			2 => "Leader",
			3 => "Manager",
		];
		if($id){
			return $positons[$id];
		}
		return $positons;
	}


    public function setPermission(){//ghi lại session quyền lúc đăng nhập => set quyền ngay lúc đó
        $role = $this->session->admin['role'];
        
        switch ($role) {
        	case 1:
        		$check = array(
					'category_edit' =>1,
					'category_list' =>1,
					'category_view' =>1,
					'contract_list' =>1,
					'contract_edit' =>1,
					'contract_view' =>1,
					'index'         =>1,
					'staff_list'    =>1,
					'staff_edit'    =>1,
					'delete'        =>1,
					'admin_edit'    =>1,
					'admin_list'    =>1,
					);
        		break;
        	case 2:
        		$check = array(
					'category_edit' =>1,
					'category_list' =>1,
					'category_view' =>1,
					'contract_list' =>1,
					'contract_edit' =>1,
					'contract_view' =>1,
					'index'         =>1,
					'staff_list'    =>1,
					'staff_edit'    =>1,
					'delete'        =>0,
					'admin_edit'    =>0,
					'admin_list'    =>0,
        			);
        		break;
        	case 3:
        		$check = array(
					'category_edit' =>0,
					'category_list' =>1,
					'category_view' =>1,
					'contract_list' =>1,
					'contract_edit' =>1,
					'contract_view' =>1,
					'index'         =>1,
					'staff_list'    =>1,
					'staff_edit'    =>0,
					'delete'        =>0,
					'admin_edit'    =>0,
					'admin_list'    =>0,
        			);
        		break;
        	case 4:
        		$check = array(
					'category_edit' =>0,
					'category_list' =>1,
					'category_view' =>1,
					'contract_list' =>1,
					'contract_edit' =>0,
					'contract_view' =>1,
					'index'         =>1,
					'staff_list'    =>1,
					'staff_edit'    =>0,
					'delete'        =>0,
					'admin_edit'    =>0,
					'admin_list'    =>0,
        			);
        		break;
        	default:
        		$check = array(
					'category_edit' =>0,
					'category_list' =>0,
					'category_view' =>0,
					'contract_list' =>0,
					'contract_edit' =>0,
					'contract_view' =>0,
					'index'         =>0,
					'staff_list'    =>0,
					'staff_edit'    =>0,
					'delete'        =>0,
					'admin_edit'    =>0,
					'admin_list'    =>0,
        			);
        		break;
        }
        $this->session->check_action = $check;
    }

}//END class
