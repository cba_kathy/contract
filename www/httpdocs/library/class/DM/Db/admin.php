<?php
/**
 * db_admin.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class DM_DB_admin extends DM_DbTableAbstract
{
	protected $_alias = 'admin';

		public function getBy( $fields = array(), $conditions = array() ){
	        if (!$conditions) {
	            return false;
	        }
	        if(empty($select)){
	            $select = $this->select("*");
	        }else{
	            $select = $this->select($fields);
	        }
	        
	        foreach ($conditions as $k => $v) {
	            $select->where( "$k=?", $v );
	        }
	        $result_r = $this->fetchRow($select);
	        return $result_r;
	    }

}
