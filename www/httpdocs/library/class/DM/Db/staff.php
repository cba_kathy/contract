<?php
/**
 * db_staff.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class DM_DB_staff extends DM_DbTableAbstract
{
	protected $_alias = 'staff';

	public function getBy($conditions = array()){
        if (!$conditions) {
            return false;
        }
        $select = $this->select("*");
        foreach ($conditions as $k => $v) {
            $select->where("$k=?",$v);
        }
        $result_r = $this->fetchRow($select);
        return $result_r;
    }


    public function getList(){
        $c_select = $this->select();
        $staffs = $this->fetchAll($c_select);
        foreach ($staffs as $k => $v) {
            $list_staff[$v['id']] = $v['fullname'].' ('.$v['nickname'].')';
        }
        return $list_staff;
    }

   
}
