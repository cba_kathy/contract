<?php
/**
 * db_category.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class DM_DB_category extends DM_DbTableAbstract
{
	protected $_alias = 'category';

	public function getList(){
		$c_select = $this->select();
		$categories = $this->fetchAll($c_select);
		foreach ($categories as $k => $v) {
			$list_category[$v['id']] = $v['name'];
		}
		return $list_category;
	}


	public function getListBody(){
		$c_select = $this->select();
		$categories = $this->fetchAll($c_select);
		foreach ($categories as $k => $v) {
			$list_category[$v['id']] = $v['body'];
		}
		return $list_category;
	}

}
