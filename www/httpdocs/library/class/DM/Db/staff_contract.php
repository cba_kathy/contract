<?php
/**
 * db_staff_contract.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class DM_DB_staff_contract extends DM_DbTableAbstract
{

	protected $_alias = 'staff_contract';

}
