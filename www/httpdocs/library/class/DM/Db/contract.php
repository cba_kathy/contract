<?php
/**
 * db_contract.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class DM_DB_contract extends DM_DbTableAbstract
{

	protected $_alias = 'contract';

    public function getList($limit = false, $from_id = null){
        $c_select = $this->select(array('contract.*', 'category.name as category_name', 'staff.image as image', 'staff.fullname as laborer', 'admin.fullname as create', 'admin.image as create_image',))
                    ->joinLeft("category", "category.id = contract.category_id")
                    ->joinLeft("staff", "staff.id = contract.staff_id")
                    ->joinLeft("admin", "admin.id = contract.admin_id")
                    ->order(array('id desc'))
                    ->limit(15);

        if($from_id) {
            $c_select->where("contract.id < ?", $from_id);
        }
       
        $c_select = str_replace(', `staff`.*', '',  $c_select);
        $c_select = str_replace(', `admin`.*', '',  $c_select);
        $c_select = str_replace(', `category`.*', '',  $c_select);

        $result = $this->fetchAll($c_select);
        return $result;
    }


    public function getByLaborer($staff_laborer = null, $category_id = null){
        $select = $this->select(array('body', 'id', 'staff_id'))
                        ->where('category_id=?', $category_id)
                        ->where('staff_id=?', $staff_laborer);

        $contract_exist = $this->fetchRow($select);
        return $contract_exist;
    }

    public function getExistContract($staff_laborer = null){
        $select = $this->select(array('body', 'id', 'staff_id'))
                        ->where('category_id IN (?)', array(2,3,4,7,5,6))
                        ->where('staff_id=?', $staff_laborer);

        $contract_exist = $this->fetchRow($select);
        return $contract_exist;
    }

}
