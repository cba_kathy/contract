<?php
/*=====================================
    PATHの定義
=====================================*/
define('CB_FRAMEWORK_PRE', 'DM');
define('CB_FW_ROOT'      , "/usr/share/php/CBFramework_3.1");
define('HTDOCS_ROOT'     , realpath(dirname(__FILE__).'/../../'));
define('CB_BASE'         , HTDOCS_ROOT);
define('CB_PUBLIC'       , CB_BASE    . '/public');
define('CB_SMARTY_CONFIG', CB_PUBLIC .'_configs/default.global.inc');
define('CB_TMP'          , CB_BASE . "/../tmp");
define('USE_CSRF'    	 , true);
define('CB_DEBUGGING'    , 0);

/*=====================================
    データベース定義
=====================================*/
define('CB_DB_TYPE' , 'pdo_mysql');
define('CB_DB_PORT' , '3306');
define('CB_DB_HOST' , 'localhost');
define('CB_DB_USER' , 'root');
define('CB_DB_PASS' , 'cctmcctm');
define('CB_DB_NAME' , 'contract');
/*=====================================
    その他の定義
=====================================*/
define('ENCRYPTION_KEY', '5a9adddba4556e4784ec17246552f2033c3f6df767516ef92a55efed1408772b');