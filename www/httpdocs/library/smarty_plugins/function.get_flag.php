<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

function smarty_function_get_flag($params){
	$key = $params['key'];
	$value = $params['value'];
	return DM_Action::getFlag($key, $value);
}
