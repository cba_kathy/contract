<?php

/**
 * default.php
 *
 * @category    CB_Framework
 * @package
 * @subpackage  admin
 * @copyright   CYBRiDGE
 * @license     CYBRiDGE License 3.1
 *
 */

function smarty_function_form_error ($params, &$smarty) {

    if ($smarty->sf->_freezed) {
        return "";
    }

    $errors = $smarty->sf->getElementErrors();
    if (empty($errors)) {
        return "";
    }
    $html = "";

    switch ($params['site']) {
        case 'pc':
            # code...
            break;

        default:
            $name = $params['name'];
            if ($name) {
                if (array_key_exists($name, $errors)) {
                    return $html .= "<p class=\"notice\">".$errors[$name]."</p>";
                }
            } else {
                foreach ($errors as $name => $error) {
                    $html .= "<li data-name=\"{$name}\">".$error."</li>";
                }
                $html = "<ul class=\"error\">".$html."</ul>";
            }
            break;
    }

    return $html;

}
