<?php

/**
 * default.php
 *
 * @category    CB_Framework
 * @package
 * @subpackage  admin
 * @copyright   CYBRiDGE
 * @license     CYBRiDGE License 3.1
 *
 */

function smarty_function_form_file($params, &$smarty)
{
    if (empty($params['name'])){
        $smarty -> _trigger_fatal_error("[plugin] parameter 'name' cannot be empty");
        return;
    }

    $name = $params["name"];
    unset($params['name']);
    $upload_type = $params['upload_type'] ? $params['upload_type'] : UPLOAD_TYPE_FILE;
    $upload_text = $params['text'] ? $params['text'] : "Drop file here or click to upload.";
    

    $smarty -> sf -> updateElementAttr($name, $params);
    $Element = &$smarty -> sf -> getElement($name);
    $Element->_attributes['type'] = "hidden";
    $value = $Element->getValue();

    $rules = $smarty -> sf -> _form -> _rules;
    foreach ((array)$rules[$name] as $items) {
        if ($items['type'] == UPLOAD_RULE && !empty($items['format'])) {
            foreach ($items['format'] as $opt => $val) {
                $Element->setAttribute("data-".$opt, $val);
            }
        }
    }

    $html = $Element->_flagFrozen ? "<div>" : "<div class=\"dropzone\" data-title=\"{$upload_type}\">";
    if (!$Element->_flagFrozen) {
        // $html .= 
        //  "<button type=\"button\" class=\"btn btn-primary btnUpload\">
        //      <span>{$upload_text}</span>
        //  </button>";
        $html .= "<div class=\"dz-message btnUpload\">{$upload_text}</div>";
    }
    $html .= "<div class=\"files\">";
    
    if ($value) {
        switch ($upload_type) {
            case UPLOAD_TYPE_CROP:
            case UPLOAD_TYPE_IMAGE:
                $html .= "<img src=\"{$value}\" {$options} alt=\"{$name}\" height=\"".UPLOAD_THUMBNAIL_SIZE."\">";
                break;
            case UPLOAD_TYPE_VIDEO:
                $html .= "<video width=\"320\" height=\"240\" controls src=\"{$value}\">Your browser does not support the video tag.</video>";
                break;
            case UPLOAD_TYPE_FILE:
                $html .= "<span class=\"boxShadow\">{$value}</span>";
        }
        $Element->_flagFrozen || $html .= "<button class=\"close btnDelete\" type=\"button\">×</button>";
    }
    
    $html .= "</div><input {$Element->_getAttrString($Element->_attributes)}></div>";

    return $html;
}
