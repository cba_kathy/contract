<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

function smarty_modifier_format($value, $type){	
	switch ($type) {
		case 'number':
			if (empty($value)) {
				return "0";
			} else {
				return is_numeric($value) ? number_format($value) : $value;
			}
			break;
		
		default:
			return $value;
			break;
	}
}
