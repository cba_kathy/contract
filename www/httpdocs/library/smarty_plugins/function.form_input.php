<?php

/**
 * default.php
 *
 * @category    CB_Framework
 * @package
 * @subpackage  admin
 * @copyright   CYBRiDGE
 * @license     CYBRiDGE License 3.1
 *
 */

function smarty_function_form_input($params, &$smarty)
{
    if (empty($params['name'])){
        $smarty -> _trigger_fatal_error("[plugin] parameter 'name' cannot be empty");
        return;
    }

    $name = $params["name"];

    unset($params['name']);

    if (!$params['id']){
        $params['id'] = $name . "_id";
    }

    $smarty -> sf -> updateElementAttr($name, $params);
    $Element = &$smarty -> sf -> getElement($name);

    if(PEAR::isError($Element)){
    	// type=dateの時に、項目単体（年・月・日）で取得できるように修正

    	$Elements = &$smarty -> sf -> getElements();
    	foreach ($Elements as &$_element) {
    		if (strpos($name, $_element->_name) === 0) {
    			$Element = &$_element;
    			break;
    		}
    	}

    	if(PEAR::isError($Element)){
    		$smarty->_trigger_fatal_error($Element->getMessage());
    		return;
    	}

    	switch ($Element -> getType()) {
    		case "group":
    		case "date":
    			$Element->_createElementsIfNotExist();	// 最初のアクセス時はgroup内のelementが存在しないため事前に作成

		    	foreach ($Element->_elements as &$_element) {
		    		if ($_element->_attributes["id"] == $name) {
		    			$params["name"] = $_element->_attributes["id"];
		    			$Element = &$_element;
		    			$Element->updateAttributes($params);
		    			break;
		    		}
		    	}

		    	if ($Element->_attributes["name"] != $name) {
		    		$smarty->_trigger_fatal_error("cannot found name '{$name}' in this group.");
		    		return;
		    	}
	    		break;
    	}
    }

    switch ($Element -> getType()) {
    	case "group":
    		unset($params["class"]);
    	case "date":
    		unset($params["id"]);
    		foreach($Element->_elements AS $key=>$val){
    			$Element->_elements[$key]->updateAttributes($params);
    		}
    		break;
        case "tel":
            $placeHolder = array("例） 03", "例） 6434", "例） 9192");
            for ($i=0; $i < 3; $i++) { 
                $Element->_elements[$i]->_attributes["placeHolder"] = $placeHolder[$i];
                $Element->_elements[$i]->_attributes["id"] = $name."_".$i;
            }
            break;
    }

    if (($params['prefix'] || $params['suffix']) && $Element -> getType() == 'group'){
        // if($params['prefix'] && $params['suffix']){
        $renderer = new HTML_QuickForm_Renderer_Default();
        $template = $params['prefix'] . ' {element}' . $params['suffix'];
        // $renderer->setElementTemplate($template,$name);
        $renderer -> setElementTemplate("{element}", $name);
        $renderer -> setGroupTemplate("{content}", $name);
        $renderer -> setGroupElementTemplate($template, $name);
        $Element -> accept($renderer);
        $html = $renderer -> toHtml();
    } elseif ($Element -> getType() === "file") {
        if ($Element->_flagFrozen) {
            $html = uploadFile($Element);
        } else {
            $html = $Element->toHtml();
            // d("<xmp>".$html."</xmp>");
        }
    } else {
        $html = $Element -> toHtml();
    }

    switch ($Element -> getType()) {
        case 'textarea':
            $html = str_replace("</textarea>","{$params["value"]}</textarea>",$html);
            break;
        
        case 'checkbox':
            $Element->setChecked(FALSE);
            break;

        case 'group':
            $html = preg_replace('/(prefix="&lt;li&gt;"|suffix="&lt;\/li&gt;")/', '', $html);
            break;

        case 'tel':
            $html = preg_replace('/>\-</', '><span>-</span><', $html);
            break;

        default:
            break;
    }

    return $html;
}

function uploadFile($Element) {
    
    $html = "";

    if ($Element->_value['tmp_name']) {
        $_file = $Element->_value;
        $_attributes = $Element->_attributes;

        $ext = ".".strtolower(end(explode(".", $_file['name'])));
        $fileName = $_attributes['file_name'] ? $_attributes['file_name'].$ext : $_file['name'];
        $filePath = $_attributes['file_dir'] ? $_attributes['file_dir'] : UPLOAD_DIR."/file";
        if (!file_exists(CB_PUBLIC.$filePath)) {
            mkdir(CB_PUBLIC.$filePath, 0777, true);
        }
        $filePath = rtrim($filePath, '\/')."/".$fileName;
        if (move_uploaded_file($_file['tmp_name'], CB_PUBLIC.$filePath)) {
            $action = new CBA_Action;
            $action->session->files[$Element->getName()] = $filePath;
        }
        
        if (preg_match('/(jpg|jpeg|gif|png)$/', strtolower($_file['name']))) {
            $options = array();
            if ($_attributes["width"]) {
                $options[] = "width=\"{$_attributes["width"]}\"";
            }
            if ($_attributes["height"]) {
                $options[] = "height=\"{$_attributes["height"]}\"";
            }
            $options = implode(" ", $options);

            $html .= "<img src=\"{$filePath}\" {$options}>";
        } else {
            $html .= "<label>".$filePath."</label>";
        }
        $html .= "<br><input type=\"hidden\" name=\"".$Element->getName()."\" value=\"".$filePath."\">";
    }
    return $html;
}
