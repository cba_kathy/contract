<?php

/**
 * default.php
 *
 * @category    CB_Framework
 * @package
 * @subpackage  admin
 * @copyright   CYBRiDGE
 * @license     CYBRiDGE License 3.1
 *
 */

function smarty_function_form_mce($params, &$smarty)
{
    if (empty($params['name'])){
        $smarty -> _trigger_fatal_error("[plugin] parameter 'name' cannot be empty");
        return;
    }

    $name = $params["name"];
    unset($params['name']);
    if (!$params['id']){
        preg_match('/\[[\w]+\]/', $name, $matches);
        $params['id'] = trim($matches[0], '\[\]');
    }

    $smarty -> sf -> updateElementAttr($name, $params);
    $Element = &$smarty -> sf -> getElement($name);
    $value = $Element->getValue();
    $html = "";

    if ($Element->_flagFrozen) {
        $html .= "{$value}<p class=\"noDisplay\">{$Element->toHtml()}</p>";
    } else {
        $html .= $Element->toHtml();
    }

    return $html;
}
