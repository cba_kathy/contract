<?php

/**
 * default.php
 *
 * @category    CB_Framework
 * @package
 * @subpackage  admin
 * @copyright   CYBRiDGE
 * @license     CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_is_phone extends CB_QuickForm_Rule {
	public $message = "[name]は正しい形式で入力してください。";

	function validate($value, $options = null) {
		if(is_array($value)){
			$value = implode("", $value);
		}
		if (empty($value)) {
			return true;
		}
		return preg_match("/^\+?[0-9]+([\-]{0,1}[0-9]+)*[0-9]+$/", $value);
	}
}
