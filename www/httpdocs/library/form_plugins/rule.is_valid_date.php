<?php

/**
 * default.php
 *
 * @category    CB_Framework
 * @package
 * @subpackage  admin
 * @copyright   CYBRiDGE
 * @license     CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_is_valid_date extends CB_QuickForm_Rule {

	public $message = "[name]に存在しない日付が指定されています。";

	function validate($value) {
		if (is_array($value)) {
			if($value["m"] || $value["d"] || $value["Y"]){
				return checkdate($value["m"], $value["d"], $value["Y"]);
			}
			return true;
		}
		return date('Y-m-d', strtotime($value)) == $value;
	}
}