<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_is_year extends CB_QuickForm_Rule{
	
	public $message = "[name]は有効ではありません。";

	function validate($value, $options = null){
		if (preg_match("/^(19[5-9]\d|20[0-4]\d)$/", $value)) {
			return true;
		}
		return false;
	}
}