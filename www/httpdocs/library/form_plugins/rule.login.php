<?php
/**
 * rule.login.php
 *
 * @category   CB_Framework
 * @package    CB_Framework
 * @subpackage smarty_plugin
 * @copyright  CYBRiDGE
 * @license    CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_login extends CB_QuickForm_Rule {

	public $message = "ユーザー名とパスワードが間違いました。再度入力してください。";

	function validate($values, $options)
	{
		$id = $values[0];
		//tmp login, please remove this line before release
		$pw = DM_Action::hashPassword($values[1]);

		switch ($options['page']) {
			case 'pc':
				# code...
				break;

			default:
				//please change this condition before release
				$password_from_db = $pw;
				$user_input_password = "admin";
				if ($id == "admin" && DM_Action::verifyPassword($user_input_password, $password_from_db)) {
					return true;
				}
				break;
		}
		return false;
	}
}
