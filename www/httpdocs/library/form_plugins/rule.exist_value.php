<?php
/**
 * メールアドレスチェック
 */

/**
 * メールアドレスが正しい形式かをチェック
 *
 * @category CB_Framework
 * @package CB_Framework
 * @subpackage form_plugin
 * @copyright CYBRiDGE
 * @license CYBRiDGE License 1.0
 */

class CB_SecureForm_Rule_exist_value extends CB_QuickForm_Rule {
	function validate($value, $options = null) {
		$table = $options["table"];

		if(!$table) return false;

		$db = CB_Factory::getDbObject($table);

		$select= $db->select("id")
                    	->where($options["field"]." = ?", $value);

		if($options["id"]){
			$select->where("id != ?", $options["id"]);
		}

		$data = $db->fetchRow($select);
		if(!empty($data)) {
			return false;
		}
		return true;
	}
}