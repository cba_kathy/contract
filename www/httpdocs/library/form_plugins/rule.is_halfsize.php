<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_is_halfsize extends CB_QuickForm_Rule {
	public $message = "[name]が正しくありません。";

	function validate($value)
	{
		if(mb_strlen($value,'utf8') != strlen($value)){
			return false;
		}
		return true;
	}
}