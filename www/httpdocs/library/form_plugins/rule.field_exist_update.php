<?php
/**
 * field exist
 *
 * Use:
"rule"    => array(
    "field_exist"   => array(
        "elements"  => array("job_type[job_type_name]"),
        "options"   => array("job_type", "job_type_name"),
        "message"   => "この職種名は既に使用されています。",
    ),
),
 */
class CB_SecureForm_Rule_field_exist_update extends CB_QuickForm_Rule{

    function validate($value, $options = null){
        if( $value[0] == null || $options[0] == null ) {
            return false;
        }

        $db_table = CB_Factory::getDbObject( $options[0] );

        $a_select = $db_table->select()
                        ->where($options[1] . " LIKE ?", $value[0])
                        // ->where($options[0].'_is_delete=?', 0)
                        ->where("id <> ?", $options[2]);

        $row = $db_table->fetchRow($a_select);
        if(!$row)
            return true;
        else
            return false;
    }
}
