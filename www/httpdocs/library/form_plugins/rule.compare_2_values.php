<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_compare_2_values extends CB_QuickForm_Rule{
	
	public $message = "パスワードとパスワード再確認が一致しません。";

	function validate($value, $options = null){
		$value1 = is_array($value) ? $value[0] : $value;
		$value2 = is_array($value) ? $value[1] : $options['value'];
		switch ($options['math']) {
			case '==':
				return ($value1 == $value2) ? true : false;
				break;
			case '!=':
				return ($value1 != $value2) ? true : false;
				break;
			case '===':
				return ($value1 === $value2) ? true : false;
				break;
			case '!==':
				return ($value1 !== $value2) ? true : false;
				break;
			case '>':
				return ($value1 > $value2) ? true : false;
				break;
			case '>=':
				return ($value1 >= $value2) ? true : false;
				break;
			case '<':
				return ($value1 < $value2) ? true : false;
				break;
			case '<=':
				return ($value1 <= $value2) ? true : false;
				break;
			default:
				return ($value1 === $value2) ? true : false;
				break;
		}
	}

}
