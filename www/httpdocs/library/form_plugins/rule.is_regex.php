<?php

/**
 * default.php
 *
 * @category    CB_Framework
 * @package
 * @subpackage  admin
 * @copyright   CYBRiDGE
 * @license     CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_is_regex extends CB_QuickForm_Rule {

	var $message = "[name]は正しい形式で入力してください。aaa";

	function validate($value, $options = null)
	{
		$regex = $options['regex'];
		if (!is_string($regex)) {
			return false;
		}
		return preg_match($regex, $value);

	} // end func validate
}