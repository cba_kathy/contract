<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_is_kanji extends CB_QuickForm_Rule {

	var $message = "[name]は漢字で入力してください。";

	function validate($value, $options = null) {
		$pattern = "/^\p{Han}+$/u";
		return preg_match($pattern, $value);
	}
}
