<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_is_fullsize extends CB_QuickForm_Rule {

	var $message = "[name]はひらがなで入力してください。";
	
	function validate($value, $options = null)
	{
		if(preg_match("/^[ァ-ヾ]+$/u",$value) || preg_match("/^[一-龠]+$/u",$value) || preg_match("/^[ぁ-ゞ\s]+$/u", $value) ){
			return true;
		}
		return false;
	} 
} 
