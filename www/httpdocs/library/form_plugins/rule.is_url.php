<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_is_url extends CB_QuickForm_Rule {

	public $message = "[name]は正しいURLの形式で入力してください。";

	function validate($value, $options)
	{
		if (empty($value) || preg_match('/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/', $value)) {
			return true;
		}
		return false;
	}
}
