<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_is_zip extends CB_QuickForm_Rule {

	var $message = "[name]は正しい形式で入力してください。";

	function validate($value, $options = null)
	{
		if(is_array($value)){
			$value = $value[0].$value[1];
		}
		return preg_match("/^([0-9]{7})$/", $value);

	} // end func validate
}
