<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_is_file extends CB_QuickForm_Rule {

	public $message = "[name] is invalid. (extension: \"[extension]\", size < [size])";
	private  $_maxFileSize = 1048576; // 1MB

    function validate($value, $options = null)
    {
    	if (!$value) {
    		return true;
    	}
        if (is_string($value)) {
            $name = $value;
            $size = filesize(CB_PUBLIC.$name);
        } else {
            $name = $value['name'];
            $size = $value['size'];
        }
        $extension = strtolower(end(explode('.', $name)));
    	if ($options['size']) {
    		$this->_maxFileSize = $options['size'];
    	}

    	if (preg_match('/^([0-9]+)([a-zA-Z]*)$/', $this->_maxFileSize, $matches)) {
            switch (strtoupper($matches['2'])) {
                case 'GB':
                case 'G':
                    $this->_maxFileSize = $matches['1'] * 1073741824;
                    break;
                case 'MB':
                case 'M':
                    $this->_maxFileSize = $matches['1'] * 1048576;
                    break;
                case 'KB':
                case 'K':
                    $this->_maxFileSize = $matches['1'] * 1024;
                    break;
                default:
                    $this->_maxFileSize = $matches['1'];
            }
        }    
        return preg_match('/('.$extension.')/', $options['extension']) && ($size < $this->_maxFileSize);
        
    } // end func validate
} 
