<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_check_unique extends CB_QuickForm_Rule {
	
	public $message = "[name]は存在しました。";
	
	function validate($value, $options = null) {
		$tbl = $options["table"];
		$field = $options["field"];

		$db = CB_Factory::getDbObject($tbl);
		$pk = $db->getPrimaryKey();
		
		$select = $db->select($pk, "tmp");
		$select->where("{$field} = ?", $value);
		if ($options['conditions']) {
			$conds = $options['conditions'];
			if (is_string($conds)) {
				$select->where($conds);
			} else {
				foreach ($conds as $cond => $value) {
					$select->where($cond, $value);
				}
			}
		}
		if($options["id"]){
			$select->where("{$pk} != ?", $options["id"]);
		}
		$data = $db->fetchRow($select);

		if(!$data) {
			return true;
		} else {
			return false;
		}
	}
}