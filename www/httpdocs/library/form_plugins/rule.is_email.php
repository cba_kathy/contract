<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_is_email extends CB_QuickForm_Rule {
	
	public $message = "[name]が正しくありません。";

    function validate($value)
    {
		return preg_match("/^[0-9a-zA-Z]+([a-zA-Z0-9]*(_|-|\.|\+){0,1}[a-zA-Z0-9])+@[a-zA-Z]+([a-zA-Z0-9]*(_|-|\.|\+){0,1}[a-zA-Z0-9])+(\.[a-z]{2,4})$/", $value);
    }
}