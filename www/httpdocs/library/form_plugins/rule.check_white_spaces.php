<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_check_white_spaces extends CB_QuickForm_Rule{
	
	public $message = "[name]半角英数字で入力してください。";

	function validate($value, $options = null){
		
		if(!preg_match('/^[　| ]+$/', $value."")){
			return true;
		}
		return false;
	}
}