<?php

/**
 * default.php
 *
 * @category	CB_Framework
 * @package
 * @subpackage	admin
 * @copyright	CYBRiDGE
 * @license		CYBRiDGE License 3.1
 *
 */

class CB_SecureForm_Rule_between extends CB_QuickForm_Rule{
	
	var $message = "[name]は[min]から[max]まで入力してください。";

	function validate($value, $options = null){
		if ($options['is_datetime']) {
			$value = floor((strtotime($value) - time()) / (24*60*60));
		}
		if (is_numeric($value) && $value >= $options['min'] && $value <= $options['max']) {
			return true;
		}
		return false;
   }
}
